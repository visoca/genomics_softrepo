#!/bin/bash
# igraph and methylKit need to be installed from the github repositories#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -l rmem=8g
#$ -j y
#$ -V
#$ -pe smp 6
#$ -o R-3.4.1-gcc-mkl-packages.log


# unsolved issues:
# MCMCpack doesn't install and hzar cannot be installed either, because it is dependant on it

# igraph and methylKit need to be installed from the github repositories

date

R='/usr/local/extras/Genomics/apps/R/3.4.1-gcc-mkl/bin/R'

cat > install_pkgs_R-3.4.1-gcc-mkl.R <<'EOF'

# set UK mirrors
# repos<-getCRANmirrors()
# uk.repos<-repos[repos$Country=="UK",]$URL
# names(uk.repos)<-rep("CRAN",length(uk.repos))
# options(repos=uk.repos[[1]])

# set preferred mirrors
preferred.repos<-c("https://mirrors.ebi.ac.uk/CRAN/", "https://cran.ma.imperial.ac.uk")
names(preferred.repos)<-rep("CRAN",length(preferred.repos))
options(repos=preferred.repos)

# install packages that require special configuration
# .............................................................................
# order is important: sp -> rgeos -> rgdal
insta.packages("sp")
# rgeos
install.packages("rgeos", 
configure.args=c("LDFLAGS=-L/usr/local/extras/Genomics/lib/lib", 
"PKG_CPPFLAGS=-I/usr/local/extras/Genomics/lib/include", 
"PKG_LIBS=-L/usr/local/extras/Genomics/lib/lib", 
"--with-geos-config=/usr/local/extras/Genomics/lib/bin/geos-config"))

# rgdal
install.packages("rgdal", 
configure.args=c("LDFLAGS=-L/usr/local/extras/Genomics/lib/lib", 
"PKG_CPPFLAGS=-I/usr/local/extras/Genomics/lib/include", 
"PKG_LIBS=-L/usr/local/extras/Genomics/lib/lib", 
"--with-gdal-config=/usr/local/extras/Genomics/lib/bin/gdal-config",
"--with-proj-lib=/usr/local/extras/Genomics/lib/proj.4/current/lib"))

# RcppEigen
# Require moving genomics softrepo Eigen temporarily from its location so it is not detected
system("mv /usr/local/extras/Genomics/lib/eigen /usr/local/extras/Genomics/lib/eigen-tmp")
install.packages("RcppEigen", Ncpus=6)
system("mv /usr/local/extras/Genomics/lib/eigen-tmp /usr/local/extras/Genomics/lib/eigen")
# perhaps there is another, more elegant way of doing this, but tweaking configuration flags
# doesn't work
# install.packages("RcppEigen", configure.args=c("CXX_FLAGS=-I../inst/include","PKG_CFLAGS=-I../inst/include/Eigen","PKG_CPPFLAGS=-I../inst/include/Eigen"))

# igraph
install.packages("devtools")
devtools::install_github("gaborcsardi/pkgconfig")
devtools::install_github("igraph/rigraph")
# .............................................................................

# install archived packages
# .............................................................................
install.packages("https://cran.r-project.org/src/contrib/Archive/laser/laser_2.4-1.tar.gz")
install.packages("https://cran.r-project.org/src/contrib/Archive/motmot/motmot_1.0.1.tar.gz")
# .............................................................................


# exclude packages that require special installation or are installed from unofficial repositories
# .............................................................................
exclude.packages<-c("rgdal", "rgeos", "RcppEigen", "igraph",
                    "ClockstaR", "diversitree", "genealogicalSorting", "phyloch", "scaleboot","methylKit","RBeast","rBEAST","splits","vioplotx",
					"Rmpi", "laser", "motmot")
# .............................................................................

# Install bioconductor and packages installed in previous version
# .............................................................................
# Bioconductor
source("http://bioconductor.org/biocLite.R")
biocLite()

# get list of packages installed in old version and check which ones are missing
package_df_old <- as.data.frame(installed.packages("/usr/local/extras/Genomics/apps/R/3.3.1/lib64/R/library/"))
package_df_new <- as.data.frame(installed.packages("/usr/local/extras/Genomics/apps/R/3.4.1/lib64/R/library/"))
package_list_old <- as.character(package_df_old$Package)
package_list_new<-as.character(package_df_new$Package)

packages2install<-setdiff(package_list_old,package_list_new)
packages2install<-packages2install[!packages2install %in% exclude.packages]

# install.packages(packages2install)
# use bioconductor to install packages
biocLite(packages2install, type="source")
# .............................................................................

# install packages from unofficial repositories
# .............................................................................
# ClockstaR2
# install.packages(c("curl","openssl","devtools")) 
devtools::install_github('ClockstaR', 'sebastianduchene')

# methylKit
devtools::install_github("al2na/methylKit", build_vignettes=FALSE, 
  repos=BiocInstaller::biocinstallRepos(),
      dependencies=TRUE, force=TRUE)

# genealogicalSorting
install.packages("http://molecularevolution.org/molevolfiles/gsi/resources/genealogicalSorting_0.92.tar.gz")

# phyloch
# install.packages(c("colorspace", "XML"))
install.packages("http://www.christophheibl.de/phyloch_1.5-5.tar.gz")

# RBeast
devtools::install_github("beast-dev/RBeast")

# rBEAST
devtools::install_github("olli0601/rBEAST")

# scaleboot
install.packages("http://www.sigmath.es.osaka-u.ac.jp/shimo-lab/prog/scaleboot/scaleboot_0.3-3.tar.gz")

# splits
install.packages("splits", repos="http://R-Forge.R-project.org")

# vioplotx
devtools::install_github("TomKellyGenetics/vioplotx")
# .............................................................................
EOF


$R CMD BATCH --vanilla install_pkgs_R-3.4.1-gcc-mkl.R

# custom diversitree
# $R CMD BATCH --vanilla 'install.packages(c("ape", "deSolve", "subplex", "Rcpp"))'
git clone git@bitbucket.org:javi_igea/ldg.git
$R CMD INSTALL ldg/diversitree --configure-args='--with-fftw=/usr/local/extras/Genomics/lib/'

