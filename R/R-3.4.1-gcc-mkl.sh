#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 24
#$ -j y
#$ -o R-3.4.1-gcc-mkl.log

# Compile with gcc 6.2 using MKL libraries for enhanced compatibility

CURRDIR=$PWD
rm -rf R-3.4.1.tar.gz R-3.4.1 >& /dev/null

# Get R and extract files
cd /local
wget https://cran.r-project.org/src/base/R-3/R-3.4.1.tar.gz
tar -xf R-3.4.1.tar.gz
cd R-3.4.1

module add compilers/gcc/4.9.2
# module add libs/binlibs/intel-mkl/2017.0
source /usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/mkl/bin/mklvars.sh intel64
MKL="-Wl,--no-as-needed -lmkl_gf_lp64 -Wl,--start-group -lmkl_gnu_thread  -lmkl_core  -Wl,--end-group -fopenmp  -ldl -lpthread -lm"

export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"

# flags from https://github.com/mikecroucher/HPC_Installers/blob/master/apps/R/3.3.1/sheffield/iceberg/intel_15/install_intel_r_sequential.sh
echo
echo "configure --enable-shared --enable-R-shlib --with-blas=$MKL --with-lapack --with-readline --prefix=/usr/local/extras/Genomics/apps/R/3.4.1-gcc-mkl"
echo "=============================================================================="
./configure \
--enable-shared \
--enable-R-shlib \
--with-blas="$MKL" \
--with-lapack \
--with-readline \
--prefix=/usr/local/extras/Genomics/apps/R/3.4.1-gcc-mkl
echo "=============================================================================="
echo
echo
echo
echo "make -j 24"
echo "=============================================================================="
make -j 24
echo "=============================================================================="
echo
echo
echo
echo "make check -k"
echo "=============================================================================="
# NOTE: Error in 'reg-tests-1d.R' is related to timezone and locale set up and it is benign
make check -k
echo "=============================================================================="
echo
echo
echo
echo "make install"
echo "=============================================================================="
make install
echo "=============================================================================="
echo
echo
echo
echo "Create wrappers"
echo "=============================================================================="
cd /usr/local/extras/Genomics/apps/R/3.4.1-gcc-mkl/bin

# R
# -----------------------------------------------------------------------------
mv R R.tmp
cat > R <<'EOF1'
#!/bin/sh
# Shell wrapper for R executable.

if [ -h $0 ]; then
    DIR=$(dirname $(readlink -m $0))
else
    DIR=$(dirname $0)
fi

# system packages take precedence
export R_LIBS="$DIR/../lib64/R/library:$R_LIBS"

# export Genomics Software Repository flags
# ---------------------------------------------------------
export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"
# ---------------------------------------------------------

# Set gcc 6.2 compiler
# ---------------------------------------------------------
export GCCDIR="/usr/local/packages6/compilers/gcc/6.2.0"

export PATH=$GCCDIR/bin:$PATH
export LD_LIBRARY_PATH=$GCCDIR/lib:$GCCDIR/lib64:$LD_LIBRARY_PATH

export CPPFLAGS="-I$GCCDIR/include $CPPFLAGS"
export CFLAGS="-I$GCCDIR/include $CPFLAGS"
export LDFLAGS="-L$GCCDIR/lib -L$GCCDIR/lib64 $LDFLAGS"

export CC=gcc
export CXX=g++
export F77=gfortran
export F90=gfortran
export F95=gfortran
export FC=gfortran
# ---------------------------------------------------------

# Set MKL libraries
# ---------------------------------------------------------
# module variables
export version=2017.0
export intelpsroot=/usr/local/packages6/compilers/intel-ps-xe-ce/$version/binary

# Variables determined using
# env2 -from bash -to modulecmd "/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/mkl/bin/mklvars.sh intel64" | sed -e "s#/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary#\$intelpsroot#g" -e 's/[{}]//g'
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export CPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/include:$CPATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export NLSPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64/locale/%l_%t/%N:$NLSPATH

export MKLROOT=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl

# License file (points at license server)
export INTEL_LICENSE_FILE=/usr/local/packages6/compilers/intel/license.lic

# Sample code
export MKL_SAMPLES=$intelpsroot/samples_2017/en/mkl/
# ---------------------------------------------------------

EOF1

tail -n+4 R.tmp >> R
rm R.tmp
chmod +x R
# -----------------------------------------------------------------------------

# Rscript
# -----------------------------------------------------------------------------
mv Rscript Rscript.bin
cat > Rscript <<'EOF2'
#!/bin/sh
# Shell wrapper for R executable.

if [ -h $0 ]; then
    DIR=$(dirname $(readlink -m $0))
else
    DIR=$(dirname $0)
fi

# system packages take precedence
export R_LIBS="$DIR/../lib64/R/library:$R_LIBS"

# export Genomics Software Repository flags
# ---------------------------------------------------------
export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"
# ---------------------------------------------------------

# Set gcc 6.2 compiler
# ---------------------------------------------------------
export GCCDIR="/usr/local/packages6/compilers/gcc/6.2.0"

export PATH=$GCCDIR/bin:$PATH
export LD_LIBRARY_PATH=$GCCDIR/lib:$GCCDIR/lib64:$LD_LIBRARY_PATH

export CPPFLAGS="-I$GCCDIR/include $CPPFLAGS"
export CFLAGS="-I$GCCDIR/include $CPFLAGS"
export LDFLAGS="-L$GCCDIR/lib -L$GCCDIR/lib64 $LDFLAGS"

export CC=gcc
export CXX=g++
export F77=gfortran
export F90=gfortran
export F95=gfortran
export FC=gfortran
# ---------------------------------------------------------

# Set MKL libraries
# ---------------------------------------------------------
# module variables
export version=2017.0
export intelpsroot=/usr/local/packages6/compilers/intel-ps-xe-ce/$version/binary

# Variables determined using
# env2 -from bash -to modulecmd "/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/mkl/bin/mklvars.sh intel64" | sed -e "s#/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary#\$intelpsroot#g" -e 's/[{}]//g'
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export CPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/include:$CPATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export NLSPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64/locale/%l_%t/%N:$NLSPATH

export MKLROOT=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl

# License file (points at license server)
export INTEL_LICENSE_FILE=/usr/local/packages6/compilers/intel/license.lic

# Sample code
export MKL_SAMPLES=$intelpsroot/samples_2017/en/mkl/
# ---------------------------------------------------------

$DIR/Rscript.bin "$@"

EOF2
chmod +x Rscript
# -----------------------------------------------------------------------------
echo "=============================================================================="

# delete all files
cd $CURRDIR
rm -rf R-3.4.1.tar.gz R-3.4.1
