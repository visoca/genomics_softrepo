#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 24
#$ -j y
#$ -o R-3.4.1-icc-mkl.log

# Get R and extract files
CURRDIR=$PWD
rm -rf R-3.4.1.tar.gz R-3.4.1 >& /dev/null

wget https://cran.r-project.org/src/base/R-3/R-3.4.1.tar.gz
tar -xf R-3.4.1.tar.gz
cd R-3.4.1

module load compilers/intel/17.0.0
source /usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/mkl/bin/mklvars.sh intel64

export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"

# flags from https://github.com/mikecroucher/HPC_Installers/blob/master/apps/R/3.3.1/sheffield/iceberg/intel_15/install_intel_r_sequential.sh
echo
echo "configure --enable-shared --enable-R-shlib --with-blas=-mkl=sequential --with-lapack=-mkl=sequential --with-readline --prefix=/usr/local/extras/Genomics/apps/R/3.4.1-icc-mkl"
echo "=============================================================================="
./configure \
--enable-shared \
--enable-R-shlib \
--with-blas="-mkl=sequential" \
--with-lapack="-mkl=sequential" \
--with-readline \
--prefix=/usr/local/extras/Genomics/apps/R/3.4.1-icc-mkl
echo "=============================================================================="
echo
echo
echo
echo "make -j 24"
echo "=============================================================================="
make -j 24
echo "=============================================================================="
echo
echo
echo
echo "make check -k"
echo "=============================================================================="
# NOTE: Error in 'reg-tests-1d.R' is related to timezone and locale set up and it is benign
make check -k
echo "=============================================================================="
echo
echo
echo
echo "make install"
echo "=============================================================================="
make install
echo "=============================================================================="
echo
echo
echo
echo "Create wrappers"
echo "=============================================================================="
cd /usr/local/extras/Genomics/apps/R/3.4.1-icc-mkl/bin

# R
# -----------------------------------------------------------------------------
mv R R.tmp
cat > R <<'EOF1'
#!/bin/sh
# Shell wrapper for R executable.

# system packages take precedence
if [ -h $0 ]; then
    DIR=$(dirname $(readlink -m $0))
else
    DIR=$(dirname $0)
fi

# system packages take precedence
export R_LIBS="$DIR/../lib64/R/library:$R_LIBS"

# Set intel compiler
# source /usr/local/modulefiles/dev/intel-compilers/17.0.0

export compilerversion=17.0.0
export parallelstudioversion=2017.0
export intelroot=/usr/local/packages6/compilers/intel-ps-xe-ce/$parallelstudioversion/binary
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export PATH=$intelroot/debugger_2017/gdb/intel64_mic/bin:$PATH
export PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/bin/intel64:$PATH;
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/lib/intel64_lin:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/lib/intel64:$LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export CLASSPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/lib/daal.jar:$CLASSPATH
export INTEL_LICENSE_FILE=$intelroot/compilers_and_libraries_2017.0.098/linux/licenses:$INTEL_LICENSE_FILE
export INTEL_LICENSE_FILE=/opt/intel/licenses:$INTEL_LICENSE_FILE
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/include:$CPATH
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/include:$CPATH
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/include:$CPATH
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/include:$CPATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/lib/intel64_lin:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/libipt/intel64/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/iga/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export NLSPATH=$intelroot/debugger_2017/gdb/intel64/share/locale/%l_%t/%N:$NLSPATH
export NLSPATH=$intelroot/debugger_2017/gdb/intel64_mic/share/locale/%l_%t/%N:$NLSPATH
export NLSPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64/locale/%l_%t/%N:$NLSPATH
export NLSPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64/locale/%l_%t/%N:$NLSPATH
export GDB_CROSS=$intelroot/debugger_2017/gdb/intel64_mic/bin/gdb-mic
export INTEL_PYTHONHOME=$intelroot/debugger_2017/python/intel64/
export PATH=$intelroot/debugger_2017/gdb/intel64_mic/bin:$PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/libipt/intel64/libi:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/iga/lib:$LD_LIBRARY_PATH
export MPM_LAUNCHER=$intelroot/debugger_2017/mpm/mic/bin/start_mpm.sh

export INTEL_LICENSE_FILE=/usr/local/packages6/compilers/intel/license.lic

EOF1

tail -n+4 R.tmp >> R
rm R.tmp
chmod +x R
# -----------------------------------------------------------------------------

# Rscript
# -----------------------------------------------------------------------------
mv Rscript Rscript.bin
cat > Rscript <<'EOF2'
#!/bin/sh
# Shell wrapper for R executable.

if [ -h $0 ]; then
    DIR=$(dirname $(readlink -m $0))
else
    DIR=$(dirname $0)
fi

# system packages take precedence
export R_LIBS="$DIR/../lib64/R/library:$R_LIBS"

# export flags
export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"

# Set intel compiler
# source /usr/local/modulefiles/compilers/intel/17.0.0

export compilerversion=17.0.0
export parallelstudioversion=2017.0
export intelroot=/usr/local/packages6/compilers/intel-ps-xe-ce/$parallelstudioversion/binary
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export PATH=$intelroot/debugger_2017/gdb/intel64_mic/bin:$PATH
export PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/bin/intel64:$PATH;
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/lib/intel64_lin:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/lib/intel64:$LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export CLASSPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/lib/daal.jar:$CLASSPATH
export INTEL_LICENSE_FILE=$intelroot/compilers_and_libraries_2017.0.098/linux/licenses:$INTEL_LICENSE_FILE
export INTEL_LICENSE_FILE=/opt/intel/licenses:$INTEL_LICENSE_FILE
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/include:$CPATH
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/include:$CPATH
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/include:$CPATH
export CPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/include:$CPATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/daal/lib/intel64_lin:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/libipt/intel64/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/iga/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/ipp/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export NLSPATH=$intelroot/debugger_2017/gdb/intel64/share/locale/%l_%t/%N:$NLSPATH
export NLSPATH=$intelroot/debugger_2017/gdb/intel64_mic/share/locale/%l_%t/%N:$NLSPATH
export NLSPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64/locale/%l_%t/%N:$NLSPATH
export NLSPATH=$intelroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64/locale/%l_%t/%N:$NLSPATH
export GDB_CROSS=$intelroot/debugger_2017/gdb/intel64_mic/bin/gdb-mic
export INTEL_PYTHONHOME=$intelroot/debugger_2017/python/intel64/
export PATH=$intelroot/debugger_2017/gdb/intel64_mic/bin:$PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/libipt/intel64/libi:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelroot/debugger_2017/iga/lib:$LD_LIBRARY_PATH
export MPM_LAUNCHER=$intelroot/debugger_2017/mpm/mic/bin/start_mpm.sh

export INTEL_LICENSE_FILE=/usr/local/packages6/compilers/intel/license.lic


$DIR/Rscript.bin "$@"

EOF2
chmod +x Rscript
# -----------------------------------------------------------------------------
echo "=============================================================================="

# delete all files
cd $CURRDIR
rm -rf R-3.4.1.tar.gz R-3.4.1
