#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -V
#$ -j y
#$ -o R-3.4.3-gcc-mkl-links.log

cd /usr/local/extras/Genomics/apps/R/
ln -sfT 3.4.3-gcc-mkl current

cd /usr/local/extras/Genomics
ln -sf /usr/local/extras/Genomics/apps/R/current/bin/R bin/
ln -sf /usr/local/extras/Genomics/apps/R/current/bin/Rscript bin/

cd /usr/local/extras/Genomics/lib
find /usr/local/extras/Genomics/apps/R/current/lib64/R/lib -type f -or -type l | xargs -I {} sh -c 'ln -sf {} lib/'
find /usr/local/extras/Genomics/apps/R/current/lib64/R/lib -mindepth 1 -type d | xargs -I {} sh -c 'ln -sfT {} lib/'

