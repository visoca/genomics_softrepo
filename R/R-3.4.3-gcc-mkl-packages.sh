#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -l rmem=8g
#$ -l h_rt=04:00:00
#$ -j y
#$ -V
#$ -o R-3.4.3-gcc-mkl-packages.log

# KNOWN ISSUES:
# - 'goseq', 'geneLenDataBase', GenomicFeatures' and 'GenomeInfoDbData' (bioconductor) cannot installed 
#   because they require 'RMySQL'/'RMariaDB', which requires 
#   mysql-devel/mariadb-devel libraries
# - 'svglite' cannot be installed because it depends on 'gdtools', which requires
#   cairo-devel libraries
# - 'rriskDistributions' cannot be installed because it depends on 'tkrplot',
#   which requires tk/tcl devel libraries

hostname
date

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRENT=$PWD

# list of packages can be obtained this way (must be a single line of code):
# R -e '
#  write.table(as.data.frame(installed.packages())[,1], file=paste("Rpackages_",format(Sys.time(), "%Y-%m-%d"), sep=""), row.names=F, quote=F)
# '
# PACKAGES="$PWD/Rpackages_"$(date +%Y-%m-%d)
PACKAGES="$PWD/Rpackages_2017-12-07"

# make backup of R
cd /usr/local/extras/Genomics/apps/R
tar -czf 3.4.3-gcc-mkl.tar.bz2 ./3.4.3-gcc-mkl/
cd $CURRENT

R='/usr/local/extras/Genomics/apps/R/3.4.3-gcc-mkl/bin/R'

cat > install_pkgs_R-3.4.3-gcc-mkl.R <<'EOF'

# set UK mirrors
# repos<-getCRANmirrors()
# uk.repos<-repos[repos$Country=="UK",]$URL
# names(uk.repos)<-rep("CRAN",length(uk.repos))
# options(repos=uk.repos[[1]])

# set preferred mirrors
preferred.repos<-c("https://mirrors.ebi.ac.uk/CRAN/", "https://cran.ma.imperial.ac.uk")
names(preferred.repos)<-rep("CRAN",length(preferred.repos))
options(repos=preferred.repos)

# install packages that require special configuration
# .............................................................................
# order is important: sp -> rgeos -> rgdal
install.packages("sp")
# rgeos
install.packages("rgeos", 
configure.args=c("LDFLAGS=-L/usr/local/extras/Genomics/lib/lib", 
"PKG_CPPFLAGS=-I/usr/local/extras/Genomics/lib/include", 
"PKG_LIBS=-L/usr/local/extras/Genomics/lib/lib", 
"--with-geos-config=/usr/local/extras/Genomics/lib/bin/geos-config"))

# rgdal
install.packages("rgdal", 
configure.args=c("LDFLAGS=-L/usr/local/extras/Genomics/lib/lib", 
"PKG_CPPFLAGS=-I/usr/local/extras/Genomics/lib/include", 
"PKG_LIBS=-L/usr/local/extras/Genomics/lib/lib", 
"--with-gdal-config=/usr/local/extras/Genomics/lib/bin/gdal-config",
"--with-proj-lib=/usr/local/extras/Genomics/lib/proj.4/current/lib"))

# RcppEigen
# Require moving genomics softrepo Eigen temporarily from its location so it is not detected
system("mv /usr/local/extras/Genomics/lib/eigen /usr/local/extras/Genomics/lib/eigen-tmp")
install.packages("RcppEigen")
system("mv /usr/local/extras/Genomics/lib/eigen-tmp /usr/local/extras/Genomics/lib/eigen")
# perhaps there is another, more elegant way of doing this, but tweaking configuration flags
# doesn't work:
# install.packages("RcppEigen", configure.args=c("CXX_FLAGS=-I../inst/include","PKG_CFLAGS=-I../inst/include/Eigen","PKG_CPPFLAGS=-I../inst/include/Eigen"))

# devtools
install.packages("devtools")
devtools::install_github("gaborcsardi/pkgconfig")

# igraph
# devtools::install_github("igraph/rigraph")

# Rmpi
install.packages("Rmpi", configure.args="--with-Rmpi-type='OPENMPI'")
# .............................................................................


# exclude packages that require special installation or are installed from unofficial repositories
# .............................................................................
exclude.packages<-c("rgdal", "rgeos", "RcppEigen",
                    "ClockstaR", "diversitree", "genealogicalSorting", "phyloch", "scaleboot","methylKit","RBeast","rBEAST","splits","vioplotx",
					"Rmpi", "laser", "motmot", "MACAU2", "INLA")
# .............................................................................

# Install bioconductor and packages installed in previous version
# .............................................................................
# Bioconductor
source("http://bioconductor.org/biocLite.R")
biocLite()

# get list of packages to install from file and check which ones are missing
package_df_new <- as.data.frame(installed.packages())
EOF

echo "package_list_old <- scan('$PACKAGES',what=character())" >> install_pkgs_R-3.4.3-gcc-mkl.R

cat >> install_pkgs_R-3.4.3-gcc-mkl.R <<'EOF2'
package_list_new<-as.character(package_df_new$Package)

packages2install<-setdiff(package_list_old,package_list_new)
packages2install<-packages2install[!packages2install %in% exclude.packages]

# install.packages(packages2install)
# use bioconductor to install packages
biocLite(packages2install, type="source")
# .............................................................................

# install archived packages
# .............................................................................
install.packages("https://cran.r-project.org/src/contrib/Archive/laser/laser_2.4-1.tar.gz")
install.packages("https://cran.r-project.org/src/contrib/Archive/motmot/motmot_1.0.1.tar.gz")
install.packages("https://cran.r-project.org/src/contrib/Archive/phybase/phybase_1.1.tar.gz")
# .............................................................................


# install packages from unofficial repositories
# .............................................................................
# MACAU2
devtools::install_github("jakyzhu/MACAU2")
https://github.com/jakyzhu/MACAU2.git

# INLA
install.packages("INLA", repos="https://inla.r-inla-download.org/R/stable", dep=TRUE)

# ClockstaR2
# install.packages(c("curl","openssl","devtools")) 
devtools::install_github('ClockstaR', 'sebastianduchene')

# methylKit
# devtools::install_github("al2na/methylKit", build_vignettes=FALSE, 
#   repos=BiocInstaller::biocinstallRepos(),
#       dependencies=TRUE, force=TRUE)

# genealogicalSorting
install.packages("http://molecularevolution.org/molevolfiles/gsi/resources/genealogicalSorting_0.92.tar.gz")

# phyloch
# install.packages(c("colorspace", "XML"))
install.packages("http://www.christophheibl.de/phyloch_1.5-5.tar.gz")

# RBeast
devtools::install_github("beast-dev/RBeast")

# rBEAST
devtools::install_github("olli0601/rBEAST")

# scaleboot
install.packages("http://www.sigmath.es.osaka-u.ac.jp/shimo-lab/prog/scaleboot/scaleboot_0.3-3.tar.gz")

# splits
install.packages("splits", repos="http://R-Forge.R-project.org")

# vioplotx
devtools::install_github("TomKellyGenetics/vioplotx")
# .............................................................................
EOF2

# install all packages
$R CMD BATCH --vanilla install_pkgs_R-3.4.3-gcc-mkl.R

# custom diversitree
# $R CMD BATCH --vanilla 'install.packages(c("ape", "deSolve", "subplex", "Rcpp"))'
git clone git@bitbucket.org:javi_igea/ldg.git
$R CMD INSTALL ldg/diversitree --configure-args='--with-fftw=/usr/local/extras/Genomics/lib/'
rm -rf ldg

date
