#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 24
#$ -l h_rt=00:40:00
#$ -V
##$ -pe smp 4
#$ -j y
#$ -o R-3.4.3-gcc-mkl.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

# Compile with gcc 6.2 using MKL libraries for enhanced compatibility
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then
	module add compilers/gcc/6.2
	module add libs/binlibs/intel-mkl/2017.0
	source /usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/mkl/bin/mklvars.sh intel64
	GCCDIR=/usr/local/packages6/compilers/gcc/6.2.0
	INTELPSROOT=/usr/local/packages6/compilers/intel-ps-xe-ce
	LICENCE=/usr/local/packages6/compilers/intel/license.lic
	MPIROOT=/usr/local/mpi/gcc/openmpi/1.10.1
else
	module add dev/gcc/6.2
	module add libs/intel-mkl/2017.0/binary
	source /usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary/mkl/bin/mklvars.sh intel64
	GCCDIR=/usr/local/packages/dev/gcc/6.2.0
	INTELPSROOT=/usr/local/packages/dev/intel-ps-xe-ce
	LICENCE=/usr/local/packages/dev/intel-ps-xe-ce/license.lic
	MPIROOT=/usr/local/packages/mpi/openmpi/2.1.1/gcc-6.2
fi

CURRDIR=$PWD
rm -rf R-3.4.3.tar.gz R-3.4.3 >& /dev/null

# Get R and extract files
wget https://cran.r-project.org/src/base/R-3/R-3.4.3.tar.gz
tar -xf R-3.4.3.tar.gz
cd R-3.4.3

# module add libs/binlibs/intel-mkl/2017.0
MKL="-Wl,--no-as-needed -lmkl_gf_lp64 -Wl,--start-group -lmkl_gnu_thread  -lmkl_core  -Wl,--end-group -fopenmp  -ldl -lpthread -lm"

export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"

# flags from https://github.com/mikecroucher/HPC_Installers/blob/master/apps/R/3.3.1/sheffield/iceberg/intel_15/install_intel_r_sequential.sh
echo
echo "configure --enable-shared --enable-R-shlib --with-blas=$MKL --with-lapack --with-readline --prefix=/usr/local/extras/Genomics/apps/R/3.4.3-gcc-mkl"
echo "=============================================================================="
./configure \
--enable-shared \
--enable-R-shlib \
--with-blas="$MKL" \
--with-lapack \
--with-readline \
--prefix=/usr/local/extras/Genomics/apps/R/3.4.3-gcc-mkl
echo "=============================================================================="
echo
echo
echo
# echo "make -j 4"
echo "make"
echo "=============================================================================="
# make -j 4
make
echo "=============================================================================="
echo
echo
echo
echo "make check -k"
echo "=============================================================================="
# NOTE: Error in 'reg-tests-1d.R' is related to timezone and locale set up and it is benign
make check -k
echo "=============================================================================="
echo
echo
echo
echo "make install"
echo "=============================================================================="
make install
echo "=============================================================================="
echo
echo
echo
echo "Create wrappers"
echo "=============================================================================="
cd /usr/local/extras/Genomics/apps/R/3.4.3-gcc-mkl/bin

# R
# -----------------------------------------------------------------------------
mv R R.tmp
cat > R <<'EOF1'
#!/bin/sh
# Shell wrapper for R executable.

if [ -h $0 ]; then
    DIR=$(dirname $(readlink -m $0))
else
    DIR=$(dirname $0)
fi

# system packages take precedence
export R_LIBS="$DIR/../lib64/R/library:$R_LIBS"

# export Genomics Software Repository flags
# ---------------------------------------------------------
export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"
# ---------------------------------------------------------

# Set gcc 6.2 compiler and OpenMPI
# ---------------------------------------------------------
EOF1

echo "export GCCDIR=\"$GCCDIR\"" >> R
echo "export MPIROOT=\"$MPIROOT\"" >> R

cat >> R <<'EOF2'
export PATH=$GCCDIR/bin:$PATH
export LD_LIBRARY_PATH=$GCCDIR/lib:$GCCDIR/lib64:$LD_LIBRARY_PATH

export CPPFLAGS="-I$GCCDIR/include $CPPFLAGS"
export CFLAGS="-I$GCCDIR/include $CPFLAGS"
export LDFLAGS="-L$GCCDIR/lib -L$GCCDIR/lib64 $LDFLAGS"

export CC=gcc
export CXX=g++
export F77=gfortran
export F90=gfortran
export F95=gfortran
export FC=gfortran

export MPI_HOME=$MPIROOT

export PATH=$MPIROOT/bin:$PATH
export CPATH=$MPIROOT/include:$CPATH
export LD_LIBRARY_PATH=$MPIROOT/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=$MPIROOT/lib:$LIBRARY_PATH
# ---------------------------------------------------------

# Set MKL libraries
# ---------------------------------------------------------
# module variables
export version=2017.0
EOF2

echo "export intelpsroot=$INTELPSROOT/\$version/binary" >> R

cat >> R <<'EOF3'

# Variables determined using
# env2 -from bash -to modulecmd "/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/mkl/bin/mklvars.sh intel64" | sed -e "s#/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary#\$intelpsroot#g" -e 's/[{}]//g'
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export CPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/include:$CPATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export NLSPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64/locale/%l_%t/%N:$NLSPATH

export MKLROOT=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl

# License file (points at license server)
EOF3

echo "export INTEL_LICENSE_FILE=$LICENCE" >> R

cat >> R <<'EOF4'
# Sample code
export MKL_SAMPLES=$intelpsroot/samples_2017/en/mkl/
# ---------------------------------------------------------

EOF4

tail -n+4 R.tmp >> R
# rm R.tmp
chmod +x R
# -----------------------------------------------------------------------------

# Rscript
# -----------------------------------------------------------------------------
mv Rscript Rscript.bin
cat > Rscript <<'EOF5'
#!/bin/sh
# Shell wrapper for R executable.

if [ -h $0 ]; then
    DIR=$(dirname $(readlink -m $0))
else
    DIR=$(dirname $0)
fi

# system packages take precedence
export R_LIBS="$DIR/../lib64/R/library:$R_LIBS"

# export Genomics Software Repository flags
# ---------------------------------------------------------
export CFLAGS="-I/usr/local/extras/Genomics/lib/include"
export CPPFLAGS="-I/usr/local/extras/Genomics/lib/include"
export LDFLAGS="-L/usr/local/extras/Genomics/lib/lib"
# ---------------------------------------------------------

# Set gcc 6.2 compiler and OpenMPI
# ---------------------------------------------------------
EOF5

echo "export GCCDIR=\"$GCCDIR\"" >> Rscript
echo "export MPIROOT=\"$MPIROOT\"" >> Rscript

cat >> Rscript <<'EOF6'
export PATH=$GCCDIR/bin:$PATH
export LD_LIBRARY_PATH=$GCCDIR/lib:$GCCDIR/lib64:$LD_LIBRARY_PATH

export CPPFLAGS="-I$GCCDIR/include $CPPFLAGS"
export CFLAGS="-I$GCCDIR/include $CFLAGS"
export LDFLAGS="-L$GCCDIR/lib -L$GCCDIR/lib64 $LDFLAGS"

export CC=gcc
export CXX=g++
export F77=gfortran
export F90=gfortran
export F95=gfortran
export FC=gfortran

export MPI_HOME=$MPIROOT

export PATH=$MPIROOT/bin:$PATH
export CPATH=$MPIROOT/include:$CPATH
export LD_LIBRARY_PATH=$MPIROOT/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=$MPIROOT/lib:$LIBRARY_PATH
# ---------------------------------------------------------

# Set MKL libraries
# ---------------------------------------------------------
# module variables
export version=2017.0
EOF6

echo "export intelpsroot=$INTELPSROOT/\$version/binary" >> Rscript

cat >> Rscript <<'EOF7'

# Variables determined using
# env2 -from bash -to modulecmd "/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/mkl/bin/mklvars.sh intel64" | sed -e "s#/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary#\$intelpsroot#g" -e 's/[{}]//g'
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LD_LIBRARY_PATH
export MIC_LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LD_LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LIBRARY_PATH
export LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/mic:$MIC_LIBRARY_PATH
export MIC_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/mic:$MIC_LIBRARY_PATH
export CPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/include:$CPATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:$LD_LIBRARY_PATH
export NLSPATH=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64/locale/%l_%t/%N:$NLSPATH

export MKLROOT=$intelpsroot/compilers_and_libraries_2017.0.098/linux/mkl

# License file (points at license server)
EOF7

echo "export INTEL_LICENSE_FILE=$LICENCE" >> Rscript

cat >> Rscript <<'EOF8'

# Sample code
export MKL_SAMPLES=$intelpsroot/samples_2017/en/mkl/
# ---------------------------------------------------------

$DIR/Rscript.bin "$@"

EOF8
chmod +x Rscript
# -----------------------------------------------------------------------------
echo "=============================================================================="

# delete all files
cd $CURRDIR
rm -rf R-3.4.3.tar.gz R-3.4.3

date
