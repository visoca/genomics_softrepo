#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -P molecosh
##$ -q molecosh.q
#$ -j y
#$ -o anaconda_python-3-5.1.0.log

CURRDIR=$PWD

rm -rf Anaconda3-5.1.0-Linux-x86_64.sh >& /dev/null
wget https://repo.anaconda.com/archive/Anaconda3-5.1.0-Linux-x86_64.sh
chmod +x Anaconda3-5.1.0-Linux-x86_64.sh
rm -rf /usr/local/extras/Genomics/apps/anaconda_python >& /dev/null

# install anaconda
./Anaconda3-5.1.0-Linux-x86_64.sh -p /usr/local/extras/Genomics/apps/anaconda_python -b

# update conda
 /usr/local/extras/Genomics/apps/anaconda_python/bin/conda update conda

# create basic environments
/usr/local/extras/Genomics/apps/anaconda_python/bin/conda create --name py27 python=2.7 anaconda
/usr/local/extras/Genomics/apps/anaconda_python/bin/conda create --name py36 python=3.6 anaconda

# install modules
/usr/local/extras/Genomics/apps/anaconda_python/bin/conda install pip numpy scipy cython mkl


# create links
ln -sf /usr/local/extras/Genomics/apps/anaconda_python/bin/conda /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/anaconda_python/bin/activate /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/anaconda_python/bin/deactivate /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -r Anaconda3-5.1.0-Linux-x86_64.sh
