#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o angsd-0.917.log

CURRDIR=$PWD
rm -rf angsd-0.917 angsd-0.917.tar.gz >& /dev/null

wget https://github.com/ANGSD/angsd/archive/0.917.tar.gz -O angsd-0.917.tar.gz
tar -xf angsd-0.917.tar.gz
cd angsd-0.917

# get htlib from previous version (standard ones downloaded from samtools github repository don't work)
# ................................................................
wget http://popgen.dk/software/download/angsd/angsd0.916.tar.gz
tar -xvf angsd0.916.tar.gz htslib
rm angsd0.916.tar.gz
cd htslib
make -j12 
cd ..
# ................................................................

make -j12 \
HTSSRC=./htslib \
CFLAGS=-L/usr/local/extras/Genomics/lib/zlib/current/lib \
CXXFLAGS=-I/usr/local/extras/Genomics/lib/zlib/current/include

mkdir bin
find . -type f -executable | grep -v sh | xargs -I {} sh -c 'mv {} bin/'
make clean
rm -r htslib

rm -rf /usr/local/extras/Genomics/apps/angsd/0.917 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/angsd/0.917
cp -r * /usr/local/extras/Genomics/apps/angsd/0.917
cd /usr/local/extras/Genomics/apps/angsd
ln -sfT 0.917 current
ln -sf /usr/local/extras/Genomics/apps/angsd/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf angsd-0.917 angsd-0.917.tar.gz
