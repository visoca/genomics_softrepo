#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o angsd-0.921.log

CURRDIR=$PWD
rm -rf angsd-0.921 angsd-0.921.tar.gz >& /dev/null

wget https://github.com/ANGSD/angsd/archive/0.921.tar.gz -O angsd-0.921.tar.gz
tar -xf angsd-0.921.tar.gz
cd angsd-0.921

# get htslib and compile it to have static library
wget https://github.com/samtools/htslib/releases/download/1.8/htslib-1.8.tar.bz2
tar -xvf htslib-1.8.tar.bz2
cd htslib-1.8
./autoreconf
LDFLAGS="-L/usr/local/extras/Genomics/lib/lib" \
CPPFLAGS="-I/usr/local/extras/Genomics/lib/include/" \
./configure 
make -j12
cd ..

LDFLAGS="-L/usr/local/extras/Genomics/lib/lib" \
CPPFLAGS="-I/usr/local/extras/Genomics/lib/include/" \
CFLAGS="-L/usr/local/extras/Genomics/lib/zlib/current/lib" \
CXXFLAGS="-I/usr/local/extras/Genomics/lib/zlib/current/include" \
HTSSRC=./htslib-1.8 \
make -j12

mkdir bin
find . -type f -executable | grep -v sh | xargs -I {} sh -c 'mv {} bin/'
make clean
rm -r htslib-1.8

rm -rf /usr/local/extras/Genomics/apps/angsd/0.921 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/angsd/0.921
cp -r * /usr/local/extras/Genomics/apps/angsd/0.921
cd /usr/local/extras/Genomics/apps/angsd
ln -sfT 0.921 current
ln -sf /usr/local/extras/Genomics/apps/angsd/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf angsd-0.921 angsd-0.921.tar.gz
