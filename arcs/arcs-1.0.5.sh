#!/bin/bash
#$ -pe openmp 4
#$ -j y
#$ -o arcs-1.0.5.log

module add dev/gcc/4.9.4
module add libs/boost/1.64.0/gcc-4.9.4
export NUM_THREADS=4

CURRDIR=$PWD
rm -rf arcs-1.0.5 arcs-1.0.5.tar.gz >& /dev/null

# cd /scratch
wget https://github.com/bcgsc/arcs/releases/download/v1.0.5/arcs-1.0.5.tar.gz
tar -xf arcs-1.0.5.tar.gz
cd arcs-1.0.5
./configure --prefix=/usr/local/extras/Genomics/apps/arcs/1.0.5
make -j $NUM_THREADS
rm -rf /usr/local/extras/Genomics/apps/arcs/1.0.5 >& /dev/null
make install

# wrapper to load libraries
# --------------------------------------
mv /usr/local/extras/Genomics/apps/arcs/1.0.5/bin/arcs /usr/local/extras/Genomics/apps/arcs/1.0.5/bin/arcs.bin
cat >/usr/local/extras/Genomics/apps/arcs/1.0.5/bin/arcs.sh <<'EOF'
#!/bin/bash

module add dev/gcc/4.9.4
module add libs/boost/1.64.0/gcc-4.9.4

/usr/local/extras/Genomics/apps/arcs/1.0.5/bin/arcs.bin $*
EOF
chmod +x /usr/local/extras/Genomics/apps/arcs/1.0.5/bin/arcs.sh
cd /usr/local/extras/Genomics/apps/arcs/1.0.5/bin
ln -s arcs.sh arcs
# --------------------------------------

cd /usr/local/extras/Genomics/apps/arcs
rm current >& /dev/null
ln -s 1.0.5 current
ln -s /usr/local/extras/Genomics/apps/arcs/current/bin/arcs /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf arcs-1.0.5 arcs-1.0.5.tar.gz
