#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o aspera-connnect-3.7.4.147727.log

# aspera-connect
# --------------------------------------------------------------
cd /local
rm -rf aspera-connect-3.7.4.147727* >& /dev/null

mkdir aspera-connect-3.7.4.147727
wget http://d3gcli72yxqn2z.cloudfront.net/connect/bin/aspera-connect-3.7.4.147727-linux-64.tar.gz
tar -xf aspera-connect-3.7.4.147727-linux-64.tar.gz
tail -n+105 aspera-connect-3.7.4.147727-linux-64.sh > aspera-connect-3.7.4.147727-linux-64.tar.gz
tar -xf aspera-connect-3.7.4.147727-linux-64.tar.gz -C aspera-connect-3.7.4.147727
cd aspera-connect-3.7.4.147727

mkdir -p /usr/local/extras/Genomics/apps/aspera-connect/3.7.4.147727
cp -r * /usr/local/extras/Genomics/apps/aspera-connect/3.7.4.147727/
cd /usr/local/extras/Genomics/apps/aspera-connect/
rm current >& /dev/null
ln -s 3.7.4.147727 current
rm /usr/local/extras/Genomics/bin/ascp
ln -s /usr/local/extras/Genomics/apps/aspera-connect/current/bin/ascp /usr/local/extras/Genomics/bin/

rm -rf /local/aspera-connect-3.7.4.147727*
# --------------------------------------------------------------

