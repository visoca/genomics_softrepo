#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o astral-multiind-5.4.4.log

CURRDIR=$PWD
rm -rf ASTRAL >& /dev/null

git clone --branch multiind https://github.com/smirarab/ASTRAL
cd ASTRAL
./make.sh
cd Astral
cat > astral-multiind.sh << 'EOF'
#!/bin/bash

export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH

if [[ -z $MEM ]];
then
	MEM=3 # memory in GB
fi

if [ -h $0 ]; then
	DIR=$(dirname $(readlink -m $0))
else
	DIR=$(dirname $0)
fi

if [[ $# -eq 0 || $1 == "-h" || $1 == "--help" || $1 == "-help" ]]; then
	echo
	echo "This is a wrapper to use ASTRAL multi individual with the simple command 'astral-multiind'."
	echo
	echo "  Maximum Java heap size (Xmx) set to $MEM""g"
	echo "  You can change this value with: MEM=<memory in GB> astral-multiind <options>"
	echo "    example: MEM=6 astral-multiind -i trees"
	echo
	echo "Usage: astral-multiind <arguments>"
	echo
	java -Xmx"$MEM"g -jar $DIR/astral.5.4.4.jar --help
	echo
	exit
fi

echo
echo "Maximum Java heap size (Xmx) set to $MEM""g"
echo
java -Xmx"$MEM"g -jar $DIR/astral.5.4.4.jar $*


EOF
chmod +x astral-multiind.sh

rm -rf /usr/local/extras/Genomics/apps/astral-multiind/5.4.4 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/astral-multiind/5.4.4
cp -rL * /usr/local/extras/Genomics/apps/astral-multiind/5.4.4/

cd /usr/local/extras/Genomics/apps/astral-multiind
ln -sTf 5.4.4 current
cd current
ln -s astral-multiind.sh astral-multiind
ln -sf /usr/local/extras/Genomics/apps/astral-multiind/current/astral-multiind /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf ASTRAL
