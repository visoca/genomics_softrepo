#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o astral2-4.11.2.log

CURRDIR=$PWD
rm -rf ASTRAL >& /dev/null

git clone --branch master https://github.com/smirarab/ASTRAL.git
cd ASTRAL
./make.sh
cd Astral
cat > astral2.sh << 'EOF'
#!/bin/bash

export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH

if [[ -z $MEM ]];
then
	MEM=3 # memory in GB
fi

if [ -h $0 ]; then
	DIR=$(dirname $(readlink -m $0))
else
	DIR=$(dirname $0)
fi

if [[ $# -eq 0 || $1 == "-h" || $1 == "--help" || $1 == "-help" ]]; then
	echo
	echo "This is a wrapper to use ASTRAL-II with the simple command 'astral2'."
	echo
	echo "  Maximum Java heap size (Xmx) set to $MEM""g"
	echo "  You can change this value with: MEM=<memory in GB> astral2 <options>"
	echo "    example: MEM=6 astral2 -i trees"
	echo
	echo "Usage: astral2 <arguments>"
	echo
	java -Xmx"$MEM"g -jar $DIR/astral.4.11.2.jar --help
	echo
	exit
fi

echo
echo "Maximum Java heap size (Xmx) set to $MEM""g"
echo
java -Xmx"$MEM"g -jar $DIR/astral.4.11.2.jar $*


EOF
chmod +x astral2.sh

rm -rf /usr/local/extras/Genomics/apps/astral2/4.11.2 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/astral2/4.11.2
cp -rL * /usr/local/extras/Genomics/apps/astral2/4.11.2/
cd /usr/local/extras/Genomics/apps/astral2
ln -sTf 4.11.2 current
cd current
ln -s astral2.sh astral2
ln -sf /usr/local/extras/Genomics/apps/astral2/current/astral2 /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf ASTRAL
