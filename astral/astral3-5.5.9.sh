#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -V
#$ -o astral3-5.5.9.log

source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf ASTRAL >& /dev/null

git clone https://github.com/smirarab/ASTRAL.git
cd ASTRAL
./make.sh
cd Astral
cat > astral3.sh << 'EOF'
#!/bin/bash

export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH

if [[ -z $MEM ]];
then
	MEM=3 # memory in GB
fi

if [ -h $0 ]; then
	DIR=$(dirname $(readlink -m $0))
else
	DIR=$(dirname $0)
fi

if [[ $# -eq 0 || $1 == "-h" || $1 == "--help" || $1 == "-help" ]]; then
	echo
	echo "This is a wrapper to use ASTRAL-III with the simple command 'astral3'."
	echo
	echo "  Maximum Java heap size (Xmx) set to $MEM""g"
	echo "  You can change this value with: MEM=<memory in GB> astral3 <options>"
	echo "    example: MEM=6 astral3 -i trees"
	echo
	echo "Usage: astral3 <arguments>"
	echo
	java -Xmx"$MEM"g -jar $DIR/astral.5.5.9.jar --help
	echo
	exit
fi

echo
echo "Maximum Java heap size (Xmx) set to $MEM""g"
echo
java -Xmx"$MEM"g -jar $DIR/astral.5.5.9.jar $*


EOF
chmod +x astral3.sh
ln -s astral3.sh astral3

rm -rf /usr/local/extras/Genomics/apps/astral3/5.5.9 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/astral3/5.5.9
cp -rL * /usr/local/extras/Genomics/apps/astral3/5.5.9/

cd /usr/local/extras/Genomics/apps/astral3
ln -sTf 5.5.9 current
ln -sf /usr/local/extras/Genomics/apps/astral3/current/astral3 /usr/local/extras/Genomics/bin/
cd current

cd $CURRDIR
rm -rf ASTRAL
