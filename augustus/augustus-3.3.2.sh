#!/bin/bash
#$ -j y
#$ -o augustus-3.3.2.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

# Load boost library
# module add libs/boost/1.64.0/gcc-4.9.4

CURRDIR=$PWD
rm -rf Augustus-3.3.2.tar.gz Augustus-3.3.2 >& /dev/null

wget https://github.com/Gaius-Augustus/Augustus/archive/3.3.2.tar.gz -O Augustus-3.3.2.tar.gz
tar -xf Augustus-3.3.2.tar.gz
cd Augustus-3.3.2


# get samtools 0.2.0-rc7
# ---------------------------------------------------------------------------------------------
cd auxprogs
mkdir htslib-samtools-bcftools
cd htslib-samtools-bcftools

wget https://github.com/samtools/htslib/archive/0.2.0-rc7.tar.gz -O htslib-0.2.0-rc7.tar.gz
tar -xf htslib-0.2.0-rc7.tar.gz
rm htslib-0.2.0-rc7.tar.gz
mv htslib-0.2.0-rc7 htslib
cd htslib
make
cd ..

wget https://github.com/samtools/samtools/archive/0.2.0-rc7.tar.gz -O samtools-0.2.0-rc7.tar.gz
tar -xf samtools-0.2.0-rc7.tar.gz
rm samtools-0.2.0-rc7.tar.gz
mv samtools-0.2.0-rc7 samtools
cd samtools
make
cd ..

wget https://github.com/samtools/bcftools/archive/0.2.0-rc7.tar.gz -O bcftools-0.2.0-rc7.tar.gz
tar -xf bcftools-0.2.0-rc7.tar.gz
rm bcftools-0.2.0-rc7.tar.gz
mv bcftools-0.2.0-rc7 bcftools
cd bcftools
make
cd ../../..
# ---------------------------------------------------------------------------------------------

# get lpsolve library
# cd auxprogs
# # ---------------------------------------------------------------------------------------------
# wget https://kent.dl.sourceforge.net/project/lpsolve/lpsolve/5.5.2.5/lp_solve_5.5.2.5_source.tar.gz
# tar -xf lp_solve_5.5.2.5_source.tar.gz
# mv lp_solve_5.5 lpsolve
# mkdir include
# mkdir lib
# cd lpsolve/lpsolve55
# sh ccc
# mv bin/ux64/* ../lib
# cd ../lp_solve
# sh ccc
# cd ..
#
# cd ../../..
# # ---------------------------------------------------------------------------------------------

# fix bam2wig
sed -i '8iTOOLDIR = ../htslib-samtools-bcftools' auxprogs/bam2wig/Makefile

# fix bam2hints
perl -pi -e 's/^INCLUDES.*/INCLUDES \= \/usr\/local\/extras\/Genomics\/apps\/bamtools\/2\.5\.1\/include\/bamtools/g' auxprogs/bam2hints/Makefile
perl -pi -e 's/^LIBS \= -lbamtools/LIBS \= \-L\/usr\/local\/extras\/Genomics\/apps\/bamtools\/2\.5\.1\/lib64 \-lbamtools/g' auxprogs/bam2hints/Makefile

# fix filterBam
perl -pi -e 's/^BAMTOOLS.*/BAMTOOLS \= \/usr\/local\/extras\/Genomics\/apps\/bamtools\/2\.5\.1\/include\/bamtools/g' auxprogs/filterBam/src/Makefile
perl -pi -e 's/^LIBS \= \-lbamtools/LIBS \= \-L\/usr\/local\/extras\/Genomics\/apps\/bamtools\/2\.5\.1\/lib64 \-lbamtools/g' auxprogs/filterBam/src/Makefile

# fix utrrnaseq
perl -pi -e 's/g\+\+/g\+\+\ \-I\/usr\/local\/extras\/Genomics\/lib\/include/g' auxprogs/utrrnaseq/Debug/src/subdir.mk

# change perl scripts
perl -pi -e 's/\/usr\/bin\/perl/\/usr\/bin\/env perl/g' scripts/*.pl

# change installation path
perl -pi -e 's/^INSTALLDIR.*/INSTALLDIR \= \/usr\/local\/extras\/Genomics\/apps\/augustus\/3.3.2/g' Makefile
perl -pi -e 's/^\tln \-sf /\# \tln \-sf/g' Makefile
perl -pi -e 's/^\#.*ZIPINPUT \= true/ZIPINPUT \= true/g' common.mk
# perl -pi -e 's/^\#.*COMPGENEPRED \= true/COMPGENEPRED \= true/g' common.mk

# add path to Genomics Repository libs
sed -i '28iLIBS += -L/usr/local/extras/Genomics/lib/lib' src/Makefile
sed -i '29iINCLS += -I/usr/local/extras/Genomics/lib/include' src/Makefile
# sed -i '28iLIBS += -L/usr/local/extras/Genomics/lib/lib -L../auxprogs/lpsolve/lpsolve55/bin/ux64' src/Makefile
# sed -i '29iINCLS += -I/usr/local/extras/Genomics/lib/include -I../auxprogs/lpsolve' src/Makefile

make
make install

cd /usr/local/extras/Genomics/apps/augustus/3.3.2
cp -r config config-users
chmod -R g+rwX config-users
cd ..
ln -sTf 3.3.2 current

cd $CURRDIR
rm -rf Augustus-3.3.2.tar.gz Augustus-3.3.2

echo "=============================================================================="
