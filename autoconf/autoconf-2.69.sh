#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o autoconf-2.69.log


cd /local
rm -rf autoconf-2.69* >& /dev/null
wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz
tar -xf autoconf-2.69.tar.gz
cd autoconf-2.69

./configure --prefix=/usr/local/extras/Genomics/apps/autoconf/2.69
make -j 12
make check
rm -rf /usr/local/extras/Genomics/apps/autoconf/2.69 >& /dev/null
make install
rm -rf autoconf-2.69

cd /usr/local/extras/Genomics/apps/autoconf
rm current >& /dev/null
ln -s 2.69 current

ln -s /usr/local/extras/Genomics/apps/autoconf/current/bin/* /usr/local/extras/Genomics/bin/

