#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 24
#$ -j y
#$ -o automake-1.15.1.log


CURRDIR=$PWD
rm -rf automake-1.15.1 automake-1.15.1.tar.gz >& /dev/null
wget http://ftp.gnu.org/gnu/automake/automake-1.15.1.tar.gz
tar -xf automake-1.15.1.tar.gz
cd automake-1.15.1

./configure --prefix=/usr/local/extras/Genomics/apps/automake/1.15.1
make -j 24
make -j 24check
rm -rf /usr/local/extras/Genomics/apps/automake/1.15.1 >& /dev/null
make install
rm -rf automake-1.15.1

cd /usr/local/extras/Genomics/apps/automake
rm current >& /dev/null
ln -s 1.15.1 current

ln -s /usr/local/extras/Genomics/apps/automake/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf automake-1.15.1 automake-1.15.1.tar.gz
