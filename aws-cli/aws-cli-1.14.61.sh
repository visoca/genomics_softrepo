#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o aws-cli-1.14.61.log

conda remove -n py36awscli --all -y >& /dev/null
conda create -n py36awscli python=3.6 anaconda -y
source activate py36awscli
conda install -c conda-forge awscli -y

# wrapper to call aws
rm -rf /usr/local/extras/Genomics/apps/aws-cli/1.14.61 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/aws-cli/1.14.61 >& /dev/null
cd /usr/local/extras/Genomics/apps/aws-cli/1.14.61
cat > aws.sh << 'EOF'
#!/bin/bash

source activate py36awscli

aws $*

source deactivate

EOF
chmod +x aws.sh
ln -s aws.sh aws
 
cd /usr/local/extras/Genomics/apps/aws-cli
ln -sTf 1.14.61 current
ln -sf /usr/local/extras/Genomics/apps/aws-cli/current/aws /usr/local/extras/Genomics/bin/

