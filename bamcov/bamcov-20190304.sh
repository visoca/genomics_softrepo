#!/bin/bash
#$ -j y
#$ -o bamcov-20190304.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf bamcov >& /dev/null

git clone https://github.com/fbreitwieser/bamcov.git
cd bamcov
make
make test

rm -rf /usr/local/extras/Genomics/apps/bamcov/20190304 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/bamcov/20190304
cp -r * /usr/local/extras/Genomics/apps/bamcov/20190304

cd /usr/local/extras/Genomics/apps/bamcov
ln -sTf 20190304 current
ln -s /usr/local/extras/Genomics/apps/bamcov/current/bamcov /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf bamcov

echo "=============================================================================="
