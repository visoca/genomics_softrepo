#!/bin/bash
#$ -j y
#$ -o bamtools-2.5.1.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf bamtools-2.5.1.tar.gz bamtools-2.5.1 >& /dev/null

wget https://github.com/pezmaster31/bamtools/archive/v2.5.1.tar.gz -O bamtools-2.5.1.tar.gz
tar -xf bamtools-2.5.1.tar.gz
cd bamtools-2.5.1
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/extras/Genomics/apps/bamtools/2.5.1 ..
make
rm -rf /usr/local/extras/Genomics/apps/bamtools/2.5.1 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/bamtools
ln -sTf 2.5.1 current
ln -s /usr/local/extras/Genomics/apps/bamtools/current/bin/* /usr/local/extras/Genomics/bin/
ln -s /usr/local/extras/Genomics/apps/bamtools/current/lib64/* /usr/local/extras/Genomics/lib/lib/
ln -s /usr/local/extras/Genomics/apps/bamtools/current/lib64/pkgconfig/* /usr/local/extras/Genomics/lib/lib/pkgconfig/
ln -s /usr/local/extras/Genomics/apps/bamtools/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf bamtools-2.5.1.tar.gz bamtools-2.5.1

echo "=============================================================================="
