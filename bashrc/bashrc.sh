#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -l h_rt=00:10:00
#$ -j y
#$ -o bashrc.log

# Genomics Software Repository bashrc to be add to users .bash_profile
# echo -e "if [[ -e '/usr/local/extras/Genomics' ]];\nthen\n\tsource /usr/local/extras/Genomics/.bashrc\nfi" >> $HOME/.bash_profile

cat > /usr/local/extras/Genomics/.bashrc <<'EOF'
echo
echo "  Your account is set up to use the Genomics Software Repository"
echo "     More info: http://soria-carrasco.staff.shef.ac.uk/softrepo"
echo

# path to Genomics Software Repository executables and libraries
export PATH=/usr/local/extras/Genomics/bin:$PATH
export PATH=/usr/local/extras/Genomics/lib/bin:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/extras/Genomics/lib/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/extras/Genomics/lib
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/extras/Genomics/lib/lib/pkgconfig

# set up perlbrew (command must be executed in this order)
rm -rf $HOME/.perlbrew-softrepo >& /dev/null
export PERLBREW_HOME=$HOME/.perlbrew-softrepo
source /usr/local/extras/Genomics/apps/perlbrew/current/etc/bashrc
/usr/local/extras/Genomics/apps/perlbrew/current/bin/perlbrew init >& /dev/null
/usr/local/extras/Genomics/apps/perlbrew/current/bin/perlbrew switch perl-5.26.1 >& /dev/null
source /usr/local/extras/Genomics/apps/perlbrew/current/etc/bashrc

# set up java
export JAVA_HOME=/usr/local/extras/Genomics/apps/java/current
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/extras/Genomics/apps/java/current/lib:/usr/local/extras/Genomics/apps/java/current/jre/lib/amd64:/usr/local/extras/Genomics/apps/java/current/jre/lib/amd64/server:$LD_LIBRARY_PATH

EOF
