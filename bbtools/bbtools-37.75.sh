#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -V
#$ -j y
#$ -o bbtools-37.75.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf bbmap BBMap_37.44.tar.gz >& /dev/null
wget https://sourceforge.net/projects/bbmap/files/BBMap_37.75.tar.gz
tar -xf BBMap_37.75.tar.gz
cd bbmap/jni
make -f makefile.linux
cd ..
rm -rf /usr/local/extras/Genomics/apps/bbtools/37.75 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/bbtools/37.75
cp -r * /usr/local/extras/Genomics/apps/bbtools/37.75

cd /usr/local/extras/Genomics/apps/bbtools
ln -sfT 37.75 current
ln -sf /usr/local/extras/Genomics/apps/bbtools/current/*.sh /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf bbmap BBMap_37.75.tar.gz

echo "=============================================================================="

date
