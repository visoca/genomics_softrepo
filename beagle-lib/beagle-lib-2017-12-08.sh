#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -l h_rt=00:45:00
#$ -l gpu=1
#$ -V
#$ -j y
#$ -o beagle-lib-2017-12-08.log

source /usr/local/extras/Genomics/.bashrc
module add libs/CUDA/8.0.44/binary

CURRDIR=$PWD
rm -rf beagle-lib >& /dev/null

git clone --depth=1 https://github.com/beagle-dev/beagle-lib.git
cd beagle-lib
./autogen.sh
./configure --prefix=/usr/local/extras/Genomics/lib/beagle-lib/2017-12-08
make
make check
rm -rf /usr/local/extras/Genomics/lib/beagle-lib/2017-12-08 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/beagle-lib
ln -sTf 2017-12-08 current
cd /usr/local/extras/Genomics/lib
ln -sf /usr/local/extras/Genomics/lib/beagle-lib/current/lib/* lib/ 
ln -sf /usr/local/extras/Genomics/lib/beagle-lib/current/lib/pkgconfig/* lib/pkgconfig/
ln -sf /usr/local/extras/Genomics/lib/beagle-lib/current/include/* include/

cd $CURRDIR
rm -rf beagle-lib
