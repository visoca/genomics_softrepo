#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o beagle-4.1.log

CURRDIR=$PWD
rm -rf beagle-4.1 >& /dev/null
mkdir beagle-4.1
cd beagle-4.1
wget https://faculty.washington.edu/browning/beagle/beagle.08Jun17.d8b.jar
wget https://faculty.washington.edu/browning/beagle/release_notes
wget https://faculty.washington.edu/browning/beagle/gpl_license
wget https://faculty.washington.edu/browning/beagle/beagle_4.1_21Jan17.pdf

# wrapper
cat > beagle.sh << 'EOF'
#!/bin/bash

# BEAGLE wrapper

export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH

if [[ -z $MEM ]];
then
	MEM=3 # memory in GB
fi

if [ -h $0 ]; then
	DIR=$(dirname $(readlink -m $0))
else
	DIR=$(dirname $0)
fi

if [[ $# -eq 0 || $1 == "-h" || $1 == "--help" || $1 == "-help" ]]; then
	echo
	echo "This is a wrapper to use BEAGLE with the simple command 'beagle'."
	echo
	echo "  Maximum Java heap size (Xmx) set to $MEM""g"
	echo "  You can change this value with: MEM=<memory in GB> beagle <options>"
	echo "    example: MEM=6 beagle gt=myfile.vcf.gz out=out.gt"
	echo
	echo "Usage: beagle <arguments>"
	echo
	java -Xmx"$MEM"g -jar $DIR/beagle.08Jun17.d8b.jar
	exit
fi

echo
echo "Maximum Java heap size (Xmx) set to $MEM""g"
echo
java -Xmx"$MEM"g -jar $DIR/beagle.08Jun17.d8b.jar $*
EOF
chmod +x beagle.sh
ln -s beagle.sh beagle

rm -rf /usr/local/extras/Genomics/apps/beagle/4.1 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/beagle/4.1
cp -r * /usr/local/extras/Genomics/apps/beagle/4.1/

cd /usr/local/extras/Genomics/apps/beagle
rm current >& /dev/null
ln -sTf 4.1 current
ln -sf /usr/local/extras/Genomics/apps/beagle/current/beagle /usr/local/extras/Genomics/bin/

# =============================================================================
# =============================================================================
# =============================================================================

# beagle utilities
cd $CURRDIR
mkdir beagle-utilities
cd beagle-utilities
wget https://faculty.washington.edu/browning/beagle_utilities
cat beagle_utilities | perl -pe '/\<a href\=\"(.+\.jar)\"\>/; $_="$1\n";' | grep jar | \
sort | uniq | \
xargs -I {} sh -c 'wget https://faculty.washington.edu/browning/beagle_utilities/{}'
rm beagle_utilities

cat > beagle-util.sh << 'EOF' # done until here
#!/bin/bash

# beagle utilities tools wrapper

# $1 -> beagle utility tool (name jar file)
# $2 .. $n -> rest of arguments

export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH

if [[ -z $MEM ]];
then
	MEM=3 # memory in GB
fi

if [ -h $0 ]; then
	DIR=$(dirname $(readlink -m $0))
else
	DIR=$(dirname $0)
fi

UTILITIES=($(cd $DIR; ls *.jar))

if [[ $# -eq 0 || $1 == "-h" || $1 == "--help" || $1 == "-help" ]]; then
	echo
	echo "This is a wrapper to use begle utilities with the simple command 'beagle-util'."
	echo
	echo "  Maximum Java heap size (Xmx) set to $MEM""g"
	echo "  You can change this value with: MEM=<memory in GB> beagle-util <options>"
	echo "    example: MEM=6 beagle-util consensusvcf.jar file1.vcf file2.vcf > fileConsensus.vcf"
	echo
	echo "Usage: beagle-util <beagle utility> <arguments>"
	echo
	echo "Available utilities:"
	echo
	for ut in "${UTILITIES[@]}";
	do
		echo "  $ut"
	done
	echo
	exit
fi

if [[ " ${UTILITIES[*]} " != *" $1 "* ]];
then
	echo
	echo "$1 is not a beagle utility! Use 'beagle-util -h' to see available tools."
	echo
	exit
fi


echo
echo "Maximum Java heap size (Xmx) set to $MEM""g"
echo
java -Xmx"$MEM"g -jar $DIR/$1 $*

EOF
chmod +x beagle-util.sh
ln -s beagle-util.sh beagle-util

rm -rf /usr/local/extras/Genomics/apps/beagle-utilities/2017-09-18 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/beagle-utilities/2017-09-18 
cp -r * /usr/local/extras/Genomics/apps/beagle-utilities/2017-09-18/

cd /usr/local/extras/Genomics/apps/beagle-utilities
ln -sTf 2017-09-18 current
ln -s /usr/local/extras/Genomics/apps/beagle-utilities/current/beagle-util /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf beagle-4.1 beagle-utilities 
