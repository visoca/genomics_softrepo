#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -V
#$ -j y
#$ -o beast-2.5.1.log

CURRDIR=$PWD
rm -rf BEAST.v2.5.1.linux.tgz beast >& /dev/null

wget https://github.com/CompEvol/beast2/releases/download/v2.5.1/BEAST.v2.5.1.linux.tgz
tar -xvf BEAST.v2.5.1.linux.tgz
cd beast
cd bin
# Edit all wrappers and change java home, and set package manager path
#    2 export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
#    3 export BEAST_PACKAGE_PATH='/usr/local/extras/Genomics/apps/beast/2.5.1'

ls * | xargs -I {} sh -c "cat {} | \
sed -e '2i\\\nexport JAVA_HOME=\"/usr/local/extras/Genomics/apps/java/current\"\
\\nexport BEAST_PACKAGE_PATH=\"/usr/local/extras/Genomics/apps/beast/2.5.1\"' > {}.tmp;
mv {}.tmp {}"

# Edit treeannotator and beauti increase java max memory to 8Gb
perl -pi -e 's/-Xmx4g/-Xmx8g/g' beauti
perl -pi -e 's/-Xmx4g/-Xmx8g/g' treeannotator

# Add path to beagle library in beast
cat beast | sed -e '38i\\nBEAGLE_LIB="/usr/local/extras/Genomics/lib/beagle/current/lib"' > beast.tmp
mv beast.tmp beast

# Make versions to use more memory: beast_mem8g & beast_mem12g
cat beast | perl -pe 's/-Xmx4g/-Xmx8g/g' > beast_mem8g
cat beast | perl -pe 's/-Xmx4g/-Xmx12g/g' > beast_mem12g
cat beast | perl -pe 's/-Xmx4g/-Xmx16g/g' > beast_mem16g

# Make them executable
chmod +x *

# Create links to allow it being used along with beast 1.x
ls * | xargs -I {} sh -c 'ln -s {} {}2'

cd ..
rm -rf /usr/local/extras/Genomics/apps/beast/2.5.1 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/beast/2.5.1
cp -r * /usr/local/extras/Genomics/apps/beast/2.5.1

cd /usr/local/extras/Genomics/apps/beast
rm current2
ln -sTf 2.5.1 current2
ln -sf /usr/local/extras/Genomics/apps/beast/current2/bin/*2 /usr/local/extras/Genomics/bin/

# install all packages
# note: this might be unreliable and manual checking is advised
/usr/local/extras/Genomics/apps/beast/2.5.1/bin/packagemanager -list | \
tail -n+5 | awk '{print $1}' | \
xargs -I {} sh -c '
    /usr/local/extras/Genomics/apps/beast/2.5.1/bin/packagemanager -dir /usr/local/extras/Genomics/apps/beast/2.5.1 -del {}
	/usr/local/extras/Genomics/apps/beast/2.5.1/bin/packagemanager -dir /usr/local/extras/Genomics/apps/beast/2.5.1 -add {}'

cd $CURRDIR
rm -rf BEAST.v2.5.1.linux.tgz beast >& /dev/null

