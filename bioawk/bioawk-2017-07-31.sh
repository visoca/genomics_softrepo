#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o bioawk-2017-07-31.log

CURRDIR=$PWD
rm -rf bioawk >& /dev/null

git clone https://github.com/lh3/bioawk.git
cd bioawk
make
make clean
rm -rf /usr/local/extras/Genomics/apps/bioawk/2017-07-31 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/bioawk/2017-07-31
cp -r * /usr/local/extras/Genomics/apps/bioawk/2017-07-31/

cd /usr/local/extras/Genomics/apps/bioawk
rm current >& /dev/null
ln -s 2017-07-31 current
ln -s /usr/local/extras/Genomics/apps/bioawk/current/bioawk /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf bioawk
