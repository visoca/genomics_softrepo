#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -V
#$ -o bioawk-2017-12-09.log

# load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf bioawk >& /dev/null

git clone https://github.com/lh3/bioawk.git
cd bioawk
make
make clean
rm -rf /usr/local/extras/Genomics/apps/bioawk/2017-12-09 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/bioawk/2017-12-09
cp -r * /usr/local/extras/Genomics/apps/bioawk/2017-12-09/

cd /usr/local/extras/Genomics/apps/bioawk
rm current >& /dev/null
ln -sTf 2017-12-09 current
ln -sf /usr/local/extras/Genomics/apps/bioawk/current/bioawk /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf bioawk
