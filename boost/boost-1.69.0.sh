#!/bin/bash
#$ -pe smp 4
#$ -j y
#$ -o boost-1.69.0.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf boost_1_69_0.tar.gz boost_1_69_0 >& /dev/null

wget https://dl.bintray.com/boostorg/release/1.69.0/source/boost_1_69_0.tar.gz
tar -xf boost_1_69_0.tar.gz
cd boost_1_69_0
./bootstrap.sh --prefix=/usr/local/extras/Genomics/lib/boost/1.69.0
./b2 -j4
bjam --prefix=/usr/local/extras/Genomics/lib/boost/1.69.0 --toolset=gcc architecture=x86 address_model=64 link=static runtime-link=static stage install
./b2 install

cd /usr/local/extras/Genomics/lib/boost
ln -s 1.69.0 current
ln -s /usr/local/extras/Genomics/lib/boost/current/lib/* /usr/local/extras/Genomics/lib/lib/
ln -s /usr/local/extras/Genomics/lib/boost/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf boost_1_69_0.tar.gz boost_1_69_0

echo "=============================================================================="
