#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -l h_rt=1:00:00
#$ -j y
#$ -V
#$ -o bowtie2-2.3.4.1.log

hostname
date
echo "==============================================================================="

# load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
	module add libs/binlibs/intel-tbb/2017.0
else # ShARC
	module add dev/intel-compilers/17.0.0
	module add libs/intel-tbb/2017.0/binary
fi

CURRDIR=$PWD

rm -rf bowtie2-2.3.4.1 bowtie2-2.3.4.1.tar.gz >& /dev/null

wget https://github.com/BenLangmead/bowtie2/archive/v2.3.4.1.tar.gz -O bowtie2-2.3.4.1.tar.gz
tar -xf bowtie2-2.3.4.1.tar.gz
cd bowtie2-2.3.4.1
perl -pe 's/SSE_FLAG\=\-msse2/SSE_FLAG\=\-sse4.2/g' Makefile
make

# add path to intel TBB libraries
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	sed -i '3i BEGIN\n{\n\t$ENV{LD_LIBRARY_PATH}="/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LD_LIBRARY_PATH";\n}' bowtie2
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-build
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-inspect
else # ShARC
	sed -i '3i BEGIN\n{\n\t$ENV{LD_LIBRARY_PATH}="/usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary//compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LD_LIBRARY_PATH";\n}' bowtie2
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary//compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-build
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary//compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-inspect
fi

mkdir bin
mv bowtie2* bin/
make clean
rm -rf /usr/local/extras/Genomics/apps/bowtie2/2.3.4.1 >& /dev/null

mkdir -p /usr/local/extras/Genomics/apps/bowtie2/2.3.4.1
cp -r * /usr/local/extras/Genomics/apps/bowtie2/2.3.4.1/
cd /usr/local/extras/Genomics/apps/bowtie2
rm current >& /dev/null
ln -sTf 2.3.4.1 current
ln -sf /usr/local/extras/Genomics/apps/bowtie2/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf bowtie2-2.3.4.1 bowtie2-2.3.4.1.tar.gz

echo "==============================================================================="
date
