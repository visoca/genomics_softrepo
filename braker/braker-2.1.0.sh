#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 4
#$ -l rmem=2g
#$ -j y
#$ -o braker-2.1.0.log


# NOTE: Braker is not included in the list of programs than can be
#       called from the command line, because annotation software
#       usually requires choosing carefully the version of the
#       programs to be used

# NOTE2: This program depends on the following programs:
#		   GeneMark-EX
#          AUGUSTUS
#          Bamtools
#          NCBI BLAST+
#        Optional tools:
#          samtools
#          GenomeThreader*
#          Spaln*
#          Exonerate*
#          ProSplign**
#
#    *  GenomeThreader is recommended over Spaln and Exonerate and it
#       is the only one that is installed with this script
#    ** ProSplign is required to process blastp hits and get intros and
#       protein hints to be used when using remote species

source /usr/local/extras/Genomics/.bashrc

# This is required for some Perl modules
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/gcc/4.9.2
else # ShARC
	module add dev/gcc/4.9.4
fi

# install cpan modules
cpanm File::Spec::Functions Hash::Merge List::Util Logger::Simple \
Module::Load::Conditional Parallel::ForkManager POSIX \
Scalar::Util::Numeric YAML

CURRDIR=$PWD
rm -rf BRAKER2.tar.gz BRAKER_v2.1.0 >& /dev/null

wget http://bioinf.uni-greifswald.de/augustus/binaries/BRAKER2.tar.gz
tar -xf BRAKER2.tar.gz
cd BRAKER_v2.1.0
chmod a+x *.pl

# Install dependent programs
# =============================================================================
# SAMTOOLS 1.7-4-g9386ed ??
# Prosplign
mkdir deps

# Genemark-EX (ES-ET-EP) 4.33
# -----------------------------------------------------------------------------
rm -rf gm_et_linux_64.tar.gz gm_key_64.gz gm_et_linux_64 >& /dev/null
wget http://topaz.gatech.edu/GeneMark/tmp/GMtool_iMER2/gm_et_linux_64.tar.gz
tar -xf gm_et_linux_64.tar.gz
cd gm_et_linux_64/gmes_petap/
wget http://topaz.gatech.edu/GeneMark/tmp/GMtool_iMER2/gm_key_64.gz
gzip -d gm_key_64.gz
find . -name "*.pl" | xargs -I {} sh -c 'perl -pi -e "s/\/usr\/bin\/perl/\/usr\/bin\/env perl/g" {}' 
cd ../..
mkdir deps/genemark-ex-4.33
cp -r gm_et_linux_64/gmes_petap/* deps/genemark-ex-4.33/
rm -rf gm_et_linux_64.tar.gz gm_key_64.gz gm_et_linux_64 >& /dev/null
# -----------------------------------------------------------------------------

# NCBI BLAST+ 2.2.31
# -----------------------------------------------------------------------------
rm -rf ncbi-blast-2.2.31+-x64-linux.tar.gz ncbi-blast-2.2.31+ >& /dev/null
wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.31/ncbi-blast-2.2.31+-x64-linux.tar.gz
tar -xf ncbi-blast-2.2.31+-x64-linux.tar.gz
mv ncbi-blast-2.2.31+ deps/
rm -rf ncbi-blast-2.2.31+-x64-linux.tar.gz ncbi-blast-2.2.31+ >& /dev/null
# -----------------------------------------------------------------------------

# BAMTOOLS 2.5.1
# -----------------------------------------------------------------------------
rm -rf bamtools-2.5.1.tar.gz bamtools-2.5.1 >& /dev/null
wget https://github.com/pezmaster31/bamtools/archive/v2.5.1.tar.gz -O bamtools-2.5.1.tar.gz
tar -xf bamtools-2.5.1.tar.gz
cd bamtools-2.5.1
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=../../deps/bamtools-2.5.1 ..
make -j4
make install
cd ../..
rm -rf bamtools-2.5.1.tar.gz bamtools-2.5.1 >& /dev/null
# -----------------------------------------------------------------------------

# AUGUSTUS 3.3
# -----------------------------------------------------------------------------
rm -rf augustus-3.3.1.tar.gz augustus-3.3.1 >& /dev/null
wget http://bioinf.uni-greifswald.de/augustus/binaries/augustus-3.3.1.tar.gz
tar -xf augustus-3.3.1.tar.gz
cd augustus-3.3.1
# change installation path (and avoid links to system dirs)
# INSTALLDIR = /opt/augustus-$(AUGVERSION)
perl -pi -e 's/^AUGVERSION.*/AUGVERSION \= 3\.3\.1/g' common.mk
perl -pi -e 's/^INSTALLDIR.*/INSTALLDIR \= \.\.\/deps\/augustus-\$\(AUGVERSION\)/g' Makefile
perl -pi -e 's/^\tln \-sf \$\(INSTALLDIR\)/\# \tln \-sf \$\(INSTALLDIR\)/g' Makefile

# add path to bamtools
# BAMTOOLSPATH=$($PWD/..//deps\/bamtools-2.5.1)
perl -pi -e 's/^INCLUDES.*/INCLUDES \= \.\.\/\.\.\/\.\.\/deps\/bamtools-2.5.1\/include\/bamtools/g' auxprogs/bam2hints/Makefile
perl -pi -e 's/(^LIBS.*)/$1 -L\.\.\/\.\.\/\.\.\/deps\/bamtools-2.5.1\/lib64/g' auxprogs/bam2hints/Makefile

perl -pi -e 's/^BAMTOOLS.*/BAMTOOLS \= \.\.\/\.\.\/\.\.\/\.\.\/deps\/bamtools-2.5.1\/include\/bamtools/g' auxprogs/filterBam/src/Makefile
perl -pi -e 's/(^INCLUDES.*)/$1 -I\.bamtools/g' auxprogs/filterBam/src/Makefile
perl -pi -e 's/(^LIBS.*)/$1 -L\.\.\/\.\.\/\.\.\/\.\.\/deps\/bamtools-2.5.1\/lib64/g' auxprogs/filterBam/src/Makefile

# Old htslib/samtools libraries are required for bam2wig
# .............................................................................
cd auxprogs/bam2wig
# htslib
wget https://github.com/samtools/htslib/archive/0.2.0-rc7.tar.gz -O htslib-0.2.0-rc7.tar.gz
tar -xf htslib-0.2.0-rc7.tar.gz
cd htslib-0.2.0-rc7
make -j4
cd ..
ln -s htslib-0.2.0-rc7 htslib
# samtools 
wget https://github.com/samtools/samtools/archive/0.2.0-rc7.tar.gz -O samtools-0.2.0-rc7.tar.gz
tar -xf samtools-0.2.0-rc7.tar.gz
cd samtools-0.2.0-rc7
make -j4
cd ..
ln -s samtools-0.2.0-rc7 samtools

# bcfools and tabix don't really seem necessary
wget https://github.com/samtools/bcftools/archive/0.2.0-rc7.tar.gz -O bcftools-0.2.0-rc7.tar.gz
tar -xf bcftools-0.2.0-rc7.tar.gz
cd bcftools-0.2.0-rc7
make -j4
cd ..
ln -s bcftools-0.2.0-rc7 bcftools
git clone https://github.com/samtools/tabix
cd tabix
make -j4
cd ..
perl -pi -e 's/ifndef TOOLDIR/TOOLDIR \= \.\/\nifndef TOOLDIR/' Makefile
perl -pi -e 's/(^LIBS.*)/$1 -L\/usr\/local\/extras\/Genomics\/lib\/lib/g' Makefile
cd ../..
# .............................................................................

# utrrnaseq
perl -pi -e 's/\-I\/usr\/include\/boost/\-I\/usr\/local\/extras\/Genomics\/lib\/include/g' auxprogs/utrrnaseq/Debug/src/subdir.mk

# change perl scripts
find . -name "*.pl" | xargs -I {} sh -c 'perl -pi -e "s/\/usr\/bin\/perl/\/usr\/bin\/env perl/g" {}' 

make -j4
make install
cd ..
rm -rf augustus-3.3.1.tar.gz augustus-3.3.1 >& /dev/null
# -----------------------------------------------------------------------------

# SAMTOOLS 1.8
# -----------------------------------------------------------------------------
# use system-wide one
# -----------------------------------------------------------------------------

# GenomeThreader 1.7.0
# -----------------------------------------------------------------------------
rm -rf gth-1.7.0-Linux_x86_64-64bit.tar.gz gth-1.7.0-Linux_x86_64-64bit >& /dev/null
wget http://genomethreader.org/distributions/gth-1.7.0-Linux_x86_64-64bit.tar.gz
tar -xf gth-1.7.0-Linux_x86_64-64bit.tar.gz
mkdir deps/genomethreader-1.7.0
cp -r gth-1.7.0-Linux_x86_64-64bit/* deps/genomethreader-1.7.0/
rm -rf gth-1.7.0-Linux_x86_64-64bit.tar.gz gth-1.7.0-Linux_x86_64-64bit >& /dev/null
# -----------------------------------------------------------------------------

# prosplign
# -----------------------------------------------------------------------------
# NOTE: This requires loading intel compiler libraries and theBerkeley DB library

# Install Berkeley DB library
wget ftp://ftp.pbone.net/mirror/rnd.rajven.net/centos/6.5/extras/x86_64/libdb4-4.8.30-5cnt6.x86_64.rpm
rpm2cpio libdb4-4.8.30-5cnt6.x86_64.rpm | cpio -idmv
mkdir deps/libdb-4.8.3
mv ./usr/lib64/* deps/libdb-4.8.3/
rm -rf usr
cd deps/libdb-4.8.3
ln -s libdb-4.8.so libdb-4.6.so
cd ../..

rm -rf prosplign.tar.gz >& /dev/null
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/TOOLS/ProSplign/prosplign.tar.gz
tar -xf prosplign.tar.gz
mkdir deps/prosplign
mv prosplign deps/prosplign/prosplign.bin
mv procompart deps/prosplign/procompart.bin

# wrapper for prosplign
# ............................................................
cat << 'EOF' > deps/prosplign/prosplign.sh
#!/bin/bash

DIR=$(dirname $0)

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]]; 
then
	module add compilers/intel/17.0.0
else
	module add dev/intel-compilers/17.0.0
fi

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DIR/../libdb-4.8.3

$DIR/prosplign.bin $*
EOF
chmod +x deps/prosplign/prosplign.sh
# ............................................................

# wrap for procompart
# ............................................................
cat << 'EOF' > deps/prosplign/procompart.sh
#!/bin/bash

DIR=$(dirname $0)

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]]; 
then
	module add compilers/intel/17.0.0
else
	module add dev/intel-compilers/17.0.0
fi

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DIR/../libdb-4.8.3/

$DIR/procompart.bin $*
EOF

chmod +x deps/prosplign/procompart.sh
# ............................................................

cd deps/prosplign
ln -s prosplign.sh prosplign
ln -s procompart.sh procompart
cd ../..

rm -rf prosplign.tar.gz >& /dev/null
# -----------------------------------------------------------------------------

# =============================================================================

rm -rf /usr/local/extras/Genomics/apps/braker/2.1.0 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/braker/2.1.0
cp -r * /usr/local/extras/Genomics/apps/braker/2.1.0/

cd $CURRDIR
rm -rf BRAKER2.tar.gz BRAKER_v2.1.0
