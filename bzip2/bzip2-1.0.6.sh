#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o bzip2-1.0.6.log

CURRDIR=$PWD
rm -rf bzip2-1.0.6 bzip2-1.0.6.tar.gz >& /dev/null

wget http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
tar -xf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make CFLAGS='-fPIC -Wall -Winline -O2 -g $(BIGFILES)'
make -f Makefile-libbz2_so
rm -rf /usr/local/extras/Genomics/apps/bzip2/1.0.6 >& /dev/null
make install PREFIX=/usr/local/extras/Genomics/apps/bzip2/1.0.6
cp bzip2-shared /usr/local/extras/Genomics/apps/bzip2/1.0.6/bin
cp libbz2.so* /usr/local/extras/Genomics/apps/bzip2/1.0.6/lib

cd /usr/local/extras/Genomics/apps/bzip2
ln -sTf 1.0.6 current
ln -sf /usr/local/extras/Genomics/apps/bzip2/current/bin/* /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/bzip2/current/lib/* /usr/local/extras/Genomics/lib/lib/
ln -sf /usr/local/extras/Genomics/apps/bzip2/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf bzip2-1.0.6 bzip2-1.0.6.tar.gz
