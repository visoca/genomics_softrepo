#!/bin/bash
##$ -P molecosh
##$ -q molecosh.q
#$ -j y
#$ -o canu-1.7.log


# if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
# then # Iceberg
# 	module add compilers/intel/17.0.0
# 	module add mpi/intel/openmpi/1.10.0
# else # ShARC
# 	module add dev/intel-compilers/17.0.0
# 	module add mpi/openmpi/2.0.1/intel-17.0.0
# fi

CURRDIR=$PWD
rm -rf canu-1.7.tar.gz canu-1.7 >& /dev/null
wget https://github.com/marbl/canu/archive/v1.7.tar.gz -O canu-1.7.tar.gz
tar -xf canu-1.7.tar.gz
cd canu-1.7/src
rm -rf /usr/local/extras/Genomics/apps/canu/1.7 >& /dev/null
make TARGET_DIR=/usr/local/extras/Genomics/apps/canu/1.7

cd /usr/local/extras/Genomics/apps/canu
ln -sTf 1.7 current
ln -sf /usr/local/extras/Genomics/apps/canu/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf canu-1.7.tar.gz canu-1.7
