#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o checkm.log

conda remove -n py27checkm --all -y
conda create -n py27checkm python=2.7 anaconda -y
source activate py27checkm
conda install -c numpy checkm-genome

rm -rf /usr/local/extras/Genomics/apps/checkm/2018-02-09 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/checkm/2018-02-09
cd /usr/local/extras/Genomics/apps/checkm/2018-02-09
wget https://data.ace.uq.edu.au/public/CheckM_databases/checkm_data_2015_01_16.tar.gz
tar -xv CheckM_databases/checkm_data_2015_01_16.tar.gz
cd /usr/local/extras/Genomics/apps/checkm
ln -sTf 2018-02-09 current

checkm data setRoot /usr/local/extras/Genomics/apps/checkm/current

