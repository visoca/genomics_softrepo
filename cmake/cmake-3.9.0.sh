#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 4
#$ -V
#$ -pe smp 4
#$ -j y
#$ -o cmake-3.9.0.log

hostname
date
echo "==============================================================================="

# load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf cmake-3.9.0 cmake-3.9.0.tar.gz >& /dev/null

wget https://cmake.org/files/v3.9/cmake-3.9.0.tar.gz
tar -xf cmake-3.9.0.tar.gz
cd cmake-3.9.0

./bootstrap \
--prefix=/usr/local/extras/Genomics/apps/cmake/3.9.0 \
--parallel=4

make -j4
rm -rf /usr/local/extras/Genomics/apps/cmake/3.9.0 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/cmake
rm current >& /dev/null
ln -s 3.9.0 current
ln -s /usr/local/extras/Genomics/apps/cmake/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf cmake-3.9.0 cmake-3.9.0.tar.gz

echo "==============================================================================="
date
