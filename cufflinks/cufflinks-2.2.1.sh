#!/bin/bash
#$ -j y
#$ -o cufflinks-2.2.1.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf cufflinks-2.2.1.Linux_x86_64 cufflinks-2.2.1.Linux_x86_64.tar.gz >& /dev/null

wget http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/cufflinks-2.2.1.Linux_x86_64.tar.gz
tar -xvf cufflinks-2.2.1.Linux_x86_64.tar.gz
cd cufflinks-2.2.1.Linux_x86_64
mkdir bin
find . -maxdepth 1 -type f -executable | xargs -I {} sh -c 'mv {} bin/'

rm -rf /usr/local/extras/Genomics/apps/cufflinks/2.2.1 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/cufflinks/2.2.1
cp -r * /usr/local/extras/Genomics/apps/cufflinks/2.2.1/

cd /usr/local/extras/Genomics/apps/cufflinks
ln -sTf 2.2.1 current
ln -sf /usr/local/extras/Genomics/apps/cufflinks/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf cufflinks-2.2.1.Linux_x86_64 cufflinks-2.2.1.Linux_x86_64.tar.gz

echo "=============================================================================="
