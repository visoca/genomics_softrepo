#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o cutadapt-1.16.log

conda remove -n py36cutadapt --all -y
conda create -n py36cutadapt python=3.6 anaconda -y
source activate py36cutadapt

conda install -c bioconda cutadapt

rm -rf /usr/local/extras/Genomics/apps/cutadapt/1.16 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/cutadapt/1.16

cd /usr/local/extras/Genomics/apps/cutadapt/1.16

# wrapper
cat > cutadapt.sh << 'EOF'
#!/bin/bash

source activate py36cutadapt

cutadapt $*

EOF

chmod +x cutadapt.sh
ln -s cutadapt.sh cutadapt

cd ..
ln -sTf 1.16 current
ln -sf /usr/local/extras/Genomics/apps/cutadapt/current/cutadapt /usr/local/extras/Genomics/bin/
