#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o expat-2.2.2.log

CURRDIR=$PWD

# Expat
# --------------------------------------------------------------
rm -rf libexpat-R_2_2_2 libexpat-R_2_2_2.tar.gz >& /dev/null

wget https://github.com/libexpat/libexpat/archive/R_2_2_2.tar.gz -O libexpat-R_2_2_2.tar.gz
tar -xf libexpat-R_2_2_2.tar.gz
cd libexpat-R_2_2_2/expat
./buildconf.sh
./configure \
CPPFLAGS="-DXML_LARGE_SIZE -DXML_POOR_ENTROPY" \
--prefix=/usr/local/extras/Genomics/lib/expat/2.2.2
make -j12
# make check
rm -rf /usr/local/extras/Genomics/lib/expat/2.2.2 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/expat
rm current
ln -s 2.2.2 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/expat/current/bin/* bin/
ln -s /usr/local/extras/Genomics/lib/expat/current/lib/* lib/
ln -s /usr/local/extras/Genomics/lib/expat/current/lib/pkgconfig/* lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/expat/current/include/* include/

cd $CURRDIR
rm -rf libexpat-R_2_2_2 libexpat-R_2_2_2.tar.gz
# --------------------------------------------------------------

