#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -P molecosh
##$ -q molecosh.q
#$ -j y
#$ -o falcon.log

# create environment
/usr/local/extras/Genomics/apps/anaconda/bin/conda remove -n py27_falcon --all -y >& /dev/null
/usr/local/extras/Genomics/apps/anaconda/bin/conda create -n py27_falcon python=2.7 anaconda -y

# activate environment
source /usr/local/extras/Genomics/apps/anaconda/bin/activate py27_falcon

# install virtualenv
/usr/local/extras/Genomics/apps/anaconda/bin/conda install virtualenv -y

# install falcon (using virtualenv)
rm -f install_unzip.sh >& /dev/null

wget http://pb-falcon.readthedocs.io/en/latest/_downloads/install_unzip.sh
FALCONFILE=$(grep "^UNZIP_TARBALL" install_unzip.sh | awk -F'"' '{print $2}' | perl -pe 's/\-ucs.*/\-ucs\*.tar.gz/g')
FALCONVERSION=$(grep "^UNZIP_TARBALL" install_unzip.sh | awk -F"-" '{print $2"-"$3}')

rm -rf $FALCONFILE /usr/local/extras/Genomics/apps/falcon/${FALCONVERSION} >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/falcon/${FALCONVERSION} >& /dev/null
bash -ex install_unzip.sh /usr/local/extras/Genomics/apps/falcon/${FALCONVERSION}

cd /usr/local/extras/Genomics/apps/falcon/
ln -sTf ${FALCONVERSION} current

# To use falcon run:
# source /usr/local/extras/Genomics/apps/falcon/current/activate
# to stop using it, simply run:
# deactivate

rm -rf install_unzip.sh $FALCONFILE
