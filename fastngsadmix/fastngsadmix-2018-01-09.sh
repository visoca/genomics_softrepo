#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -l h_rt=1:00:00
#$ -j y
#$ -V
#$ -o fastngsadmix-2018-01-09.log

hostname
date
echo "==============================================================================="

# load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/gcc/4.8.2
fi

CURRDIR=$PWD

rm -rf fastNGSadmix >& /dev/null

git clone https://github.com/e-jorsboe/fastNGSadmix.git
cd fastNGSadmix
make
mkdir bin
mv fastNGSadmix bin/
cd bin
ln -s fastNGSadmix fastngsadmix
cd ..
make clean
rm -rf /usr/local/extras/Genomics/apps/fastngsadmix/2018-01-09 >& /dev/null

mkdir -p /usr/local/extras/Genomics/apps/fastngsadmix/2018-01-09
cp -r * /usr/local/extras/Genomics/apps/fastngsadmix/2018-01-09
cd /usr/local/extras/Genomics/apps/fastngsadmix
ln -sTf 2018-01-09 current
ln -sf /usr/local/extras/Genomics/apps/fastngsadmix/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf fastNGSadmix

echo "==============================================================================="
date
