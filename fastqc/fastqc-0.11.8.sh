#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 12
#$ -j y
#$ -o fastqc-0.11.8.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf FastQC fastqc_v0.11.8.zip >& /dev/null


wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.8.zip
unzip fastqc_v0.11.8.zip
cd FastQC
chmod +x fastqc
rm -rf /usr/local/extras/Genomics/apps/fastqc/0.11.8 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/fastqc/0.11.8
cp -r * /usr/local/extras/Genomics/apps/fastqc/0.11.8/
cd /usr/local/extras/Genomics/apps/fastqc
ln -sTf 0.11.8 current
ln -s /usr/local/extras/Genomics/apps/fastqc/current/fastqc /usr/local/extras/Genomics/bin/


cd $CURRDIR
rm -rf FastQC fastqc_v0.11.8.zip

echo "=============================================================================="
