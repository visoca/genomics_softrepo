#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o fastsimcoal-2.5.2.21.log

CURRDIR=$PWD
rm -rf fsc_linux64.zip fsc_linux64  >& /dev/null

wget http://cmpg.unibe.ch/software/fastsimcoal2/downloads/fsc_linux64.zip
unzip fsc_linux64.zip
cd fsc_linux64
mkdir scripts
cd scripts
wget http://cmpg.unibe.ch/software/fastsimcoal2/R/ParFileInterpreter-v6.3.r
dos2unix ParFileInterpreter-v6.3.r
# add shebang
echo -e '#!/usr/local/extras/Genomics/bin/Rscript' | cat - ParFileInterpreter-v6.3.r > /tmp/out
mv /tmp/out ParFileInterpreter-v6.3.r
cd ..
mkdir bin
mv fsc25221 bin
cd bin
chmod +x fsc25221
ln -s fsc25221 fastsimcoal
ln -s fsc25221 fsc
cd ..

rm -rf /usr/local/extras/Genomics/apps/fastsimcoal/2.5.2.21 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/fastsimcoal/2.5.2.21
cp -r * /usr/local/extras/Genomics/apps/fastsimcoal/2.5.2.21

cd /usr/local/extras/Genomics/apps/fastsimcoal
ln -sfT 2.5.2.21 current
ln -sf /usr/local/extras/Genomics/apps/fastsimcoal/current/bin/fastsimcoal /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/fastsimcoal/current/bin/fsc /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf fsc_linux64.zip fsc_linux64
