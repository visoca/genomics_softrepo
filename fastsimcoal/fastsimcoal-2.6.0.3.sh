#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o fastsimcoal-2.6.0.3.log

CURRDIR=$PWD
rm -rf fsc26_linux64.zip fsc26_linux64  >& /dev/null

wget http://cmpg.unibe.ch/software/fastsimcoal2/downloads/fsc26_linux64.zip
unzip fsc26_linux64.zip
cd fsc26_linux64
mkdir scripts
cd scripts
wget http://cmpg.unibe.ch/software/fastsimcoal2/R/ParFileInterpreter-v6.3.1.r
dos2unix ParFileInterpreter-v6.3.1.r
# add shebang
echo -e '#!/usr/local/extras/Genomics/bin/Rscript' | cat - ParFileInterpreter-v6.3.1.r > /tmp/out
mv /tmp/out ParFileInterpreter-v6.3.1.r
cd ..
mkdir bin
mv fsc26 bin
cd bin
chmod +x fsc26
ln -s fsc26 fastsimcoal
ln -s fsc26 fsc
cd ..

rm -rf /usr/local/extras/Genomics/apps/fastsimcoal/2.6.0.3 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/fastsimcoal/2.6.0.3
cp -r * /usr/local/extras/Genomics/apps/fastsimcoal/2.6.0.3

cd /usr/local/extras/Genomics/apps/fastsimcoal
ln -sfT 2.6.0.3 current
ln -sf /usr/local/extras/Genomics/apps/fastsimcoal/current/bin/fastsimcoal /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/fastsimcoal/current/bin/fsc /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf fsc26_linux64.zip fsc26_linux64
