#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o flash-1.2.11.log

CURRDIR=$PWD
rm -rf FLASH-1.2.11.tar.gz FLASH-1.2.11 >& /dev/null

wget https://sourceforge.net/projects/flashpage/files/FLASH-1.2.11.tar.gz
tar -xf FLASH-1.2.11.tar.gz
cd FLASH-1.2.11
make
mkdir bin
mv flash bin/
make clean
rm -rf /usr/local/extras/Genomics/apps/flash/1.2.11 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/flash/1.2.11
cp -r * /usr/local/extras/Genomics/apps/flash/1.2.11/

cd /usr/local/extras/Genomics/apps/flash
rm current >& /dev/null
ln -s 1.2.11 current
ln -s /usr/local/extras/Genomics/apps/flash/current/bin/flash /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf FLASH-1.2.11.tar.gz FLASH-1.2.11

