#!/bin/bash
#$ -j y
#$ -o fmlrc-1.0.0.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf fmlrc-1.0.0.tar.gz fmlrc-1.0.0 >& /dev/null

wget https://github.com/holtjma/fmlrc/archive/v1.0.0.tar.gz -O fmlrc-1.0.0.tar.gz
tar -xf fmlrc-1.0.0.tar.gz
cd fmlrc-1.0.0
make
mkdir bin
mv fmlrc* bin/
make clean

rm -rf /usr/local/extras/Genomics/apps/fmlrc/1.0.0 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/fmlrc/1.0.0
cp -r * /usr/local/extras/Genomics/apps/fmlrc/1.0.0/

cd /usr/local/extras/Genomics/apps/fmlrc
rm current >& /dev/null
ln -sTf 1.0.0 current
ln -s /usr/local/extras/Genomics/apps/fmlrc/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf fmlrc-1.0.0.tar.gz fmlrc-1.0.0

echo "=============================================================================="
