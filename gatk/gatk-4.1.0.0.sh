#!/bin/bash
#$ -j y
#$ -o gatk-4.1.0.0.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD

rm -rf gatk-4.1.0.0 gatk-4.1.0.0.zip >& /dev/null
wget https://github.com/broadinstitute/gatk/releases/download/4.1.0.0/gatk-4.1.0.0.zip
unzip gatk-4.1.0.0.zip
cd gatk-4.1.0.0

rm -rf /usr/local/extras/Genomics/apps/gatk/4.1.0.0 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/gatk/4.1.0.0
cp -r * /usr/local/extras/Genomics/apps/gatk/4.1.0.0/

cd /usr/local/extras/Genomics/apps/gatk
ln -sfT 4.1.0.0 current
ln -sf /usr/local/extras/Genomics/apps/gatk/current/gatk /usr/local/extras/Genomics/bin/ 

cd $CURRDIR
rm -rf gatk-4.1.0.0 gatk-4.1.0.0.zip

echo "=============================================================================="

