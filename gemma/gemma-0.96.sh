#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -l h_rt=00:5:00
#$ -V
#$ -j y
#$ -o gemma-0.96.log

hostname
date

rm -rf gemma.linux.gz >& /dev/null

wget https://github.com/genetics-statistics/GEMMA/releases/download/v0.96/gemma.linux.gz
gzip -d gemma.linux.gz
mv gemma.linux gemma
chmod +x gemma
rm -rf /usr/local/extras/Genomics/apps/gemma/0.96 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/gemma/0.96
mv gemma /usr/local/extras/Genomics/apps/gemma/0.96/

cd /usr/local/extras/Genomics/apps/gemma
ln -sTf 0.96 current
ln -sf /usr/local/extras/Genomics/apps/gemma/current/gemma /usr/local/extras/Genomics/bin/

date
