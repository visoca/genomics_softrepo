#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -pe openmp 12
#$ -l h_rt=2:00:00
#$ -j y
#$ -o geos-3.6.1-proj-4.9.3-gdal-2.2.1.log

# use intel compiler
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
else # ShARC
	module add dev/intel-compilers/17.0.0
fi

CURRDIR=$PWD

# download ESRI FileGDB API
# --------------------------------------------------------------
rm -rf FileGDB_API-64 FileGDB_API_1_5_64.tar.gz >& /dev/null
wget https://github.com/Esri/file-geodatabase-api/raw/master/FileGDB_API_1.5/FileGDB_API_1_5_64.tar.gz
tar -xvf FileGDB_API_1_5_64.tar.gz
cd FileGDB_API-64
rm -rf /usr/local/extras/Genomics/lib/filegdb-api-64/1.5 >& /dev/null
mkdir -p /usr/local/extras/Genomics/lib/filegdb-api-64/1.5
cp -r * /usr/local/extras/Genomics/lib/filegdb-api-64/1.5/
cd /usr/local/extras/Genomics/lib/filegdb-api-64/
rm current >& /dev/null
ln -s 1.5 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/filegdb-api-64/current/lib/* lib/ 
ln -s /usr/local/extras/Genomics/lib/filegdb-api-64/current/include/* include/

cd $CURRDIR
rm -rf FileGDB_API-64 FileGDB_API_1_5_64.tar.gz
# --------------------------------------------------------------


# geos
# --------------------------------------------------------------
cd $CURRDIR
rm -rf geos-3.6.1 geos-3.6.1.tar.bz2 >& /dev/null

wget http://download.osgeo.org/geos/geos-3.6.1.tar.bz2
tar -xf geos-3.6.1.tar.bz2
cd geos-3.6.1
./configure \
--prefix=/usr/local/extras/Genomics/lib/geos/3.6.1
make -j 1 
make check
rm -rf /usr/local/extras/Genomics/lib/geos/3.6.1 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/geos
rm current >& /dev/null
ln -s 3.6.1 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/geos/current/bin/* bin/ 
ln -s /usr/local/extras/Genomics/lib/geos/current/lib/* lib/ 
ln -s /usr/local/extras/Genomics/lib/geos/current/lib/pkgconfig/* lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/geos/current/include/* include/

cd $CURRDIR
rm -rf geos-3.6.1 geos-3.6.1.tar.bz2
# --------------------------------------------------------------

# proj.4
# --------------------------------------------------------------
cd $CURRDIR
rm -rf proj-4.9.3 proj-4.9.3.tar.gz >& /dev/null

wget http://download.osgeo.org/proj/proj-4.9.3.tar.gz
tar -xf proj-4.9.3.tar.gz
cd proj-4.9.3
# add datum grids
wget http://download.osgeo.org/proj/proj-datumgrid-1.5.tar.gz
tar -xvf proj-datumgrid-1.5.tar.gz -C nad/
./configure \
--prefix=/usr/local/extras/Genomics/lib/proj.4/4.9.3
make -j 12
make check
rm -rf /usr/local/extras/Genomics/lib/proj.4/4.9.3 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/proj.4
rm current >& /dev/null
ln -s 4.9.3 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/proj.4/current/bin/* bin/
ln -s /usr/local/extras/Genomics/lib/proj.4/current/lib/* lib/
ln -s /usr/local/extras/Genomics/lib/proj.4/current/lib/pkgconfig/* lib/pkgconfig
ln -s /usr/local/extras/Genomics/lib/proj.4/current/include/* include/

cd $CURRDIR
rm -rf proj-4.9.3 proj-4.9.3.tar.gz
# --------------------------------------------------------------

# sqlite (required for spatialite)
# --------------------------------------------------------------
cd $CURRDIR
rm -rf sqlite-autoconf-3190300 sqlite-autoconf-3190300.tar.gz >& /dev/null

wget https://www.sqlite.org/2017/sqlite-autoconf-3190300.tar.gz
tar -xf sqlite-autoconf-3190300.tar.gz
cd sqlite-autoconf-3190300
./configure \
--prefix=/usr/local/extras/Genomics/lib/sqlite/3190300 \
--enable-threadsafe \
--enable-readline \
--enable-dynamic-extensions
make -j12
make check
rm -rf /usr/local/extras/Genomics/lib/sqlite/3190300 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/sqlite
rm current >& /dev/null
ln -s 3190300 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/sqlite/current/bin/* bin/
ln -s /usr/local/extras/Genomics/lib/sqlite/current/lib/* lib/
ln -s /usr/local/extras/Genomics/lib/sqlite/current/lib/pkgconfig/* lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/sqlite/current/include/* include/

cd $CURRDIR
rm -rf sqlite-autoconf-3190300 sqlite-autoconf-3190300.tar.gz
# --------------------------------------------------------------

# spatialite (for full spatial support for GDAL)
# --------------------------------------------------------------
cd $CURRDIR
rm -rf libspatialite-4.3.0a libspatialite-4.3.0a.tar.gz >& /dev/null

wget http://www.gaia-gis.it/gaia-sins/libspatialite-4.3.0a.tar.gz
tar -xf libspatialite-4.3.0a.tar.gz
cd libspatialite-4.3.0a
CPPFLAGS="-I/usr/local/extras/Genomics/lib/include" \
LDFLAGS="-L/usr/local/extras/Genomics/lib/lib" \
PKG_CONFIG_PATH="/usr/local/extras/Genomics/lib/lib/pkgconfig" \
./configure \
--prefix=/usr/local/extras/Genomics/lib/spatialite/4.3.0a \
--enable-freexl="no"
make -j12
make check
rm -rf /usr/local/extras/Genomics/lib/spatialite/4.3.0a >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/spatialite
rm current
ln -s 4.3.0a current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/spatialite/current/bin/* bin/
ln -s /usr/local/extras/Genomics/lib/spatialite/current/lib/* lib/
ln -s /usr/local/extras/Genomics/lib/spatialite/current/lib/pkgconfig/* lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/spatialite/current/include/* include/

cd $CURRDIR
rm -rf libspatialite-4.3.0a libspatialite-4.3.0a.tar.gz
# --------------------------------------------------------------

# Xerces-C (required for reading GML files)
# --------------------------------------------------------------
cd $CURRDIR
rm -rf xerces-c-3.1.4 xerces-c-3.1.4.tar.gz >& /dev/null

wget http://mirror.vorboss.net/apache//xerces/c/3/sources/xerces-c-3.1.4.tar.gz
tar -xf xerces-c-3.1.4.tar.gz
cd xerces-c-3.1.4
./configure \
--prefix=/usr/local/extras/Genomics/lib/xerces-c/3.1.4
make -j12
# make check
rm -rf /usr/local/extras/Genomics/lib/xerces-c/3.1.4 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/xerces-c
rm current >& /dev/null
ln -s 3.1.4 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/xerces-c/current/bin/* bin/
ln -s /usr/local/extras/Genomics/lib/xerces-c/current/lib/* lib/
ln -s /usr/local/extras/Genomics/lib/xerces-c/current/lib/pkgconfig/* lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/xerces-c/current/include/* include/

cd $CURRDIR
rm -rf xerces-c-3.1.4 xerces-c-3.1.4.tar.gz
# --------------------------------------------------------------

# Expat (required for reading KML files) - not used in the end
# installed separately using GCC
# --------------------------------------------------------------
# cd /local
# rm -rf libexpat-R_2_2_2* >& /dev/null
#
# wget https://github.com/libexpat/libexpat/archive/R_2_2_2.tar.gz -O libexpat-R_2_2_2.tar.gz
# tar -xf libexpat-R_2_2_2.tar.gz
# cd libexpat-R_2_2_2/expat
# ./buildconf.sh
# ./configure \
# CPPFLAGS="-DXML_LARGE_SIZE -DXML_POOR_ENTROPY" \
# --prefix=/usr/local/extras/Genomics/lib/expat/2.2.2
# make -j12
# # make check
# make install
#
# cd /usr/local/extras/Genomics/lib/expat
# rm current
# ln -s 2.2.2 current
# cd /usr/local/extras/Genomics/lib
# ln -s /usr/local/extras/Genomics/lib/expat/current/bin/* bin/
# ln -s /usr/local/extras/Genomics/lib/expat/current/lib/* lib/
# ln -s /usr/local/extras/Genomics/lib/expat/current/lib/pkgconfig/* lib/pkgconfig/
# ln -s /usr/local/extras/Genomics/lib/expat/current/include/* include/
#
# rm -rf /local/libexpat-R_2_2_2* >& /dev/null
# --------------------------------------------------------------


# gdal
# --------------------------------------------------------------
cd $CURRDIR
rm -rf gdal-2.2.1.tar.gz gdal-2.2.1 >& /dev/null

wget http://download.osgeo.org/gdal/2.2.1/gdal-2.2.1.tar.gz
tar -xf gdal-2.2.1.tar.gz
cd gdal-2.2.1
./configure \
--prefix=/usr/local/extras/Genomics/lib/gdal/2.2.1 \
--with-threads \
--with-spatialite=/usr/local/extras/Genomics/lib/spatialite/current \
--with-xerces=/usr/local/extras/Genomics/lib/xerces-c/current \
--with-libz=internal \
--with-png=internal \
--with-gif=internal \
--with-jpeg=internal \
--with-geotiff=internal \
--with-libtiff=internal \
--with-pcraster=internal \
--with-pcidsk=internal \
--with-qhull=internal \
--with-libjson-c=internal \
--with-hide-internal-symbols \
--with-hdf5=/usr/local/extras/Genomics/lib \
--with-fgdb=/usr/local/extras/Genomics/lib/filegdb-api-64/current \
--with-static-proj4=/usr/local/extras/Genomics/lib/proj.4/current/include \
--without-netcdf
# --with-expat=/usr/local/extras/Genomics/lib/expat/current \

make -j 12
rm -rf /usr/local/extras/Genomics/lib/gdal/2.2.1 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/gdal
rm current >& /dev/null
ln -s 2.2.1 current
cd /usr/local/extras/Genomics/lib
ln -s /usr/local/extras/Genomics/lib/gdal/current/bin/* bin/ 
ln -s /usr/local/extras/Genomics/lib/gdal/current/lib/* lib/ 
ln -s /usr/local/extras/Genomics/lib/gdal/current/lib/pkgconfig/* lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/gdal/current/include/* include/

cd $CURRDIR
rm -rf gdal-2.2.1.tar.gz gdal-2.2.1 >& /dev/null
# --------------------------------------------------------------

