#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 12
#$ -V
#$ -j y
#$ -o gmp-6.1.2.log

hostname
date

source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf gmp-6.1.2 gmp-6.1.2.tar.bz2 >& /dev/null

wget https://gmplib.org/download/gmp/gmp-6.1.2.tar.bz2
tar -xf gmp-6.1.2.tar.bz2
cd gmp-6.1.2

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then
	./configure \
	--prefix=/usr/local/extras/Genomics/lib/gmp/6.1.2 \
	--host=core2-pc-linux-gnu
	# --host=westmere-pc-linux-gnu # this will work in all Iceberg nodes except old amds (testnodes)
else
	./configure \
	--prefix=/usr/local/extras/Genomics/lib/gmp/6.1.2 \
	--host=haswell-pc-linux-gnu

fi

make 
make check
rm -rf /usr/local/extras/Genomics/lib/gmp/6.1.2 >& /dev/null
make install
cd /usr/local/extras/Genomics/lib/gmp
ln -sfT 6.1.2 current
cd /usr/local/extras/Genomics/lib
find /usr/local/extras/Genomics/lib/gmp/current/lib -type f -or -type l | xargs -I {} sh -c 'ln -sf {} lib/'
find /usr/local/extras/Genomics/lib/gmp/current/lib -mindepth 1 -type d | xargs -I {} sh -c 'ln -sfT {} lib/'
find /usr/local/extras/Genomics/lib/gmp/current/include -type f -or -type l | xargs -I {} sh -c 'ln -sf {} include/'
find /usr/local/extras/Genomics/lib/gmp/current/include -mindepth 1 -type d | xargs -I {} sh -c 'ln -sfT {} include/'

cd $CURRDIR
rm -rf gmp-6.1.2 gmp-6.1.2.tar.bz2

date
