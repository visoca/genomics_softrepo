#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o gnu-coreutils-8.27.log

CURRDIR=$PWD
rm -rf coreutils-8.27 coreutils-8.27.tar.xz >& /dev/null

 wget http://ftp.gnu.org/gnu/coreutils/coreutils-8.27.tar.xz
tar -xf coreutils-8.27.tar.xz
cd coreutils-8.27
./configure \
--prefix=/usr/local/extras/Genomics/apps/gnu-coreutils/8.27
make -j12
# make check
rm -rf /usr/local/extras/Genomics/apps/gnu-coreutils/8.27 >& /dev/null
make install
# make installcheck

cd /usr/local/extras/Genomics/apps/gnu-coreutils
ln -sfT 8.27 current
ln -sf /usr/local/extras/Genomics/apps/gnu-coreutils/current/bin/* /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/gnu-coreutils/current/libexec/* /usr/local/extras/Genomics/lib/lib/

cd $CURRDIR
rm -rf coreutils-8.27 coreutils-8.27.tar.xz >& /dev/null
