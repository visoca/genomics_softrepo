#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o gnu-parallel-20170722.log

CURRDIR=$PWD
rm -rf parallel-20170722 parallel-20170722.tar.bz2 >& /dev/null

wget http://ftp.gnu.org/gnu/parallel/parallel-20170722.tar.bz2
tar -xf parallel-20170722.tar.bz2
cd parallel-20170722
./configure \
--prefix=/usr/local/extras/Genomics/apps/gnu-parallel/20170722
make -j12
# make check
rm -rf /usr/local/extras/Genomics/apps/gnu-parallel/20170722 >& /dev/null
make install
# make installcheck

cd /usr/local/extras/Genomics/apps/gnu-parallel
ln -sfT 20170722 current
ln -sf /usr/local/extras/Genomics/apps/gnu-parallel/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf parallel-20170722 parallel-20170722.tar.bz2 >& /dev/null
