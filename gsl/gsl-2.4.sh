#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -V
#$ -j y
#$ -o gsl-2.4.log

ource /usr/local/extras/Genomics/.bashrc

CURRDIR=$PWD
rm -rf gsl-2.4 gsl-2.4.tar.gz >& /dev/null

wget ftp://ftp.gnu.org/gnu/gsl/gsl-2.4.tar.gz
tar -xvf gsl-2.4.tar.gz
cd gsl-2.4
./configure --prefix=/usr/local/extras/Genomics/lib/gsl/2.4
make
make install
cd /usr/local/extras/Genomics/lib/gsl
ln -sTf 2.4 current
cd /usr/local/extras/Genomics/lib
ln -sf /usr/local/extras/Genomics/lib/gsl/current/bin/* bin/ 
ln -sf /usr/local/extras/Genomics/lib/gsl/current/lib/* lib/ 
ln -sf /usr/local/extras/Genomics/lib/gsl/current/lib/pkgconfig/* lib/pkgconfig/
ln -sf /usr/local/extras/Genomics/lib/gsl/current/include/* include/

cd $CURRDIR
rm -rf gsl-2.4 gsl-2.4.tar.gz 
