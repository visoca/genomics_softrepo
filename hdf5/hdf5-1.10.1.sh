#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 4
##$ -pe smp 4
##$ -l h_rt=00:45:00
#$ -V
#$ -j y
#$ -o hdf5-1.10.1.log

hostname
date

source /usr/local/extras/Genomics/.bashrc 

# a local location is required for tests
mkdir -p /scratch/bo1vsx
cd /scratch/bo1vsx

CURRDIR=$PWD
rm -rf hdf5-1.10.1 hdf5-1.10.1.tar.bz2 >& /dev/null

wget http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.10.1.tar.bz2
tar -xf hdf5-1.10.1.tar.bz2
cd hdf5-1.10.1
./configure \
--prefix=/usr/local/extras/Genomics/lib/hdf5/1.10.1 \
--enable-static --enable-shared \
--enable-build-mode=production \
--enable-fortran --enable-cxx
# make -j -l4
make
make check
rm -rf /usr/local/extras/Genomics/lib/hdf5/1.10.1 >& /dev/null
make install
make check-install

cd /usr/local/extras/Genomics/lib/hdf5
rm current >& /dev/null
ln -sTf 1.10.1 current
ln -sf /usr/local/extras/Genomics/lib/hdf5/current/bin/* /usr/local/extras/Genomics/lib/bin/
ln -sf /usr/local/extras/Genomics/lib/hdf5/current/lib/* /usr/local/extras/Genomics/lib/lib/
ln -sf /usr/local/extras/Genomics/lib/hdf5/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf hdf5-1.10.1 hdf5-1.10.1.tar.bz2

date
