#!/bin/bash
#$ -j y
#$ -o hisat2-2.1.0.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf hisat2-2.1.0 hisat2-2.1.0-Linux_x86_64.zip >& /dev/null

wget http://ccb.jhu.edu/software/hisat2/dl/hisat2-2.1.0-Linux_x86_64.zip
unzip hisat2-2.1.0-Linux_x86_64.zip

cd hisat2-2.1.0
mkdir bin
find . -maxdepth 1 -type f -executable | xargs -I {} sh -c 'mv {} bin/'

rm -rf /usr/local/extras/Genomics/apps/hisat2/2.1.0 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/hisat2/2.1.0
cp -r * /usr/local/extras/Genomics/apps/hisat2/2.1.0/

cd /usr/local/extras/Genomics/apps/hisat2
ln -sTf 2.1.0 current
ln -sf /usr/local/extras/Genomics/apps/hisat2/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf hisat2-2.1.0 hisat2-2.1.0-Linux_x86_64.zip

echo "=============================================================================="
