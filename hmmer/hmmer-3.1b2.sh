#!/bin/bash
#$ -j y
#$ -o hmmer-3.1b2.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf hmmer-3.1b2.tar.gz hmmer-3.1b2 >& /dev/null

wget http://eddylab.org/software/hmmer/hmmer-3.1b2.tar.gz
tar -xf hmmer-3.1b2.tar.gz
cd hmmer-3.1b2
./configure --prefix=/usr/local/extras/Genomics/apps/hmmer/3.1b2
make
make check

rm -rf /usr/local/extras/Genomics/apps/hmmer/3.1b2 >& /dev/null
make install

# cd /usr/local/extras/Genomics/apps/hmmer
# ln -sTf 3.1b2 current
# ln -sf /usr/local/extras/Genomics/apps/hmmer/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf hmmer-3.1b2.tar.gz hmmer-3.1b2

echo "=============================================================================="
