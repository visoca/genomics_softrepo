#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o htslib-samtools-bcftools-1.5.log

CURRDIR=$PWD

# htslib
# --------------------------------------------------------------
rm -rf htslib-1.5 htslib-1.5.tar.bz2 >& /dev/null

wget https://github.com/samtools/htslib/releases/download/1.5/htslib-1.5.tar.bz2
tar -xf htslib-1.5.tar.bz2
cd htslib-1.5
LDFLAGS="-L/usr/local/extras/Genomics/lib/lib" \
CPPFLAGS="-I/usr/local/extras/Genomics/lib/include/" \
./configure \
--prefix=/usr/local/extras/Genomics/lib/htslib/1.5
make -j12
rm -rf /usr/local/extras/Genomics/lib/htslib/1.5 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/htslib
ln -sfT 1.5 current
ln -sf /usr/local/extras/Genomics/lib/htslib/current/bin/* /usr/local/extras/Genomics/bin/ 
ln -sf /usr/local/extras/Genomics/lib/htslib/current/lib/* /usr/local/extras/Genomics/lib/lib/ 
ln -sf /usr/local/extras/Genomics/lib/htslib/current/lib/pkgconfig/* /usr/local/extras/Genomics/lib/lib/pkgconfig/
ln -sf /usr/local/extras/Genomics/lib/htslib/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf htslib-1.5 htslib-1.5.tar.bz2
# --------------------------------------------------------------

# samtools
# --------------------------------------------------------------
rm -rf samtools-1.5 samtools-1.5.tar.bz2 >& /dev/null

wget https://github.com/samtools/samtools/releases/download/1.5/samtools-1.5.tar.bz2
tar -xf samtools-1.5.tar.bz2
cd samtools-1.5
LDFLAGS="-L/usr/local/extras/Genomics/lib/lib" \
CPPFLAGS="-I/usr/local/extras/Genomics/lib/include/" \
./configure \
--prefix=/usr/local/extras/Genomics/apps/samtools/1.5 \
--with-htslib=/usr/local/extras/Genomics/lib/htslib/1.5 
make -j12
rm -rf /usr/local/extras/Genomics/apps/samtools/1.5 >& /dev/null
make install


cd /usr/local/extras/Genomics/apps/samtools
ln -sfT 1.5 current
ln -sf /usr/local/extras/Genomics/apps/samtools/current/bin/* /usr/local/extras/Genomics/bin/ 

cd $CURRDIR
rm -rf samtools-1.5 samtools-1.5.tar.bz2
# --------------------------------------------------------------

# bcftools
# --------------------------------------------------------------
rm -rf bcftools-1.5 bcftools-1.5.tar.bz2 >& /dev/null

wget https://github.com/samtools/bcftools/releases/download/1.5/bcftools-1.5.tar.bz2
tar -xf bcftools-1.5.tar.bz2
cd bcftools-1.5
LDFLAGS="-L/usr/local/extras/Genomics/lib/lib" \
CPPFLAGS="-I/usr/local/extras/Genomics/lib/include/" \
./configure \
--prefix=/usr/local/extras/Genomics/apps/bcftools/1.5 \
--with-htslib=/usr/local/extras/Genomics/lib/htslib/1.5 
make -j12
rm -rf /usr/local/extras/Genomics/apps/bcftools/1.5 >& /dev/null
make install


cd /usr/local/extras/Genomics/apps/bcftools
ln -sfT 1.5 current

# correct shebang of vcfutils.pl and plot-roh.py
perl -pi -e 's/\#\!\/usr\/bin\/perl \-w/\#\!\/usr\/bin\/env perl/g' /usr/local/extras/Genomics/apps/bcftools/1.5/bin/vcfutils.pl
perl -pi -e 's/\#\!\/usr\/bin\/python/\#\!\/usr\/bin\/env python/g' /usr/local/extras/Genomics/apps/bcftools/1.5/bin/plot-roh.py

ln -sf /usr/local/extras/Genomics/apps/bcftools/current/bin/* /usr/local/extras/Genomics/bin/ 
ln -sf /usr/local/extras/Genomics/apps/bcftools/current/libexec/* /usr/local/extras/Genomics/lib/lib/

cd $CURRDIR
rm -rf bcftools-1.5 bcftools-1.5.tar.bz2
# --------------------------------------------------------------

