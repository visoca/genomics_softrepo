#!/bin/bash
#$ -l h_rt=04:00:00
##$ -q popgenom.q
##$ -P popgenom
##$ -pe openmp 24
#$ -l mem=8g
#$ -j y
#$ -o iqtree-1.6.2.log

# load genomics software repository

source /usr/local/extras/Genomics/.bashrc 

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/gcc/4.8.2
	module add mpi/gcc/openmpi/1.10.1
else # ShARC
	module add mpi/openmpi/2.1.1/gcc-4.8.5
fi

CURRDIR=$PWD
rm -rf IQ-TREE-1.6.2 iqtree-1.6.2.tar.gz >& /dev/null

wget https://github.com/Cibiv/IQ-TREE/archive/v1.6.2.tar.gz -O iqtree-1.6.2.tar.gz
tar -xf iqtree-1.6.2.tar.gz
cd IQ-TREE-1.6.2

# get Eigen headers
wget http://bitbucket.org/eigen/eigen/get/3.3.4.tar.bz2 -O eigen-3.3.4.tar.bz2
tar -xf eigen-3.3.4.tar.bz2
rm eigen-3.3.4.tar.bz2
EIGENDIR=$(ls -d *| grep eigen)

mkdir build bin
cd build
CMAKE_LIBRARY_PATH=/usr/local/extras/Genomics/lib/lib:$CMAKE_LIBRARY_PATH \
CMAKE_INCLUDE_PATH=/usr/local/extras/Genomics/lib/include:$CMAKE_INCLUDE_PATH \
cmake -DEIGEN3_INCLUDE_DIR=$PWD/../$EIGENDIR ..
make -j24
# make
cp iqtree ../bin
rm -rf *

CMAKE_LIBRARY_PATH=/usr/local/extras/Genomics/lib/lib:$CMAKE_LIBRARY_PATH \
CMAKE_INCLUDE_PATH=/usr/local/extras/Genomics/lib/include:$CMAKE_INCLUDE_PATH \
cmake -DEIGEN3_INCLUDE_DIR=$PWD/../$EIGENDIR -DIQTREE_FLAGS="mpi" ..
# make -j24
make
cp iqtree-mpi ../bin
cd ..
rm -rf build

rm -rf /usr/local/extras/Genomics/apps/iqtree/1.6.2
mkdir -p /usr/local/extras/Genomics/apps/iqtree/1.6.2
cp -r * /usr/local/extras/Genomics/apps/iqtree/1.6.2/

cd /usr/local/extras/Genomics/apps/iqtree
ln -sTf 1.6.2 current
ln -sf /usr/local/extras/Genomics/apps/iqtree/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf IQ-TREE-1.6.2 iqtree-1.6.2.tar.gz
