#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o java-8u144.log

CURRDIR=$PWD
rm -rf jdk1.8.0_144 jdk-8u144-linux-x64.tar.gz >& /dev/null

wget --no-check-certificate --no-cookies \
--header "Cookie: oraclelicense=accept-securebackup-cookie" \
http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-linux-x64.tar.gz
tar -xvf jdk-8u144-linux-x64.tar.gz
cd jdk1.8.0_144
mkdir -p /usr/local/extras/Genomics/apps/java/jdk1.8.0_144
cp -r * /usr/local/extras/Genomics/apps/java/jdk1.8.0_144/
cd /usr/local/extras/Genomics/apps/java/
ln -sfT jdk1.8.0_144 current

cd $CURRDIR
rm -rf jdk1.8.0_144 jdk-8u144-linux-x64.tar.gz
