#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -l h_rt=00:30:00
#$ -j y
#$ -V 
#$ -o java-8u151.log

CURRDIR=$PWD
rm -rf jdk1.8.0_151 jdk-8u151-linux-x64.tar.gz >& /dev/null

wget --no-check-certificate --no-cookies \
--header "Cookie: oraclelicense=accept-securebackup-cookie" \
http://download.oracle.com/otn-pub/java/jdk/8u151-b12/e758a0de34e24606bca991d704f6dcbf/jdk-8u151-linux-x64.tar.gz

tar -xvf jdk-8u151-linux-x64.tar.gz
cd jdk1.8.0_151
mkdir -p /usr/local/extras/Genomics/apps/java/jdk1.8.0_151
cp -r * /usr/local/extras/Genomics/apps/java/jdk1.8.0_151/
cd /usr/local/extras/Genomics/apps/java/
ln -sfT jdk1.8.0_151 current

cd $CURRDIR
rm -rf jdk1.8.0_151 jdk-8u151-linux-x64.tar.gz
