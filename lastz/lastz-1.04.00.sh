#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o lastz-1.04.00.log

CURRDIR=$PWD
rm -rf lastz-distrib-1.04.00 lastz-1.04.00.tar.gz >& /dev/null

wget http://www.bx.psu.edu/~rsharris/lastz/lastz-1.04.00.tar.gz
tar -xf lastz-1.04.00.tar.gz
cd lastz-distrib-1.04.00
export LASTZ_INSTALL='/usr/local/extras/Genomics/apps/lastz/1.04.00'
rm -rf /usr/local/extras/Genomics/apps/lastz/1.04.00 >& /dev/null
make -j 12
make install
make test
make clean
cd src
make -j 12 lastz_32
make install_32

cd /usr/local/extras/Genomics/apps/lastz/
ln -sf 1.04.00 current
ln -sf /usr/local/extras/Genomics/apps/lastz/current/lastz* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf lastz-distrib-1.04.00 lastz-1.04.00.tar.gz
