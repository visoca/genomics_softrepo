#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 12
#$ -V
#$ -j y
#$ -o m4-1.4.18.log

hostname
date

echo "==============================================================================="

# load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

mkdir -p /scratch/bo1vsx >& /dev/null
cd /scratch/bo1vsx/
CURR=$PWD

rm -rf m4-1.4.18 m4-1.4.18.tar.bz2 >& /dev/null
wget http://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.bz2
tar -xf m4-1.4.18.tar.bz2
cd m4-1.4.18

./configure --prefix=/usr/local/extras/Genomics/apps/m4/1.4.18
make 
make check
rm -rf /usr/local/extras/Genomics/apps/m4/1.4.18 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/m4
rm current >& /dev/null
ln -sTf 1.4.18 current

ln -sf /usr/local/extras/Genomics/apps/m4/current/bin/m4 /usr/local/extras/Genomics/bin/

cd $CURR
rm -rf m4-1.4.18 m4-1.4.18.tar.bz2

echo "==============================================================================="
date
