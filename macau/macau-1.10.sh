#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o macau-1.10.log

CURRDIR=$PWD
rm -rf macau-1.10 macau-1.10.tar.gz >& /dev/null

wget http://www.xzlab.org/software/macau/macau-1.10.tar.gz
tar -xf macau-1.10.tar.gz
cd macau-1.10
rm -rf /usr/local/extras/Genomics/apps/macau/1.10 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/macau/1.10
cp -r * /usr/local/extras/Genomics/apps/macau/1.10/

cd /usr/local/extras/Genomics/apps/macau
ln -sTf 1.10 current
ln -sf /usr/local/extras/Genomics/apps/macau/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf macau-1.10 macau-1.10.tar.gz
