#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o mapit_2018-06-13.log

CURRDIR=$PWD

rm -rf MAPIT mapit_2018-06-13 >& /dev/null

git clone https://github.com/lorinanthony/MAPIT

mkdir mapit_2018-06-13 >& /dev/null
cd mapit_2018-06-13
cp ../MAPIT/OpenMP\ Version/MAPIT_OpenMP.R ./
cp ../MAPIT/OpenMP\ Version/MAPIT_OpenMP.cpp ./
cp ../MAPIT/QQPlot.R ./QQplot.R

# Compile c++ libraries
R --vanilla -e 'Rcpp::sourceCpp("MAPIT_OpenMP.cpp", cacheDir="./tmp", verbose=T, showOutput=T)'

find ./tmp -name "sourceCpp_2.so" -exec cp {} ./MAPIT_OpenMP.so \;
find ./tmp -name "MAPIT_OpenMP.cpp.R" -exec cp {} ./ \; 
rm -rf ./tmp
perl -pi -e 's/dyn\.load.*/dyn\.load\(\"MAPIT_OpenMP\.so\"\)/g' MAPIT_OpenMP.cpp.R

# Modify MAPIT R script to load c++ library
# .............................................................................
sed -i '30i	\
source("MAPIT_OpenMP.cpp.R") \
 \
### Load in the R libraries ### \
library(doParallel) \
library(Rcpp) \
library(RcppArmadillo) \
library(RcppParallel) \
library(CompQuadForm) \
library(tools) \
' MAPIT_OpenMP.R
# .............................................................................

### scripts to run MAPIT: runMAPIT.R and runMAPITExhaustiveSearch.R
# =============================================================================
cat > runMAPIT.R <<'EOF'
#!/usr/local/extras/Genomics/bin/Rscript --vanilla

# (c) Victor Soria-Carrasco
# This is a wrapper to run MAPIT analyses (in combination with runMAPITExhaustiveSearch.R)
# See https://github.com/lorinanthony/MAPIT and Crawford (2017) 
# (https://doi.org/10.1371/journal.pgen.1006869) for more information

### Run MArginal ePIstasis Test (MAPIT) using either
### the faster hybrid method (Z-test + Davies) or the slower exact method (Davies)
###
### The following model variations are accepted:
###   (1) Standard Model
###       y = m+g+e where 
###           m ~ MVN(0,omega^2K), g ~ MVN(0,sigma^2G), e ~ MVN(0,tau^2M)
###           Recall from Crawford et al. (2017) that m is the combined additive 
###           effects from all other variants, and effectively represents the 
###           additive effect of the kth variant under the polygenic background 
###           of all other variants; 
###      K = X_{-k}^tX_{-k}/(p - 1) is the genetic relatedness matrix computed 
###           using genotypes from all variants other than the kth; g is the 
###           summation of all pairwise interaction effects between the kth 
###           variant and all other variants; G = DKD represents a relatedness 
###           matrix computed based on pairwise interaction terms between the 
###           kth variant and all other variants. Here, we also denote D = diag(x_k)
###           to be an n x n diagonal matrix with the genotype vector x_k as its
###           diagonal elements. It is important to note that both K and G change
###           with every new marker k that is considered. Lastly, M is a variant 
###           specific projection matrix onto both the null space of the intercept 
###           and the corresponding genotypic vector x_k.
###  (2) Standard + Covariate Model 
###      y = Wa+m+g+e where 
###          W is a matrix of covariates with effect sizes a.
###  (3) Standard + Common Environment Model 
###      y = m+g+c+e where 
###          c ~ MVN(0,eta^2C) controls for extra environmental effects and 
###          population structure with covariance matrix C.
###  (4) Standard + Covariate + Common Environment Model 
###      y = Wa+m+g+c+e


library(getopt)
library(tools)

# get arguments C-style
spec<-matrix(c(
  'phenofile',         'p',  1, "character", "Phenotype file",
  'genofile',          'g',  1, "character", "Genotype file",
  'covfile',           'w',  2, "character", "Covariate file (optional, default=none)",
  'popenvfile',        'c',  2, "character", "Population structure and environmental effects covariate file (optional, deault=none)",
  'method',            'm',  2, "character", "Method ('hybrid' or 'davies') (optional, default=hybrid)",
  'outdir',            'o',  2, "character", "Output directory (optional, default=dirname(phenofile))",
  'threads',           'n',  2, "integer",   "Number of threads (optional, default=detectCores))",
  'threshold',         't',  2, "float",     "Threshold for recalculating p-values using Davies method (optional, default=0.05)",
  'help',              'h',  0, "logical",   "Help"
), byrow=T, ncol=5)
opt<-getopt(spec)

cat("\n")
# Check required arguments
if (is.null(opt$help) && is.null(opt$phenofile)) cat("ERROR: Phenotype file not specified\n\n")
if (is.null(opt$help) && is.null(opt$genofile)) cat("ERROR: Genotype file not specified\n\n")

# show usage as help
if (!is.null(opt$help) | is.null(opt$phenofile) | is.null (opt$genofile)) {
	cat(getopt(spec, usage=TRUE));
	q(status=1);
	cat("/n")
}

# use default values if none is given
if (is.null(opt$method)) opt$method<-'hybrid'
if (is.null(opt$outdir)) opt$outdir<-dirname(opt$phenofile)
if (is.null(opt$threads)) opt$threads<-detectCores()
if (is.null(opt$threshold.dav)) opt$threshold<-0.05

cat ("\nArguments:\n")
cat ("-------------------------------------------------------------------------------\n")
cat ("  phenofile:      ", opt$phenofile,"\n")
cat ("  genofile:       ", opt$genofile,"\n")
cat ("  covfile:        ", opt$covfile,"\n")
cat ("  popenvfile:     ", opt$popenvfile,"\n")
cat ("  method:         ", opt$method,"\n")
cat ("  outdir:         ", opt$outdir,"\n")
cat ("  threads:        ", opt$threads,"\n")
cat ("  threshold:      ", opt$threshold,"\n")
cat ("-------------------------------------------------------------------------------\n\n")

### phenotypes file
phenofile<-opt$phenofile
### genotype file
genofile<-opt$genofile
### covariates file - genotypes PCs
covfile<-opt$covfile
### covariates population structure and environment effects file
popenvfile<-opt$covfile

### method
method<-opt$method
if (method=='hybrid') hybrid<-TRUE
if (method=='davies') hybrid<-FALSE; test<-'davies'

### output directory
outdir<-opt$outdir

### Number of cores
threads<-opt$threads
### Threshold for recalculating p-values using Davies method 
threshold<-opt$threshold


### output files
outRdata<-paste(outdir, "/", file_path_sans_ext(basename(phenofile)), ".MAPIT", method, ".Rdata", sep="")
# table of pvalues and pves
outpvalpve<-paste(outdir, "/", file_path_sans_ext(basename(phenofile)),".MAPIT", method, ".pval-pve.dsv", sep="")
# table of pvalues and pves
outqqplot<-paste(outdir, "/", file_path_sans_ext(basename(phenofile)),".MAPIT", method, ".qqplot.png", sep="")

# echo cmds
options(echo=TRUE)

# Show time
Sys.time()

# Start global clock
stm<-proc.time()

###############################################################################
#######                        Load functions                           #######
###############################################################################

### Load in functions to make QQ-plot plots ###
source("/usr/local/extras/Genomics/apps/mapit/2018-06-08/QQplot.R")

### Load in the C++ MAPIT wrapper functions ###
source("/usr/local/extras/Genomics/apps/mapit/2018-06-08/MAPIT_OpenMP.R");

# Show time
Sys.time()

###############################################################################
###############################################################################


###############################################################################
#######                           Load data                             #######
###############################################################################

# genotype file assumed to be bimbam geno format with header
# snps on rows, inds in columns
# first three columns must be: snp id, ref allele, and alt allele
geno<-read.table(genofile, sep=" ", header=T)

# phenofile is simply a 1-column list of phenotypes, without header
pheno<-read.table(phenofile, sep=" ", header=F)

W<-C<-NULL
# covfile - matrix of covariates
if (!is.null(covfile)) W<-as.matrix(read.table(covfile,sep=" "))
# popenvfile is matrix of covariates accounting for population structure and 
# environmental effects
if (!is.null(popenvfile)) C<-as.matrix(read.table(popenvfile,sep=" "))

# standardize genotypes
X<-geno[,-(1:3)]
Xmean<-apply(X, 1, mean)
Xsd<-apply(X, 1, sd) 
X<-(X-Xmean)/Xsd

# get phenotypes
y<-pheno

# matrix required for MAPIT functions
X<-as.matrix(X)
y<-as.matrix(y)

# Show time
Sys.time()

###############################################################################
###############################################################################

###############################################################################
#######                           Run MAPIT                             #######
###############################################################################

#'X' is the pxn genotype matrix where p is the number of variants and n is the
#    number of samples. Must be a matrix and not a data.frame.
#'y' is the nx1 vector of quantitative or continuous traits.
#'W' is the matrix qxn matrix of covariates. Must be a matrix and not a data.frame.
#'C' is an nxn covariance matrix detailing environmental effects and population 
#    structure effects.
#'hybrid' is a parameter detailing if the function should run the hybrid 
#         hypothesis testing procedure between the normal Z-test and the 
#         Davies method. Default is TRUE.
#'threshold' is a parameter detailing the value at which to recalibrate the 
#            Z-test p-values. If nothing is defined by the user, the default 
#            value will be 0.05 as recommended by the Crawford et al. (2017). 
#'test' is a parameter defining what hypothesis test should be implemented. Takes
#       on values 'normal' or 'davies'. This parameter only matters when 
#       hybrid = FALSE. If test is not defined when hybrid = FALSE, the function
#       will automatically use test = 'normal'.

### Run MAPIT ###
ptm <- proc.time() #Start clock

mapit<-MAPIT(X, y, W, C, hybrid, test, threshold, cores=threads)

proc.time() - ptm #Stop clock

etm<-proc.time() - stm # Stop global clock

# Add snps ids, inds ids, ref allele, alt allele
mapit[["snp"]]<-as.character(geno[,1])
mapit[["ind"]]<-as.character(geno[1,-(1:3)])
mapit[["X"]]<-X # genotypes
mapit[["y"]]<-y # phenotypes
mapit[["C"]]<-C # covariates
mapit[["W"]]<-W # covariates population structure and environment
mapit[["method"]]<-method # method
mapit[["threshold"]]<-threshold # threshold
mapit[["runtime"]]<-etm # running time

save(mapit, file=outRdata)

# Output table of pvalues and pves
out<-data.frame(snp=geno[,1],ref=geno[,2],alt=geno[,3],pvalue=mapit$pvalues, pve=mapit$pves)
write.table(out, file=outpvalpve, sep="\t", quote=F, row.names=F)

# Output qqplot
png(file=outqqplot, width=1200, height=1200)
par(mar=c(5,5,2,1))
ggd.qqplot(mapit$pvalues,cex.lab=2,cex.axis=2)
dev.off()
###############################################################################
###############################################################################
EOF

cat > runMAPITExhaustiveSearch.R <<'EOF'
#!/usr/local/extras/Genomics/bin/Rscript --vanilla

# (c) Victor Soria-Carrasco
# This is a wrapper to run MAPIT analyses (in combination with runMAPIT.R)
# See https://github.com/lorinanthony/MAPIT and Crawford (2017) 
# (https://doi.org/10.1371/journal.pgen.1006869) for more information

library(getopt)
library(tools)

# get arguments C-style
spec<-matrix(c(
  'mapitrun',      'm',  1, "character",
  'threshold',     't',  1, "double",
  'help',          'h',  0, "logical"
), byrow=T, ncol=4)
opt<-getopt(spec)

cat ("\n")
if (is.null(opt$help) & is.null(opt$mapitrun)) cat("ERROR: R data file with output of MAPIT run not specified\n\n")

# show usage as help
if (!is.null(opt$help) || is.null(opt$mapitrun)) {
	cat(getopt(spec, usage=TRUE));
	cat("\n")
	q(status=1);
}

# use default values if none is given
if (is.null(opt$threshold)) opt$threads<-2

cat ("\nArguments:\n")
cat ("-------------------------------------------------------------------------------\n")
cat ("  mapitrun:      ", opt$mapitrun,"\n")
cat ("  threshold:     ", opt$threshold,"\n")
cat ("-------------------------------------------------------------------------------\n\n")

### MAPIT Rdata file with MAPIT results from runMAPIT.R
mapitrunfile<-opt$mapitrun

### Threshold for significant SNPs according to their marginal epistatic effects
### to run a simple exhaustive search between them
threshold<-opt$threshold

### output directory
outdir<-dirname(mapitrunfile)

# echo cmds
options(echo=TRUE)

# Show time
Sys.time()

### load MAPIT results

load(mapitrunfile)

# mapit is a list expected to have the following fields
# 	mapit[["snp"]] # name of snps
# 	mapit[["ind"]] # name of individuals
# 	mapit[["X"]]   # genotypes
# 	mapit[["y"]] # phenotypes
# 	mapit[["C"]] # covariates
# 	mapit[["W"]] # covariates population structure and environment
# 	mapit[["method"]] # method
# 	mapit[["threshold"]] # threshold for recalculating p-values using Davies method
# 	mapit[["runtime"]] # running time


X<-mapit$X # genotypes
y<-mapit$y # phenotypes
pvalues<-mapit$pvalues # margianl epistatic effect p-values
snps<-mapit$snp # snps ids
method<-mapit$method # method used in MAPIT run

# Use Bonferroni correction as default threshold if none is given
if (is.null(threshold)) threshold<-0.05/length(pvalues)

# Output of pairwise p-values
outpwise<-paste(outdir, "/", file_path_sans_ext(basename(mapitrunfile)),".MAPIT", method, ".pwise.thr",threshold,".dsv", sep="")

### Run an Informed Exhaustive Search ###
# NOTE: Now we may take only the significant SNPs according to their marginal epistatic effects
# and run a simple exhaustive search between them

pvalues<-pvalues[!is.na(pvalues)] # Remove NAs
v<-pvalues[pvalues<=threshold] # Call only marginally significant SNPs
rownames(X)<-snps
names(v)<-snps[pvalues<=threshold]
pwise<-vector()
for (k in 1:length(v)){
	fit<-vector();
	m<-1:length(v)
	for(w in m[-k]){
		pt<-lm(y~X[names(v)[k],]:X[names(v)[w],])
		fit[w]<-coefficients(summary(pt))[8]
		names(fit)[w]<-paste(names(v)[k],names(v)[w],sep = "-")
	}
	pwise<-c(pwise,fit)
}

### Sort and keep only unique pairs ###
vc.pwise<-sort(pwise[!is.na(pwise)]) # Get rid of the NAs and sort
vc.pwise<-vc.pwise[seq(1,length(vc.pwise),2)] # Only keep the unique pairs
vc.pwise<-sort(vc.pwise) # Sort the pairs in order of significance

# Save results to table
out<-data.frame(pair=names(vc.pwise), pvalue=as.numeric(vc.pwise))
write.table(out, file=outpwise, sep="\t", row.names=F, col.names=T, quote=F)

Sys.time()

######################################################################################
######################################################################################
EOF

chmod +x runMAPIT.R
chmod +x runMAPITExhaustiveSearch.R
# =============================================================================

rm -rf /usr/local/extras/Genomics/apps/mapit/2018-06-13 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/mapit/2018-06-13 >& /dev/null
cp -r * /usr/local/extras/Genomics/apps/mapit/2018-06-13/
cd /usr/local/extras/Genomics/apps/mapit/
ln -sTf 2018-06-13 current
cd current
ln -sf /usr/local/extras/Genomics/apps/mapit/current/runMAPIT.R /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/mapit/current/runMAPITExhaustiveSearch.R /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf MAPIT mapit_2018-06-13

