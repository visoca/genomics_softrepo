#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o marvel-2017-11-20.log

source /usr/local/extras/Genomics/.bashrc

CURRDIR=$PWD
rm -rf MARVEL >& /dev/null

git clone https://github.com/schloi/MARVEL.git
source activate py35
pip install --upgrade networkx
cd MARVEL
autoreconf
./configure \
--prefix=/usr/local/extras/Genomics/apps/marvel/2017-11-20
make -j 12
rm -rf /usr/local/extras/Genomics/apps/marvel/2017-11-20 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/marvel
ln -sTf 2017-11-20 current
cd current

sed -i "2i import sys" scripts/constants.py
sed -i '0,/from marvel/ s/from marvel/sys\.path\.append\(\"\/usr\/local\/extras\/Genomics\/apps\/marvel\/2017\-11\-19\/lib\.python\"\)\nfrom marvel/' scripts/*.py

ln -sf /usr/local/extras/Genomics/apps/marvel/current/bin/* /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/marvel/current/scripts/*.py /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf MARVEL
