#!/bin/bash
#$ -pe openmp 4
#$ -j y
#$ -o masurca-3.3.0.log

module add dev/gcc/4.9.4
module add libs/boost/1.64.0/gcc-4.9.4
export NUM_THREADS=4

CURRDIR=$PWD
rm -rf MaSuRCA-3.3.0 MaSuRCA-3.3.0.tar.gz >& /dev/null

wget https://github.com/alekseyzimin/masurca/releases/download/v3.3.0/MaSuRCA-3.3.0.tar.gz
tar -xf MaSuRCA-3.3.0.tar.gz
cd MaSuRCA-3.3.0
# comment out the line getting all threads available in the node
perl -pi -e 's/^export NUM\_THREADS/\# export NUM\_THREADS=/g' install.sh
./install.sh
mkdir -p /usr/local/extras/Genomics/apps/masurca/3.3.0

# wrapper to load libraries and set up paths
# --------------------------------------------------
cd bin
mv masurca masurca.bin
cat >masurca.sh <<'EOF'
#!/bin/bash

module add dev/gcc/4.9.4
module add libs/boost/1.64.0/gcc-4.9.4

/usr/local/extras/Genomics/apps/arcs/3.3.0/bin/masurca.bin $*
EOF
chmod +x /usr/local/extras/Genomics/apps/masurca/3.3.0/bin/masurca.sh
cd /usr/local/extras/Genomics/apps/masurca/3.3.0/bin
ln -s masurca.sh masurca
# --------------------------------------------------

cp -r * /usr/local/extras/Genomics/apps/masurca/3.3.0/
cd /usr/local/extras/Genomics/apps/masurca
ln -sTf 3.3.0 current

ln -s /usr/local/extras/Genomics/apps/masurca/current/bin/masurca /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf MaSuRCA-3.3.0 MaSuRCA-3.3.0.tar.gz
