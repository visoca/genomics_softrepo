#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 12
#$ -V
#$ -j y
#$ -o mpfr-3.1.5.log

hostname
date

# load genomics software repository
source /usr/local/extras/Genomics/.bashrc

CURRDIR=$PWD
rm -rf mpfr-3.1.5 mpfr-3.1.5.tar.bz2 >& /dev/null

wget http://www.mpfr.org/mpfr-3.1.5/mpfr-3.1.5.tar.bz2
tar -xf mpfr-3.1.5.tar.bz2
cd mpfr-3.1.5
wget http://www.mpfr.org/mpfr-3.1.5/allpatches
patch -N -Z -p1 < allpatches

./configure \
--prefix=/usr/local/extras/Genomics/lib/mpfr/3.1.5 \
--with-gmp=/usr/local/extras/Genomics/lib/gmp/6.1.2 \
--enable-thread-safe
make
make check
rm -rf /usr/local/extras/Genomics/lib/mpfr/3.1.5 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/mpfr
ln -sfT 3.1.5 current
cd /usr/local/extras/Genomics/lib
find /usr/local/extras/Genomics/lib/mpfr/current/lib -type f -or -type l | xargs -I {} sh -c 'ln -sf {} lib/'
find /usr/local/extras/Genomics/lib/mpfr/current/lib -mindepth 1 -type d | xargs -I {} sh -c 'ln -sfT {} lib/'
find /usr/local/extras/Genomics/lib/mpfr/current/include -type f -or -type l | xargs -I {} sh -c 'ln -sf {} include/'
find /usr/local/extras/Genomics/lib/mpfr/current/include -mindepth 1 -type d | xargs -I {} sh -c 'ln -sfT {} include/'

cd $CURRDIR
rm -rf mpfr-3.1.5 mpfr-3.1.5.tar.bz2

date
