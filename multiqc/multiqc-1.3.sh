#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o multiqc-1.3.log

conda remove --name py27multiqc12 --all -y
conda create -n py27multiqc12 python=2.7 anaconda -y
source activate py27multiqc12
conda install -c bioconda multiqc

rm -rf /usr/local/extras/Genomics/apps/multiqc/1.3 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/multiqc/1.3
cd /usr/local/extras/Genomics/apps/multiqc/1.3

# create wrapper
cat >multiqc.sh << 'EOF'
#!/bin/bash

source activate py27multiqc12

multiqc $*

source deactivate

EOF
chmod +x multiqc.sh
ln -s multiqc.sh multiqc

cd ..
ln -sTf 1.3 current
ln -sf /usr/local/extras/Genomics/apps/multiqc/current/multiqc /usr/local/extras/Genomics/bin/

