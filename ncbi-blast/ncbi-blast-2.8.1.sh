#!/bin/bash
#$ -j y
#$ -o ncbi-blast-2.8.1.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf ncbi-blast-2.8.1+-x64-linux.tar.gz ncbi-blast-2.8.1+ >& /dev/null

wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.8.1/ncbi-blast-2.8.1+-x64-linux.tar.gz
tar -xf ncbi-blast-2.8.1+-x64-linux.tar.gz

rm -rf /usr/local/extras/Genomics/apps/ncbi-blast/2.8.1 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/ncbi-blast/2.8.1
cd ncbi-blast-2.8.1+
cp -r * /usr/local/extras/Genomics/apps/ncbi-blast/2.8.1/

cd /usr/local/extras/Genomics/apps/ncbi-blast
ln -sTf 2.8.1 current

ln -sf /usr/local/extras/Genomics/apps/ncbi-blast/current/bin/* /usr/local/extras/Genomics/bin

cd $CURRDIR
rm -rf ncbi-blast-2.8.1+-x64-linux.tar.gz ncbi-blast-2.8.1+

echo "=============================================================================="
