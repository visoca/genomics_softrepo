#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o ncbi-decryption-tools-2.7.0.log


# NCBI decryption tools
# --------------------------------------------------------------
cd /local
rm -rf decryption.2.7.0* >& /dev/null

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.7.0/decryption.2.7.0-centos_linux64.tar.gz
tar -xf decryption.2.7.0-centos_linux64.tar.gz
cd decryption.2.7.0-centos_linux64
mkdir -p /usr/local/extras/Genomics/apps/ncbi-decryption-tools/2.7.0
cp -r * /usr/local/extras/Genomics/apps/ncbi-decryption-tools/2.7.0/
cd /usr/local/extras/Genomics/apps/ncbi-decryption-tools
rm current
ln -s 2.7.0 current
find /usr/local/extras/Genomics/apps/ncbi-decryption-tools/current/bin -type l -regextype grep -name "[a-z]*" -not -name "*\.*" | xargs -I {} sh -c 'rm /usr/local/extras/Genomics/bin/$(basename {})'
find /usr/local/extras/Genomics/apps/ncbi-decryption-tools/current/bin -type l -regextype grep -name "[a-z]*" -not -name "*\.*" -exec ln -s {} /usr/local/extras/Genomics/bin/ \;

rm -rf /local/decryption.2.7.0*
# --------------------------------------------------------------

