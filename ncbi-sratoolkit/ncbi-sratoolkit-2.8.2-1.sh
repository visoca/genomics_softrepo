#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o ncbi-sratoolkit-2.8.2-1.log


# NCBI SRA toolkit
# --------------------------------------------------------------
cd /local
rm -rf sratoolkit.2.8.2-1* >& /dev/null

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.8.2-1/sratoolkit.2.8.2-1-centos_linux64.tar.gz
tar -xf sratoolkit.2.8.2-1-centos_linux64.tar.gz
cd sratoolkit.2.8.2-1-centos_linux64
mkdir -p /usr/local/extras/Genomics/apps/ncbi-sratoolkit/2.8.2-1
cp -r * /usr/local/extras/Genomics/apps/ncbi-sratoolkit/2.8.2-1/
cd /usr/local/extras/Genomics/apps/ncbi-sratoolkit
rm current >& /dev/null
ln -s 2.8.2-1 current
find /usr/local/extras/Genomics/apps/ncbi-sratoolkit/current/bin -type l -regextype grep -name "[a-z]*" -not -name "*\.*" | xargs -I {} sh -c 'rm /usr/local/extras/Genomics/bin/$(basename {})'
find /usr/local/extras/Genomics/apps/ncbi-sratoolkit/current/bin -type l -regextype grep -name "[a-z]*" -not -name "*\.*" -exec ln -s {} /usr/local/extras/Genomics/bin/ \;
rm -rf ratoolkit.2.8.2-1*
# --------------------------------------------------------------

