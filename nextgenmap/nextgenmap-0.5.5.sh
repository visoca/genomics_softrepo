#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o nextgenmap-0.5.5.log

CURRDIR=$PWD
rm -rf NextGenMap-0.5.5 NextGenMap-0.5.5.tar.gz >& /dev/null

wget https://github.com/Cibiv/NextGenMap/archive/v0.5.5.tar.gz -O NextGenMap-0.5.5.tar.gz
tar xzvf NextGenMap-0.5.5.tar.gz
cd NextGenMap-0.5.5
mkdir -p build
cd build
cmake ..
make
cd ../bin/ngm-0.5.5
rm -rf /usr/local/extras/Genomics/apps/ngm/0.5.5 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/ngm/0.5.5
cp -r * /usr/local/extras/Genomics/apps/ngm/0.5.5/

cd /usr/local/extras/Genomics/apps/ngm
ln -sf 0.5.5 current

ln -sf /usr/local/extras/Genomics/apps/ngm/current/ngm* /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/ngm/current/oclTool /usr/local/extras/Genomics/bin/


# easy installation
# conda remove --name py35nextgenamp --all
# conda create -n py35nextgenamp python=3.5 anaconda
# source activate py35nextgenamp
# conda install nextgenmap

cd $CURRDIR
rm -rf NextGenMap-0.5.5 NextGenMap-0.5.5.tar.gz
