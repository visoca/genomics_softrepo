#!/bin/bash
#$ -j y
#$ -o ngsld-1.0.0-20190308.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf ngsLD >& /dev/null

git clone https://github.com/fgvieira/ngsLD.git
cd ngsLD
perl -pi -e 'if (/^CFLAGS/) { chomp; $_.=" -I/usr/local/extras/Genomics/lib/include\n"; }
             if (/^LIB/) {chomp; $_.=" -L/usr/local/extras/Genomics/lib/lib\n"; }' Makefile
make
mkdir bin
mv ngsLD bin/
make clean

rm -rf /usr/local/extras/Genomics/apps/ngsld/1.0.0-20190308 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/ngsld/1.0.0-20190308
cp -r * /usr/local/extras/Genomics/apps/ngsld/1.0.0-20190308/

cd /usr/local/extras/Genomics/apps/ngsld
ln -sTf 1.0.0-20190308 current
ln -s /usr/local/extras/Genomics/apps/ngsld/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf ngsLD

echo "=============================================================================="
