#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o oligominer-2017-12-05.log

CURRDIR=$PWD
rm -rf bowtie2-2.3.3.1 bowtie2-2.3.3.1.tar.gz >& /dev/null
rm -rf jellyfish-2.2.6.tar.gz jellyfish-2.2.6 >& /dev/null
rm -rf nupack3.0.6.tar.gz nupack3.0.6 ._nupack3.0.6 >& /dev/null
rm -rf /usr/local/extras/Genomics/apps/oligominer/2017-12-05 >& /dev/null

mkdir -p /usr/local/extras/Genomics/apps/oligominer/2017-12-05

# required programs
# .............................................................................
# .............................................................................
# bowtie2
# .............................................................................
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
	module add libs/binlibs/intel-tbb/2017.0
else # ShARC
	module add dev/intel-compilers/17.0.0
	module add libs/intel-tbb/2017.0/binary
fi

wget https://github.com/BenLangmead/bowtie2/archive/v2.3.3.1.tar.gz -O bowtie2-2.3.3.1.tar.gz
tar -xf bowtie2-2.3.3.1.tar.gz
cd bowtie2-2.3.3.1
perl -pe 's/SSE_FLAG\=\-msse2/SSE_FLAG\=\-sse4.2/g' Makefile
make -j12

# add path to intel TBB libraries
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	sed -i '3i BEGIN\n{\n\t$ENV{LD_LIBRARY_PATH}="/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LD_LIBRARY_PATH";\n}' bowtie2
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-build
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages6/compilers/intel-ps-xe-ce/2017.0/binary/compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-inspect
else # ShARC
	sed -i '3i BEGIN\n{\n\t$ENV{LD_LIBRARY_PATH}="/usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary//compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:$LD_LIBRARY_PATH";\n}' bowtie2
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary//compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-build
	sed -i "28i os.environ['LD_LIBRARY_PATH'] = '/usr/local/packages/dev/intel-ps-xe-ce/2017.0/binary//compilers_and_libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7:' + os.environ['LD_LIBRARY_PATH']" bowtie2-inspect
fi

mkdir bin
mv bowtie2* bin/

mkdir -p /usr/local/extras/Genomics/apps/oligominer/2017-12-05/bowtie2-2.3.3.1
cp -r * /usr/local/extras/Genomics/apps/oligominer/2017-12-05/bowtie2-2.3.3.1

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module rm compilers/intel/17.0.0
	module rm libs/binlibs/intel-tbb/2017.0
else # ShARC
	module rm dev/intel-compilers/17.0.0
	module rm libs/intel-tbb/2017.0/binary
fi

cd $CURRDIR
rm -rf bowtie2-2.3.3.1 bowtie2-2.3.3.1.tar.gz
# .............................................................................

# jellyfish
# .............................................................................
wget https://github.com/gmarcais/Jellyfish/releases/download/v2.2.6/jellyfish-2.2.6.tar.gz
tar -xf jellyfish-2.2.6.tar.gz
cd jellyfish-2.2.6
./configure --prefix=/usr/local/extras/Genomics/apps/oligominer/2017-12-05/jellyfish-2.2.6
make -j12 
# make check
# make check BIG=1
make install
cd $CURRDIR
rm -rf jellyfish-2.2.6.tar.gz jellyfish-2.2.6
# .............................................................................

# NUPACK
# .............................................................................
wget --user v.soria-carrasco@sheffield.ac.uk --password gVyZzSG6 http://www.nupack.org/downloads/serve_file/nupack3.0.6.tar.gz
tar -xvf nupack3.0.6.tar.gz
cd nupack3.0.6
make
mkdir -p /usr/local/extras/Genomics/apps/oligominer/2017-12-05/nupack-3.0.6
cp -r * /usr/local/extras/Genomics/apps/oligominer/2017-12-05/nupack-3.0.6

cd $CURRDIR
rm -rf nupack3.0.6.tar.gz nupack3.0.6 ._nupack3.0.6
# .............................................................................

# .............................................................................
# .............................................................................

# install oligominer
# .............................................................................
conda remove --name oligominer --all
conda create --name oligominer python=2.7 numpy scipy biopython scikit-learn
source activate oligominer

git clone https://github.com/brianbeliveau/OligoMiner.git
cp -r OligoMiner/* /usr/local/extras/Genomics/apps/oligominer/2017-12-05/
# .............................................................................

cd /usr/local/extras/Genomics/apps/oligominer/2017-12-05

# wrapper to launch oligominer
cat > oligominer.sh << 'EOF'
#!/bin/bash

OLIGOMINERPATH="/usr/local/extras/Genomics/apps/oligominer/2017-12-05"
export PATH=$OLIGOMINERPATH/bowtie2-2.3.3.1/bin:$PATH
export PATH=$OLIGOMINERPATH/jellyfish-2.2.6/bin:$PATH
export PATH=$OLIGOMINERPATH/nupack-3.0.6/bin:$PATH
export NUPACKHOME=$OLIGOMINERPATH/nupack-3.0.6

cd $OLIGOMINERPATH

source activate oligominer
if [[ -z $1 ]];
then
	echo "Usage: oligominer <tool> <args>"
	echo
	echo "Examples:"
	echo "  oligominer python blockParse.py -f 3.fa"
	echo "  oligominer jellyfish count -s 3300M -m 18 -o hg38_18.jf --out-counter-len 1 -L 2 hg38.fa"
	echo "  oligominer bowtie2 -x /path_to_hg38_index/hg38 -U 3.fastq --no-hd -t -k 100 --very-sensitive-local -S 3_u.sam"
	echo "  oligominer python outputClean.py -u -f 3_u.sam"
	echo "  oligominer python outputClean.py -T 42 -f 3.sam"
	echo "  oligominer python kmerFilter.py -f 3_probes.bed -m 18 -j 18 -j sp.jf -k 4"
	echo "  oligominer python probeRC.py -f 3_probes.bed"
	echo "  oligominer python structureCheck.py -f 3_probes.bed -t 0.4"
	echo "  oligominer python probeTm.py -f 3.txt"
else
	echo "Running: $*"
	$*
fi
EOF
chmod +x oligominer.sh
ln -s oligominer.sh oligominer

cd ..
ln -s 2017-12-05 current
ln -s /usr/local/extras/Genomics/apps/oligominer/current/oligominer /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf OligoMiner
