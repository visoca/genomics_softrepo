#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o orgasm-b2.2.log

CURRDIR=$PWD
rm -rf org-asm >& /dev/null

conda remove -n py34oa --all -y
conda create -n py34oa python=3.4 anaconda -y
source activate py34oa

conda install pip sphinx cython
git clone https://git.metabarcoding.org/org-asm/org-asm.git
cd org-asm
python setup.py install


rm -rf /usr/local/extras/Genomics/apps/orgasm/b2.2 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/orgasm/b2.2

cd /usr/local/extras/Genomics/apps/orgasm/b2.2

# wrapper
cat > oa.sh << 'EOF'
#!/bin/bash

source activate py34oa
if [ -h $0 ]; then 
	DIR=$(dirname $(readlink -m $0)) 
else 
	DIR=$(dirname $0)
fi   

oa $*

EOF

chmod +x oa.sh
ln -s oa.sh oa

cd ..
ln -sTf b2.2 current
ln -sf /usr/local/extras/Genomics/apps/orgasm/current/oa /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf org-asm
