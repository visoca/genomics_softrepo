#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o p7zip-16.02.log

cd /local
rm -rf p7zip_16.02* >& /dev/null
wget https://kent.dl.sourceforge.net/project/p7zip/p7zip/16.02/p7zip_16.02_src_all.tar.bz2
tar -xf p7zip_16.02_src_all.tar.bz2
cd p7zip_16.02
make all3 -j 12

perl -pi -e 's/DEST_HOME\=\/usr\/local/DEST_HOME\=\/usr\/local\/extras\/Genomics\/apps\/p7zip\/16.02/g' install.sh
rm -rf /usr/local/extras/Genomics/apps/p7zip/16.02 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/p7zip/16.02
./install.sh
rm -rf p7zip-16.02*

cd /usr/local/extras/Genomics/apps/p7zip
rm current >& /dev/null
ln -s 16.02 current
ln -s /usr/local/extras/Genomics/apps/p7zip/current/bin/* /usr/local/extras/Genomics/bin/
