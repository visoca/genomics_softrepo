#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o paml-4.9h.log

if [[ -e '/usr/local/extras/Genomics' ]];
then 
	source /usr/local/extras/Genomics/.bashrc 
fi

CURRDIR=$PWD
rm -rf paml4.9h paml4.9h.tgz >& /dev/null

wget http://abacus.gene.ucl.ac.uk/software/paml4.9h.tgz
tar -xf paml4.9h.tgz
cd paml4.9h/src
make -f Makefile
mv baseml basemlg codeml pamp evolver yn00 chi2 mcmctree ../bin
make clean
perl -pi -e 's/\#define NS            400/\#define NS            5000/g' mcmctree.c
make mcmctree
mv mcmctree ../bin/mcmctree_large
cd ..
rm bin/*.exe
mkdir -p /usr/local/extras/Genomics/apps/paml/4.9h
cp -r * /usr/local/extras/Genomics/apps/paml/4.9h
cd /usr/local/extras/Genomics/apps/paml
ln -sTf 4.9h current
ln -s /usr/local/extras/Genomics/apps/paml/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf paml4.9h paml4.9h.tgz
