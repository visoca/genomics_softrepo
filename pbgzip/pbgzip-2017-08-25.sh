#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o pbgzip-2017-08-25.log

# use intel compiler
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
else # ShARC
	module add dev/intel-compilers/17.0.0
fi

CURRDIR=$PWD
rm -rf pbgzip >& /dev/null

git clone https://github.com/nh13/pbgzip.git
cd pbgzip
sh ./autogen.sh
./configure \
--enable-igzip \
--enable-ipp \
--prefix=/usr/local/extras/Genomics/apps/pbgzip/2017-08-25
make
rm -rf /usr/local/extras/Genomics/apps/pbgzip/2017-08-25 >& /dev/null
rm -rf /usr/local/extras/Genomics/apps/pbgzip/2017-08-25 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/pbgzip
ln -sf 2017-08-25 current

# Create wrapper
cd current/bin
mv pbgzip pbgzip.bin
cat > pbgzip.sh << 'EOF'
#!/bin/bash

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
else # ShARC
	module add dev/intel-compilers/17.0.0
fi

if [ -h $0 ]; then 
    DIR=$(dirname $(readlink -m $0)) 
else 
    DIR=$(dirname $0)
fi

$DIR/pbgzip.bin "$@"
EOF

chmod +x pbgzip.sh

ln -sf pbgzip.sh pbgzip

ln -sf /usr/local/extras/Genomics/apps/pbgzip/current/bin/pbgzip /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf pbgzip
