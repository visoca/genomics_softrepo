#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o pbgzip-2018-03-20.log

# use intel compiler
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
elif [[ $(hostname -d) == "sharc.shef.ac.uk" ]]; # ShARC
then
	module add dev/intel-compilers/17.0.0
fi

CURRDIR=$PWD
rm -rf pbgzip >& /dev/null

git clone https://github.com/nh13/pbgzip.git
cd pbgzip
sh ./autogen.sh
if [[ $(hostname -d) != "iceberg.shef.ac.uk" ]] && [[ $(hostname -d) != "sharc.shef.ac.uk" ]];
then
	./configure \
	--enable-igzip \
	--prefix=/usr/local/extras/Genomics/apps/pbgzip/2018-03-20
else
	./configure \
	--enable-intel64
	--enable-igzip \
	--enable-ipp \
	--prefix=/usr/local/extras/Genomics/apps/pbgzip/2018-03-20
fi
make
rm -rf /usr/local/extras/Genomics/apps/pbgzip/2018-03-20 >& /dev/null
rm -rf /usr/local/extras/Genomics/apps/pbgzip/2018-03-20 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/pbgzip
ln -sf 2018-03-20 current

# Create wrapper
cd current/bin
mv pbgzip pbgzip.bin
cat > pbgzip.sh << 'EOF'
#!/bin/bash

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
elif [[ $(hostname -d) == "sharc.shef.ac.uk" ]]; # ShARC
then
	module add dev/intel-compilers/17.0.0
fi

if [ -h $0 ]; then 
    DIR=$(dirname $(readlink -m $0)) 
else 
    DIR=$(dirname $0)
fi

$DIR/pbgzip.bin "$@"
EOF

chmod +x pbgzip.sh

ln -sf pbgzip.sh pbgzip

ln -sf /usr/local/extras/Genomics/apps/pbgzip/current/bin/pbgzip /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf pbgzip
