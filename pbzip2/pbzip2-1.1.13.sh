#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o pbzip2-1.1.13.log

CURRDIR=$PWD
rm -rf pbzip2-1.1.13 pbzip2-1.1.13.tar.gz >& /dev/null

wget https://launchpad.net/pbzip2/1.1/1.1.13/+download/pbzip2-1.1.13.tar.gz
tar -xf pbzip2-1.1.13.tar.gz
cd pbzip2-1.1.13
cp /usr/local/extras/Genomics/apps/bzip2/1.0.6/include/bzlib.h ./
cp /usr/local/extras/Genomics/apps/bzip2/1.0.6/lib/libbz2.a ./
make pbzip2-static
mkdir bin
mv pbzip2 bin/
make clean
rm -rf /usr/local/extras/Genomics/apps/pbzip2/1.1.13 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/pbzip2/1.1.13
cp -r * /usr/local/extras/Genomics/apps/pbzip2/1.1.13

cd /usr/local/extras/Genomics/apps/pbzip2
rm current >& /dev/null
ln -sTf 1.1.13 current
ln -sf /usr/local/extras/Genomics/apps/pbzip2/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf pbzip2-1.1.13 pbzip2-1.1.13.tar.gz
