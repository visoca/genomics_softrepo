#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o pcangsd-2018-06-04.log

conda remove -n py36pcangsd --all -y >& /dev/null
conda remove -n py27pcangsd --all -y >& /dev/null
conda create -n py27pcangsd python=2.7 anaconda -y
source activate py27pcangsd
conda install -c numpy scipy pandas numba pysnptools --y

CURRDIR=$PWD
rm -rf pcangsd >& /dev/null

git clone https://github.com/Rosemeis/pcangsd.git
cd pcangsd

# wrapper to call pcangsd
cat > pcangsd.sh << 'EOF'
#!/bin/bash

DIR=$(dirname $(readlink -m $0))

source activate py27pcangsd

python $DIR/pcangsd.py $*

source deactivate

EOF
chmod +x pcangsd.sh
ln -s pcangsd.sh pcangsd

rm -rf /usr/local/extras/Genomics/apps/pcangsd/2018-06-04 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/pcangsd/2018-06-04 >& /dev/null
cp -r * /usr/local/extras/Genomics/apps/pcangsd/2018-06-04/

cd /usr/local/extras/Genomics/apps/pcangsd
ln -sTf 2018-06-04 current
ln -sf /usr/local/extras/Genomics/apps/pcangsd/current/pcangsd /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf pcangsd

