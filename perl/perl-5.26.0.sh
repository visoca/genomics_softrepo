#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o perl-5.26.0.log

module add compilers/gcc/6.2

CURRDIR=$PWD
rm -rf perl-5.26.0 perl-5.26.0.tar.gz >& /dev/null

wget http://www.cpan.org/src/5.0/perl-5.26.0.tar.gz
tar -xf perl-5.26.0.tar.gz
cd perl-5.26.0
./Configure \
-Dcc=gcc \
-des \
-Doptimize='-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong' \
-Dusethreads \
-Duse64bitall \
-Duselongdouble \
-Dprefix=/usr/local/extras/Genomics/apps/perl/5.26.0

# centos 7 flags:
# -Doptimize='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic'

# NOTE: For 128bit precision, libquadmath can be used (adding the lines 
# below), but then a wrapper to add the path to such library is required
# -Dusequadmath \
# -Dloclibpth='/usr/local/packages6/compilers/gcc/6.2.0/lib /usr/local/packages6/compilers/gcc/6.2.0/lib64' \
# -Dlocincpth='/usr/local/packages6/compilers/gcc/6.2.0/include' \

make -j12
make test
rm -rf /usr/local/extras/Genomics/apps/perl/5.26.0 >& /dev/null
make install

# autoconfigure cpan (ensure there is not MyConfig.pm file in $HOME)
# /usr/local/extras/Genomics/apps/perl/5.26.0/bin/perl -MCPAN -e shell
# cp $HOME/.cpan/CPAN/MyConfig.pm /usr/local/extras/Genomics/apps/perl/5.26.0/lib/5.26.0/CPAN/Config.pm

# install cpanminus
/usr/local/extras/Genomics/apps/perl/5.26.0/bin/cpan App::cpanminus

# install PerlBench
/usr/local/extras/Genomics/apps/perl/5.26.0/bin/cpanm PerlBench

# install bioperl
/usr/local/extras/Genomics/apps/perl/5.26.0/bin/cpanm Bio::Perl

# Get list of modules installed in previous version and install them
MODULES_OLD=$(/usr/local/extras/Genomics/apps/perl/5.16.3/bin/perldoc perllocal | grep Module | awk '{print $7}')
MODULES_NEW=$(/usr/local/extras/Genomics/apps/perl/5.26.0/bin/perldoc perllocal | grep Module | awk '{print $7}')
ALL_MODULES="$MODULES_OLD $MODULES_NEW"
MODULES2INSTALL=$(echo $ALL_MODULES | tr -d '[0m' | tr -dc '[:print:]'| sort | uniq -u)

echo $MODULES2INSTALL | xargs -I {} sh -c '/usr/local/extras/Genomics/apps/perl/5.26.0/bin/cpanm {}'

# clean up .cpanm
rm -rf $HOME/.cpanm/work/*

cd /usr/local/extras/Genomics/apps/perl
rm current >& /dev/null
ln -s 5.26.0 current
ln -s /usr/local/extras/Genomics/apps/perl/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf perl-5.26.0 perl-5.26.0.tar.gz

