#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 12
#$ -V
#$ -j y
#$ -o perlbrew-0.80.log

# Known issues
# B::Lint, 1 test fails lint.t line 119, it is a bug and can be ignored https://rt.cpan.org/Public/Bug/Display.html?id=101115
# XML::DOM::XPath, 1 test fails, very likely unharmful and can be ignored
if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then
	module add compilers/gcc/6.2
else
	module add dev/gcc/6.2
fi

# switch off perlbrew first if already installed
if [[ ! -z $(command -v perlbrew) ]]
then
	perlbrew switch-off
fi

export PERLBREW_ROOT=/usr/local/extras/Genomics/apps/perlbrew/0.80
export PERLBREW_HOME=$HOME/.perlbrew-softrepo

rm -rf $PERLBREW_ROOT $PWD/.cpanm >& /dev/null

# install perlbrew
wget -O - https://install.perlbrew.pl | bash

# set default root for perlbrew
sed -i "8 a \\\t\$ENV{'PERLBREW_ROOT'}='$PERLBREW_ROOT';" $PERLBREW_ROOT/bin/perlbrew

# build perl without threads support (faster when no threads are required)
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew -j12 install 5.26.1 \
-Dcc=gcc \
-des \
-Doptimize='-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong' \
-Duse64bitall \
-Duselongdouble

# build perl with threads support
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew -j12 install 5.26.1 --as perl-5.26.1-thread \
-Dcc=gcc \
-des \
-Doptimize='-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong' \
-Duse64bitall \
-Duselongdouble \
-Dusethreads

# set default version
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew switch perl-5.26.1

# install cpanminus
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew install-cpanm
export PERL_CPANM_HOME=$PWD/.cpanm

# install PerlBench
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew exec cpanm PerlBench

# install bioperl
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew exec cpanm Bio::Perl

# install some modules that require special configuration
/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew exec cpanm \
--configure-args="EXPATLIBPATH=/usr/local/extras/Genomics/lib/expat/current/lib EXPATINCPATH=/usr/local/extras/Genomics/lib/expat/current/include" \
XML::Parser

# Get list of modules installed in previous version and install them
# MODULES_OLD=$(/usr/local/extras/Genomics/apps/perl/5.16.3/bin/perldoc perllocal | grep Module | awk '{print $7}')
MODULES_OLD=$(/usr/local/extras/Genomics/apps/perlbrew/0.76/perls/perl-5.26.0/bin/perldoc perllocal | grep Module | awk '{print $7}')
MODULES_NEW=$(/usr/local/extras/Genomics/apps/perlbrew/0.80/perls/perl-5.26.1/bin/perldoc perllocal | grep Module | awk '{print $7}')
ALL_MODULES="$MODULES_OLD $MODULES_NEW"
MODULES2INSTALL=$(echo $ALL_MODULES | perl -pe 's/\[0m//g' | tr -dc '[:print:]'| sort | uniq -u)

echo $MODULES2INSTALL | xargs -I {} sh -c '/usr/local/extras/Genomics/apps/perlbrew/0.80/bin/perlbrew exec cpanm {}'

# clean up .cpanm
rm -rf $PWD/.cpanm*

cd /usr/local/extras/Genomics/apps/perlbrew
rm current >& /dev/null
ln -sTf 0.80 current
ln -sf /usr/local/extras/Genomics/apps/perlbrew/current/bin/* /usr/local/extras/Genomics/bin/

