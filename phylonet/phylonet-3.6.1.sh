#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o phylonet-3.6.1.log

CURRDIR=$PWD
rm -rf phylonet >& /dev/null

mkdir phylonet
cd phylonet
wget https://bioinfocs.rice.edu/sites/g/files/bxs266/f/kcfinder/files/PhyloNet_3.6.1.jar

cat > phylonet.sh << 'EOF'
#!/bin/bash

export JAVA_HOME='/usr/local/extras/Genomics/apps/java/current'
export PATH=/usr/local/extras/Genomics/apps/java/current/bin:$PATH

if [[ -z $MEM ]];
then
	MEM=3 # memory in GB
fi

if [ -h $0 ]; then
	DIR=$(dirname $(readlink -m $0))
else
	DIR=$(dirname $0)
fi

if [[ $# -eq 0 || $1 == "-h" || $1 == "--help" || $1 == "-help" ]]; then
	echo
	echo "This is a wrapper to use PhyloNet with the simple command 'phylonet'."
	echo
	echo "  Maximum Java heap size (Xmx) set to $MEM""g"
	echo "  You can change this value with: MEM=<memory in GB> phylonet <options>"
	echo "    example: MEM=6 phylonet script.nex"
	echo
	echo "Usage: phylonet <arguments>"
	echo
	java -Xmx"$MEM"g -jar $DIR/PhyloNet_3.6.1.jar
	echo
	exit
fi

echo
echo "Maximum Java heap size (Xmx) set to $MEM""g"
echo
java -Xmx"$MEM"g -jar $DIR/PhyloNet_3.6.1.jar $*


EOF
chmod +x phylonet.sh
ln -s phylonet.sh phylonet

rm -rf /usr/local/extras/Genomics/apps/phylonet/3.6.1 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/phylonet/3.6.1
cp -rL * /usr/local/extras/Genomics/apps/phylonet/3.6.1/

cd /usr/local/extras/Genomics/apps/phylonet
ln -sTf 3.6.1 current
ln -sf /usr/local/extras/Genomics/apps/phylonet/current/phylonet /usr/local/extras/Genomics/bin/
cd current

cd $CURRDIR
rm -rf phylonet
