#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
##$ -pe openmp 12
#$ -j y
#$ -o poolstat-20190205.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf poolstat >& /dev/null

git clone https://bitbucket.org/WegmannLab/poolstat
cd poolstat
cp makefile makefile.orig
sed -i '16 a \\t$(CC) -O3 -o $(BIN) $(OBJ) /usr/local/extras/Genomics/lib/lib/libhts.so -lz -lm -llzma -pthread -lcurl' makefile
sed -i '25 a \\t$(CC) -O3 -c -Ibamtools -I/usr/local/extras/Genomics/lib/include -std=c++1y $< -o $@' makefile
sed -i '14,16 {s/^/#/}; 23,25 {s/^/#/};' makefile
make

mkdir bin
mv poolStat bin/
make clean

rm -rf /usr/local/extras/Genomics/apps/poolstat/20190205 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/poolstat/20190205
cp -r * /usr/local/extras/Genomics/apps/poolstat/20190205/

cd /usr/local/extras/Genomics/apps/poolstat
rm current >& /dev/null
ln -sTf 20190205 current
ln -s /usr/local/extras/Genomics/apps/poolstat/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf poolstat

echo "=============================================================================="
