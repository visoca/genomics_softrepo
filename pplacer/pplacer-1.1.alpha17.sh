#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o pplacer-1.17.alpha17.log

CURRDIR=$PWD
rm -rf pplacer-Linux-v1.1.alpha17 pplacer-Linux-v1.1.alpha17.zip >& /dev/null

wget https://github.com/matsen/pplacer/releases/download/v1.1.alpha17/pplacer-Linux-v1.1.alpha17.zip
unzip pplacer-Linux-v1.1.alpha17.zip
cd pplacer-Linux-v1.1.alpha17
rm -rf /usr/local/extras/Genomics/apps/pplacer/1.17.alpha17 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/pplacer/1.17.alpha17
cp -r * /usr/local/extras/Genomics/apps/pplacer/1.17.alpha17/

cd /usr/local/extras/Genomics/apps/pplacer
ln -sTf 1.17.alpha17 current
ln -sf /usr/local/extras/Genomics/apps/pplacer/current/* /usr/local/extras/Genomics/bin/
# ln -s /usr/local/extras/Genomics/apps/pplacer/current/scripts/*.py /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf pplacer-Linux-v1.1.alpha17 pplacer-Linux-v1.1.alpha17.zip
