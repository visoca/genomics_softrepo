#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o prodigal-2.6.3.log

CURRDIR=$PWD
rm -rf Prodigal-2.6.3 Prodigal-2.6.3.tar.gz >& /dev/null
wget https://github.com/hyattpd/Prodigal/archive/v2.6.3.tar.gz -O Prodigal-2.6.3.tar.gz
tar -xf Prodigal-2.6.3.tar.gz
cd Prodigal-2.6.3
perl -pi -e 's/^INSTALLDIR.*/INSTALLDIR \= \/usr\/local\/extras\/Genomics\/apps\/prodigal\/2\.6\.3/g' Makefile
make
rm -rf /usr/local/extras/Genomics/apps/prodigal/2.6.3 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/prodigal
rm current >& /dev/null
ln -s 2.6.3 current

ln -sf /usr/local/extras/Genomics/apps/prodigal/current/prodigal /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf Prodigal-2.6.3 Prodigal-2.6.3.tar.gz
