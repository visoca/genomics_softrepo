#!/bin/bash
#$ -j y
#$ -o proovread-2.14.1.log

CURRDIR=$PWD
rm -rf proovread-2.14.1 proovread-2.14.1.tar.gz >& /dev/null

wget https://github.com/BioInf-Wuerzburg/proovread/releases/download/proovread-2.14.1/proovread-2.14.1.tar.gz
tar -xvf proovread-2.14.1.tar.gz
cd proovread-2.14.1

# custom samtools, ncbi-blast, daligner, and dazzler-db installation
# -------------------------------------------------------------------------------------------------
cd util 

wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2
mkdir tmp
tar -xvf samtools-1.9.tar.bz2 -C tmp
cd tmp/samtools-1.9
make
mkdir ../../samtools-1.9
cp samtools ../../samtools-1.9
cd ../..
rm -r tmp samtools-1.9.tar.bz2

wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.8.1+-x64-linux.tar.gz
tar -xvf ncbi-blast-2.8.1+-x64-linux.tar.gz
rm ncbi-blast-2.8.1+-x64-linux.tar.gz

mkdir tmp
cd tmp
git clone https://github.com/thegenemyers/DALIGNER
cd DALIGNER
make
mkdir ../../daligner
find . -type f -executable | grep -v ".git" | xargs -I {} sh -c 'cp {} ../../daligner'
cd ../..
rm -rf tmp

mkdir tmp
cd tmp
git clone https://github.com/thegenemyers/DAZZ_DB
cd DAZZ_DB
make
mkdir ../../dazz_db
find . -type f -executable | grep -v ".git" | xargs -I {} sh -c 'cp {} ../../dazz_db'
cd ../..
rm -rf tmp

cd ..
# ----------------------------------------------------

# edit core config
perl -pi -e "s/^\'daligner\-path\'.*/\'daligner\-path\' \=\> \\\$RealBin\.\'\/\.\.\/util\/daligner\/\'\,/g" proovread.cfg
perl -pi -e "s/^\'dazz\-db\-path\'.*/\'dazz\-db\-path\' \=\> \\\$RealBin\.\'\/\.\.\/util\/dazz\-db\/\'\,/g" proovread.cfg
perl -pi -e "s/^\'samtools\-path\'.*/\'samtools\-path\' \=\> \\\$RealBin\.\'\/\.\.\/util\/samtools\-1\.9\/\'\,/g" proovread.cfg
perl -pi -e "s/^\'blast\-path\'.*/\'blast\-path\' \=\> \\\$RealBin\.\'\/\.\.\/util\/ncbi\-blast\-2\.8\.1\+\/bin\/\'\,/g" proovread.cfg

make

rm -rf /usr/local/extras/Genomics/apps/proovread/2.14.1 >& /dev/null
make install PREFIX=/usr/local/extras/Genomics/apps/proovread/2.14.1

cd /usr/local/extras/Genomics/apps/proovread
ln -sTf 2.14.1 current

cd /usr/local/extras/Genomics/apps/proovread/2.14.1/bin
mv proovread proovread.bin
cat > proovread.sh <<'EOF'
#!/bin/bash

# Wrapper to run proovread

# load genomics repository
source /usr/local/extras/Genomics/.bashrc >& /dev/null

# switch to threaded perl version to run proovread
perlbrew switch perl-5.26.1-thread

/usr/local/extras/Genomics/apps/proovread/current/bin/proovread.bin $*

# switch back no-threaded perl
perlbrew switch perl-5.26.1

EOF

chmod +x proovread.sh
ln -s proovread.sh proovread

ln -s /usr/local/extras/Genomics/apps/proovread/current/bin/* /usr/local/extras/Genomics/bin

cd $CURRDIR
rm -rf proovread-2.14.1 proovread-2.14.1.tar.gz
