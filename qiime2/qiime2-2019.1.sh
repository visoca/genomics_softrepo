#!/bin/bash
#$ -j y
#$ -o qiime2-2019.1.log

conda remove --name py36qiime2-2019.1 --all
wget https://data.qiime2.org/distro/core/qiime2-2019.1-py36-linux-conda.yml
conda env create --name py36qiime2-2019.1 python=3.6 --file qiime2-2019.1-py36-linux-conda.yml

# source activate py36qiime2-2019.1
