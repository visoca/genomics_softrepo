#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -j y
#$ -o raxml-8.2.11.log

CURRDIR=$PWD
rm -rf standard-RAxML-8.2.11 raxml-8.2.11.tar.gz >& /dev/null

if [[ $(hostname -d) == "iceberg.shef.ac.uk" ]];
then # Iceberg
	module add compilers/intel/17.0.0
	module add mpi/intel/openmpi/1.10.0
else # ShARC
	module add dev/intel-compilers/17.0.0
	module add mpi/openmpi/2.0.1/intel-17.0.0
fi

wget https://github.com/stamatak/standard-RAxML/archive/v8.2.11.tar.gz -O raxml-8.2.11.tar.gz
tar -xf raxml-8.2.11.tar.gz
cd standard-RAxML-8.2.11
perl -pi -e 's/CC = gcc/CC = icc/g' Makefile.*

mkdir bin

make -f Makefile.AVX2.gcc
mv raxmlHPC-AVX2 bin/
rm *.o

make -f Makefile.AVX.gcc
mv raxmlHPC-AVX bin/
rm *.o

make -f Makefile.SSE3.gcc
mv raxmlHPC-SSE3 bin/
rm *.o

make -f Makefile.AVX2.PTHREADS.gcc
mv raxmlHPC-PTHREADS-AVX2 bin/
rm *.o

make -f Makefile.AVX.PTHREADS.gcc
mv raxmlHPC-PTHREADS-AVX bin/
rm *.o

make -f Makefile.SSE3.PTHREADS.gcc
mv raxmlHPC-PTHREADS-SSE3 bin/
rm *.o

make -f Makefile.AVX2.MPI.gcc
mv raxmlHPC-MPI-AVX2 bin/
rm *.o

make -f Makefile.AVX.MPI.gcc
mv raxmlHPC-MPI-AVX bin/
rm *.o

make -f Makefile.SSE3.MPI.gcc
mv raxmlHPC-MPI-SSE3 bin/
rm *.o

make -f Makefile.AVX2.HYBRID.gcc
mv raxmlHPC-HYBRID-AVX2 bin/
rm *.o

make -f Makefile.AVX.HYBRID.gcc
mv raxmlHPC-HYBRID-AVX bin/
rm *.o

make -f Makefile.SSE3.HYBRID.gcc
mv raxmlHPC-HYBRID-SSE3 bin/
rm *.o


rm -rf /usr/local/extras/Genomics/apps/raxml/8.2.11 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/raxml/8.2.11
cp -r * /usr/local/extras/Genomics/apps/raxml/8.2.11/

cd /usr/local/extras/Genomics/apps/raxml
ln -sTf 8.2.11 current
ln -sf /usr/local/extras/Genomics/apps/raxml/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf standard-RAxML-8.2.11 raxml-8.2.11.tar.gz
