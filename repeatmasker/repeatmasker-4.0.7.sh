#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o repeatmasker-4.0.7.log

CURRDIR=$PWD
rm -rf RepeatMasker RepeatMasker-open-4-0-7.tar.gz >& /dev/null
wget http://www.repeatmasker.org/RepeatMasker-open-4-0-7.tar.gz
tar -xf RepeatMasker-open-4-0-7.tar.gz
cd RepeatMasker/Libraries
wget --user=visoca --password=vdnrau https://www.girinst.org/server/RepBase/protected/repeatmaskerlibraries/RepBaseRepeatMaskerEdition-20170127.tar.gz
tar -xvf RepBaseRepeatMaskerEdition-20170127.tar.gz -C ../
rm RepBaseRepeatMaskerEdition-20170127.tar.gz
wget http://www.dfam.org/web_download/Current_Release/Dfam.hmm.gz
gzip -d Dfam.hmm.gz

cd ..
mkdir -p /usr/local/extras/Genomics/apps/repeatmasker/4.0.7
cp -r * /usr/local/extras/Genomics/apps/repeatmasker/4.0.7/
cd /usr/local/extras/Genomics/apps/repeatmasker
rm current
ln -s 4.0.7 current
cd current
perl ./configure << EOF



/usr/local/extras/Genomics/apps/trf/4.04/trf
2
/usr/local/extras/Genomics/apps/rmblast/2.6.0v2/bin
y
4
/usr/local/extras/Genomics/apps/hmmer/3.1b2/binaries
n
5
EOF

cd $CURRDIR
rm -rf RepeatMasker RepeatMasker-open-4-0-7.tar.gz >& /dev/null
