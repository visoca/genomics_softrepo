#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o repeatmodeler-1.0.11.log

CURRDIR=$PWD

rm -rf RepeatModeler-open-1.0.11 RepeatModeler-open-1.0.11.tar.gz >& /dev/null
wget http://www.repeatmasker.org/RepeatModeler/RepeatModeler-open-1.0.11.tar.gz
tar -xf RepeatModeler-open-1.0.11.tar.gz
cd RepeatModeler-open-1.0.11

# Install customized programs
# ======================================================================================
# recon
# --------------------------------------------------------------------------------------
rm -rf RECON-1.08.tar.gz RECON-1.08 recon >& /dev/null
wget http://www.repeatmasker.org/RepeatModeler/RECON-1.08.tar.gz
tar -xf RECON-1.08.tar.gz
mv RECON-1.08 recon
cd recon/src
make
make install
make clean
cd ../..
rm -f RECON-1.08.tar.gz
# --------------------------------------------------------------------------------------

# nseg (requirement of repeatscout)
# --------------------------------------------------------------------------------------
rm -rf nseg >& /dev/null
wget --recursive --no-parent -nH --cut-dirs=2 ftp://ftp.ncbi.nih.gov/pub/seg/nseg
cd nseg
make
cd ..
# --------------------------------------------------------------------------------------

# repeatscout
# --------------------------------------------------------------------------------------
rm -rf RepeatScout-1.0.5.tar.gz RepeatScout-1 repeatscout >& /dev/null
wget http://bix.ucsd.edu/repeatscout/RepeatScout-1.0.5.tar.gz
tar -xf RepeatScout-1.0.5.tar.gz
mv RepeatScout-1 repeatscout
cd repeatscout
make
cd ..
rm -f RepeatScout-1.0.5.tar.gz
# --------------------------------------------------------------------------------------
# ======================================================================================


# configure repeatmodeler
cat RepModelConfig.pm.tmpl | perl -pe '
if (/\$REPEATMASKER_DIR\s+\=/){
	$_="# $_";
	$_.="  \$REPEATMASKER_DIR = \"/usr/local/extras/Genomics/apps/repeatmasker/4.0.7\";\n";
}
if (/\$RMBLAST_DIR\s+\=/){
	$_="# $_";
	$_.="  \$RMBLAST_DIR = \"/usr/local/extras/Genomics/apps/rmblast/2.6.0v2/bin\";\n";
}
if (/\$RECON_DIR\s+\=/){
	$_="# $_";
	$_.="  \$RECON_DIR = \"/usr/local/extras/Genomics/apps/repeatmodeler/1.0.11/recon/bin\";\n";
}
if (/\$TRF_PRGM\s+\=/){
	$_="# $_";
	$_.="  \$TRF_PRGM = \"/usr/local/extras/Genomics/apps/trf/4.04/trf\";\n";
}
if (/\$RSCOUT_DIR\s+\=/){
	$_="# $_";
	$_.="  \$RSCOUT_DIR = \"/usr/local/extras/Genomics/apps/repeatmodeler/1.0.11/repeatscout\";\n";
}
' > RepModelConfig.pm


# change perl paths
grep -lr 'bin/perl' | xargs -I {} sh -c 'perl -pi -e "s/\#\!.*\/bin\/perl/\#\!\/usr\/bin\/env perl/g" {}'

rm -rf /usr/local/extras/Genomics/apps/repeatmodeler/1.0.11 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/repeatmodeler/1.0.11
cp -r * /usr/local/extras/Genomics/apps/repeatmodeler/1.0.11/
cd /usr/local/extras/Genomics/apps/repeatmodeler
ln -sTf 1.0.11 current
ln -sf /usr/local/extras/Genomics/apps/repeatmodeler/current/RepeatModeler /usr/local/extras/Genomics/bin/
ln -sf /usr/local/extras/Genomics/apps/repeatmodeler/current/BuildDatabase /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf RepeatModeler-open-1.0.11 RepeatModeler-open-1.0.11.tar.gz
