#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 24
#$ -j y
#$ -o rmblast-2.6.0v2.log

module add compilers/gcc/4.8.2

CURRDIR=$PWD
rm -rf ncbi-blast-2.6.0+-src.tar.gz ncbi-blast-2.6.0+-src isb-2.6.0+-changes-vers2.patch.gz isb-2.6.0+-changes-vers2.patch >& /dev/null

wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.6.0/ncbi-blast-2.6.0+-src.tar.gz
wget http://www.repeatmasker.org/isb-2.6.0+-changes-vers2.patch.gz
tar -xf ncbi-blast-2.6.0+-src.tar.gz
gzip -d isb-2.6.0+-changes-vers2.patch.gz

cd ncbi-blast-2.6.0+-src
patch -p1 < ../isb-2.6.0+-changes-vers2.patch
cd c++
./configure \
--with-mt \
--without-debug \
--prefix=/usr/local/extras/Genomics/apps/rmblast/2.6.0v2
make -j 24
rm -rf /usr/local/extras/Genomics/apps/rmblast/2.6.0v2 >& /dev/null
make install
# install gives an error, but it is benign and doesn't seem to have serious effects

cd /usr/local/extras/Genomics/apps/rmblast
ln -sTf 2.6.0v2 current
# ln -sf /usr/local/extras/Genomics/apps/rmblast/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf ncbi-blast-2.6.0+-src.tar.gz ncbi-blast-2.6.0+-src isb-2.6.0+-changes-vers2.patch
