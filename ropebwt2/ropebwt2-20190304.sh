#!/bin/bash
#$ -j y
#$ -o ropebwt2-20201903044.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf ropebwt2 >& /dev/null

git clone https://github.com/lh3/ropebwt2.git
cd ropebwt2
make
mkdir bin
mv ropebwt2 bin/
make clean

rm -rf /usr/local/extras/Genomics/apps/ropebwt2/20190304 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/ropebwt2/20190304
cp -r * /usr/local/extras/Genomics/apps/ropebwt2/20190304/

cd /usr/local/extras/Genomics/apps/ropebwt2
rm current >& /dev/null
ln -sTf 20190304 current
ln -s /usr/local/extras/Genomics/apps/ropebwt2/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf ropebwt2

echo "=============================================================================="
