#!/bin/bash
#$ -j y
:u
#$ -o sabre-20190305.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf sabre >& /dev/null

git clone https://github.com/najoshi/sabre.git
cd sabre
make
mkdir bin
mv sabre bin/
make clean

rm -rf /usr/local/extras/Genomics/apps/sabre/20190305 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/sabre/20190305
cp -r * /usr/local/extras/Genomics/apps/sabre/20190305/

cd /usr/local/extras/Genomics/apps/sabre
rm current >& /dev/null
ln -sTf 20190305 current
ln -s /usr/local/extras/Genomics/apps/sabre/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf sabre

echo "=============================================================================="
