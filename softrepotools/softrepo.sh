#!/bin/bash
##$ -P popgenom
##$ -q popgenom.q
#$ -l h_rt=00:10:00
#$ -j y
#$ -o bashrc.log

mkdir -p /usr/local/extras/Genomics/softrepo >& /dev/null


cat > /usr/local/extras/Genomics/softrepo/softrepo.sh <<'EOF'
#!/bin/bash

SOFTREPOROOT=/usr/local/extras/Genomics

# list installed programs, libraries, R packages, and perl modules

# programs
PROGRAMS=$(find $SOFTREPOROOT/apps -mindepth 1 -maxdepth 1 -exec basename {} \; | sort)
echo
echo "Programs"
echo "---------------------"
echo -e "program\tcurrent\tother"
echo "....................."
for p in $PROGRAMS;
do
	versions=$(find $SOFTREPOROOT/apps/$p -maxdepth 1 -type d -name "*[0-9]*" -exec basename {} \; | sort -rV | perl -pe 's/\n/,/g' | perl -pe 's/,$//g;s/,/\t/;')
	echo -e "$p\t$versions"
done
echo "---------------------"

# libraries
LIBRARIES=$(find $SOFTREPOROOT/lib -mindepth 1 -maxdepth 1 -not -name "lib" -not -name "bin" -not -name "include"  -exec basename {} \; | sort)
echo
echo "Libraries"
echo "---------------------"
echo -e "library\tcurrent\tother"
echo "....................."
for l in $LIBRARIES;
do
	versions=$(find $SOFTREPOROOT/lib/$l -mindepth 1 -maxdepth 1 -type d -name "*[0-9]*" -exec basename {} \; | sort -rV | perl -pe 's/\n/,/g' | perl -pe 's/,$//g;s/,/\t/;')
	echo -e "$l\t$versions"
done
echo    "---------------------"

# R packages
echo
echo "R packages"
echo "---------------------"
Rscript --quiet -e 'data.frame(package=installed.packages()[, "Package"], version=installed.packages()[,"Version"], row.names=NULL)'
echo "---------------------"

# perl modules
echo
echo "Perl modules"
echo "---------------------"
perldoc perllocal | grep Module | awk '{print $7}' | sort
echo "---------------------"
echo
EOF

chmod +x /usr/local/extras/Genomics/softrepo/softrepo.sh

ln -sf /usr/local/extras/Genomics/softrepo/softrepo.sh /usr/local/extras/Genomics/softrepo/softrepo

ln -sf /usr/local/extras/Genomics/softrepo/softrepo /usr/local/extras/Genomics/bin/
