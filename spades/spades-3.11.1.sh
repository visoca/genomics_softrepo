#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -l rmem=16g
#$ -j y
#$ -o spades-3.11.1.log

source /usr/local/extras/Genomics/.bashrc

module add compilers/gcc/4.8.2

CURRDIR=$PWD
rm -rf SPAdes-3.11.1 SPAdes-3.11.1.tar.gz >& /dev/null

wget http://cab.spbu.ru/files/release3.11.1/SPAdes-3.11.1.tar.gz
tar -xzf SPAdes-3.11.1.tar.gz
cd SPAdes-3.11.1

rm -rf /usr/local/extras/Genomics/apps/spaces/3.11.1 >& /dev/null
PREFIX=/usr/local/extras/Genomics/apps/spades/3.11.1 ./spades_compile.sh

cd /usr/local/extras/Genomics/apps/spades
rm current >& /dev/null
ln -s 3.11.1 current

find /usr/local/extras/Genomics/apps/spades/3.11.1/bin -type f -executable | \
xargs -I {} sh -c 'ln -sf {} /usr/local/extras/Genomics/bin/'

cd $CURRDIR
rm -rf SPAdes-3.11.1 SPAdes-3.11.1.tar.gz
