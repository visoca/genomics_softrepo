#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o sparcc-2018-02-22.log

CURRDIR=$PWD
rm -rf sparcc >& /dev/null

hg clone https://bitbucket.org/yonatanf/sparcc

cd sparcc

# wrapper
cat > sparcc.sh << 'EOF'
#!/bin/bash

source activate py27
if [ -h $0 ]; then 
	DIR=$(dirname $(readlink -m $0)) 
else 
	DIR=$(dirname $0)
fi   

python $DIR/SparCC.py $*

EOF

chmod +x sparcc.sh
ln -s sparcc.sh sparcc

rm -rf /usr/local/extras/Genomics/apps/sparcc/2018-02-22 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/sparcc/2018-02-22
cp -r * /usr/local/extras/Genomics/apps/sparcc/2018-02-22/

cd /usr/local/extras/Genomics/apps/sparcc
ln -sTf 2018-02-22 current
ln -sf /usr/local/extras/Genomics/apps/sparcc/current/sparcc /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf sparcc
