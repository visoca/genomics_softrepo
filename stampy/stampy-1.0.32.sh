#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o stampy-1.0.32.log

UNFINISHED

# create anaconda environment
conda remove --name py27-stampy --all >& /dev/null
conda create -n py27-stampy python=2.7 anaconda
source activate py27-stampy

CURRDIR=$PWD
rm -rf stampy-1.0.32 Stampy-latest.tgz >& /dev/null

wget http://www.well.ox.ac.uk/bioinformatics/Software/Stampy-latest.tgz
tar -xf Stampy-latest.tgz
cd Stampy-1.0.32
make

rm -rf /usr/local/extras/Genomics/apps/stampy/1.0.32 >& /dev/null


cd $CURRDIR
rm -rf stampy-1.0.32 Stampy-latest.tgz
