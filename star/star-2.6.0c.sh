#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o star-2.6.0c.log

module add compilers/gcc/4.8.2

CURRDIR=$PWD
rm -rf STAR-2.6.0c.tar.gz STAR-2.6.0c >& /dev/null

wget https://github.com/alexdobin/STAR/archive/2.6.0c.tar.gz -O STAR-2.6.0c.tar.gz
tar -xf STAR-2.6.0c.tar.gz
cd STAR-2.6.0c
rm -r bin/*
cd source
make STAR
mv STAR ../bin/
make clean
make STARlong
mv STARlong ../bin/
make clean
cd ..

rm -rf /usr/local/extras/Genomics/apps/star/2.6.0c >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/star/2.6.0c
cp -r * /usr/local/extras/Genomics/apps/star/2.6.0c/
cd /usr/local/extras/Genomics/apps/star
ln -sTf 2.6.0c current
ln -sf /usr/local/extras/Genomics/apps/star/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf STAR-2.6.0c.tar.gz STAR-2.6.0c

