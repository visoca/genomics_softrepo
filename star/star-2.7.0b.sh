#!/bin/bash
#$ -j y
#$ -o star-2.7.0b.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf STAR-2.7.0b.tar.gz STAR-2.7.0b >& /dev/null

wget https://github.com/alexdobin/STAR/archive/2.7.0b.tar.gz -O STAR-2.7.0b.tar.gz
tar -xf STAR-2.7.0b.tar.gz
cd STAR-2.7.0b
rm -r bin/*
cd source
make STAR
mv STAR ../bin/
make clean
make STARlong
mv STARlong ../bin/
make clean
cd ..

rm -rf /usr/local/extras/Genomics/apps/star/2.7.0b >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/star/2.7.0b
cp -r * /usr/local/extras/Genomics/apps/star/2.7.0b/
cd /usr/local/extras/Genomics/apps/star
ln -sTf 2.7.0b current
ln -sf /usr/local/extras/Genomics/apps/star/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf STAR-2.7.0b.tar.gz STAR-2.7.0b

echo "=============================================================================="
