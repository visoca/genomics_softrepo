#!/bin/bash
#$ -j y
#$ -o stringtie-1.3.5.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf stringtie-1.3.5.tar.gz stringtie-1.3.5 >& /dev/null

wget http://ccb.jhu.edu/software/stringtie/dl/stringtie-1.3.5.tar.gz
tar -xf stringtie-1.3.5.tar.gz
cd stringtie-1.3.5
make release
mkdir bin
cp stringtie bin/
make clean

rm -rf /usr/local/extras/Genomics/apps/stringtie/1.3.5 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/stringtie/1.3.5
cp -r * /usr/local/extras/Genomics/apps/stringtie/1.3.5/

cd /usr/local/extras/Genomics/apps/stringtie
rm current >& /dev/null
ln -sTf 1.3.5 current
ln -s /usr/local/extras/Genomics/apps/stringtie/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf stringtie-1.3.5.tar.gz stringtie-1.3.5

echo "=============================================================================="
