#!/bin/bash
#$ -j y
#$ -o tophat-2.1.2.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

# Load boost library
module add libs/boost/1.64.0/gcc-4.9.4

CURRDIR=$PWD

rm -rf  tophat-2.1.2 tophat-2.1.2.tar.gz >& /dev/null
wget https://github.com/infphilo/tophat/archive/v2.1.2.tar.gz -O tophat-2.1.2.tar.gz
tar -xf tophat-2.1.2.tar.gz
cd tophat-2.1.2
./autogen.sh
./configure \
--prefix=/usr/local/extras/Genomics/apps/tophat/2.1.2
make
rm -rf /usr/local/extras/Genomics/apps/tophat/2.1.2 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/tophat
ln -sfT 2.1.2 current
find /usr/local/extras/Genomics/apps/tophat/2.1.0/bin -maxdepth 1 -type f | \
xargs -I {} sh -c 'ln -sf {} /usr/local/extras/Genomics/bin/'

cd $CURRDIR
rm -rf  tophat-2.1.2 tophat-2.1.2.tar.gz

echo "=============================================================================="

