#!/bin/bash
#$ -j y
#$ -o tree-1.8.0.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf tree-1.8.0.tgz tree-1.8.0 >& /dev/null

wget ftp://mama.indstate.edu/linux/tree/tree-1.8.0.tgz
tar -xf tree-1.8.0.tgz
cd tree-1.8.0
perl -pi -e 's/prefix \=.*/prefix \= \/usr\/local\/extras\/Genomics\/apps\/tree\/1\.8\.0/g' Makefile
make
rm -rf /usr/local/extras/Genomics/apps/tree/1.8.0 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/tree
ln -sTf 1.8.0 current
ln -s /usr/local/extras/Genomics/apps/tree/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf tree-1.8.0.tgz tree-1.8.0

echo "=============================================================================="
