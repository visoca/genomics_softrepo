#!/bin/bash
#$ -j y
#$ -o trimmomatic-0.38.log

hostname
date
echo "=============================================================================="

# Load genomics software repository
source /usr/local/extras/Genomics/.bashrc 

CURRDIR=$PWD
rm -rf Trimmomatic-0.38 Trimmomatic-0.38.zip >& /dev/null

wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip
unzip Trimmomatic-0.38.zip
cd Trimmomatic-0.38/
ln -s trimmomatic-0.38.jar trimmomatic.jar

# wrapper
cat > trimmomatic.sh << 'EOF'
#!/bin/bash

# Trimmomatic wrapper

if [ -h $0 ]; then 
	DIR=$(dirname $(readlink -m $0)) 
else 
	DIR=$(dirname $0)
fi   

java -Xmx2g -jar $DIR/trimmomatic.jar $*

EOF

chmod +x trimmomatic.sh
ln -s trimmomatic.sh trimmomatic

rm -rf /usr/local/extras/Genomics/apps/trimmomatic/0.38 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/trimmomatic/0.38
cp -r * /usr/local/extras/Genomics/apps/trimmomatic/0.38/
cd /usr/local/extras/Genomics/apps/trimmomatic
ln -sTf 0.38 current
ln -sf /usr/local/extras/Genomics/apps/trimmomatic/current/trimmomatic /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf Trimmomatic-0.38 Trimmomatic-0.38.zip

echo "=============================================================================="
