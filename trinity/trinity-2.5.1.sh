#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o trinity-2.5.1.log

# set genomics repository paths
source /usr/local/extras/Genomics/.bashrc


# set gcc 6.2
module add compilers/gcc/6.2

# set multithreading perl version
perlbrew switch perl-5.26.1-thread

CURRDIR=$PWD
rm -rf trinityrnaseq-Trinity-v2.5.1 Trinity-v2.5.1.tar.gz >& /dev/null

wget https://github.com/trinityrnaseq/trinityrnaseq/archive/Trinity-v2.5.1.tar.gz
tar -xf Trinity-v2.5.1.tar.gz
cd trinityrnaseq-Trinity-v2.5.1
make -j 12
make -j 12 plugins
perl -pi -e 's/use FindBin\;/use FindBin qw\(\$RealBin\)\;/g' Trinity
perl -pi -e 's/usee lib \(\"\$FindBin\:\:Bin\/PerlLib\"\)\;/use lib \(\"\$RealBin\/PerlLib\"\)\;/g' Trinity
perl -pi -e 'if (/\$ENV\{TRINITY_HOME\}/){
				$_.="    \$ENV\{JAVA_HOME\} = \"/usr/local/extras/Genomics/apps/java/current\";\n";
				$_.="    \$ENV\{PATH\} = \"/usr/local/extras/Genomics/apps/java/current/bin\:\$ENV\{PATH\}\";\n";
			 }' Trinity

rm -rf /usr/local/extras/Genomics/apps/trinity/2.5.1 >& /dev/null
mkdir -p /usr/local/extras/Genomics/apps/trinity/2.5.1
cp -r * /usr/local/extras/Genomics/apps/trinity/2.5.1/

cd /usr/local/extras/Genomics/apps/trinity
ln -sTf 2.5.1 current
cd current

cat > trinity.sh <<'EOF'
#!/bin/bash

# Wrapper to run Trinity

# libraries gcc 6.2
module add compilers/gcc/6.2

# load genomics repository
source /usr/local/extras/Genomics/.bashrc >& /dev/null

export TRINITY_HOME="/usr/local/extras/Genomics/apps/trinity/current"

# switch to threaded perl version to run Trinity
perlbrew switch perl-5.26.1-thread

/usr/local/extras/Genomics/apps/trinity/current/Trinity $*

# switch back no-threaded perl
perlbrew switch perl-5.26.1

EOF

chmod +x trinity.sh
ln -s trinity.sh trinity

ln -sf /usr/local/extras/Genomics/apps/trinity/current/trinity /usr/local/extras/Genomics/bin/trinity

# set back no-multithreading perl version
perlbrew switch perl-5.26.1

cd $CURRDIR
rm -rf trinityrnaseq-Trinity-v2.5.1 Trinity-v2.5.1.tar.gz 
