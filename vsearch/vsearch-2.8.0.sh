#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -j y
#$ -o vsearch-2.8.0.log

CURRDIR=$PWD

rm -rf vsearch-2.8.0.tar.gz vsearch-2.8.0 >& /dev/null

wget https://github.com/torognes/vsearch/archive/v2.8.0.tar.gz -O vsearch-2.8.0.tar.gz
tar -xf vsearch-2.8.0.tar.gz
cd vsearch-2.8.0
./autogen.sh
./configure --prefix=/usr/local/extras/Genomics/apps/vsearch/2.8.0
make
make install

cd /usr/local/extras/Genomics/apps/vsearch
ln -sTf 2.8.0 current
ln -sf /usr/local/extras/Genomics/apps/vsearch/current/bin/* /usr/local/extras/Genomics/bin/

cd $CURRDIR
rm -rf vsearch-2.8.0.tar.gz vsearch-2.8.0

