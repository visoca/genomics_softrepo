#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o xz-5.2.3.log

CURRDIR=$PWD
rm -rf xz-5.2.3 xz-5.2.3.tar.bz2 >& /dev/null

wget https://tukaani.org/xz/xz-5.2.3.tar.bz2
tar -xf xz-5.2.3.tar.bz2
cd xz-5.2.3
./configure \
--prefix=/usr/local/extras/Genomics/apps/xz/5.2.3
make -j12
rm -rf /usr/local/extras/Genomics/apps/xz/5.2.3 >& /dev/null
make install

cd /usr/local/extras/Genomics/apps/xz
rm current >& /dev/null
ln -s 5.2.3 current
ln -s /usr/local/extras/Genomics/apps/xz/current/bin/* /usr/local/extras/Genomics/bin/
ln -s /usr/local/extras/Genomics/apps/xz/current/lib/* /usr/local/extras/Genomics/lib/lib/
ln -s /usr/local/extras/Genomics/apps/xz/current/lib/pkgconfig/* /usr/local/extras/Genomics/lib/lib/pkgconfig/
ln -s /usr/local/extras/Genomics/apps/xz/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf xz-5.2.3 xz-5.2.3.tar.bz2
