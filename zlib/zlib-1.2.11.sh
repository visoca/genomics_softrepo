#!/bin/bash
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 12
#$ -j y
#$ -o zlib-1.2.11.log

CURRDIR=$PWD
rm -rf zlib-1.2.11 zlib-1.2.11.tar.gz >& /dev/null

wget https://zlib.net/zlib-1.2.11.tar.gz
tar -xf zlib-1.2.11.tar.gz
cd zlib-1.2.11
./configure \
--prefix=/usr/local/extras/Genomics/lib/zlib/1.2.11
make -j12
rm -rf /usr/local/extras/Genomics/lib/zlib/1.2.11 >& /dev/null
make install

cd /usr/local/extras/Genomics/lib/zlib
rm current >& /dev/null
ln -s 1.2.11 current
ln -s /usr/local/extras/Genomics/lib/zlib/current/lib/* /usr/local/extras/Genomics/lib/lib/
ln -s /usr/local/extras/Genomics/lib/zlib/current/lib/pkgconfig/* /usr/local/extras/Genomics/lib/lib/pkgconfig/
ln -s /usr/local/extras/Genomics/lib/zlib/current/include/* /usr/local/extras/Genomics/lib/include/

cd $CURRDIR
rm -rf zlib-1.2.11 zlib-1.2.11.tar.gz
